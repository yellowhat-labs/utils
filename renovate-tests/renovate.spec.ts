// https://github.com/renovatebot/renovate/blob/main/lib/modules/manager/fvm/extract.spec.ts
import { extractPackageFile } from "renovate/dist/modules/manager/custom/regex";
// https://github.com/renovatebot/renovate/blob/main/lib/modules/manager/types.ts
import type { ExtractConfig } from "renovate/dist/modules/manager/types";

describe("./config.js", () => {
    const baseConfig = require("./config.js");
    const packageFile = "UNUSED";

    describe("Look for pip install", () => {
        // Use index until https://github.com/renovatebot/renovate/issues/21760
        const config: ExtractConfig = baseConfig.customManagers[0];

        it("without options", () => {
            const content = `
            codespell:
              before_script:
                - 'echo Extra opts: $[[ inputs.codespell_extra_opts ]]'
                - pip install codespell==2.3.0
              script:
                - codespell
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "codespell",
                        currentValue: "2.3.0",
                    },
                ],
            });
        });

        it("with options", () => {
            const content = `
            pylint:
              before_script:
                - pip install pylint[spelling]==3.2.6
              script:
                - pylint
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "pylint",
                        currentValue: "3.2.6",
                    },
                ],
            });
        });

        it("uv", () => {
            const content = `
            codespell:
              before_script:
                - 'echo Extra opts: $[[ inputs.codespell_extra_opts ]]'
                - uv pip install codespell==2.2.0 --system
              script:
                - codespell
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "codespell",
                        currentValue: "2.2.0",
                    },
                ],
            });
        });
    });

    describe("Look for npm install", () => {
        // Use index until https://github.com/renovatebot/renovate/issues/21760
        const config: ExtractConfig = baseConfig.customManagers[1];

        it("without group", () => {
            const content = `
            markdown-table-formatter:
              before_script:
                - npm install --global markdown-table-formatter@1.6.1
              script:
                - markdown-table-formatter
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "markdown-table-formatter",
                        currentValue: "1.6.1",
                    },
                ],
            });
        });

        it("with group", () => {
            const content = `
            biome:
              before_script:
                - npm install --global @biomejs/biome@1.8.3
              script:
                - biome
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "@biomejs/biome",
                        currentValue: "1.8.3",
                    },
                ],
            });
        });
    });

    describe("Look for npx", () => {
        // Use index until https://github.com/renovatebot/renovate/issues/21760
        const config: ExtractConfig = baseConfig.customManagers[2];

        it("without group", () => {
            const content = `
            script:
              - npx --yes @biomejs/biome@1.8.3 ci --indent-style space --indent-width 4 .
           `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "@biomejs/biome",
                        currentValue: "1.8.3",
                    },
                ],
            });
        });

        it("with group", () => {
            const content = `
            biome:
              script:
                - npx -y @biomejs/biome@1.8.2 version
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "@biomejs/biome",
                        currentValue: "1.8.2",
                    },
                ],
            });
        });
    });

    describe("Comment", () => {
        // Use index until https://github.com/renovatebot/renovate/issues/21760
        const config: ExtractConfig = baseConfig.customManagers[3];

        it("ARG HELM_VER no quote", () => {
            const content = `
            # renovate: datasource=github-releases depName=helm/helm
            ARG HELM_VER=v1.0.0
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "helm/helm",
                        currentValue: "v1.0.0",
                    },
                ],
            });
        });

        it("ARG HELM_VER single quote", () => {
            const content = `
            # renovate: datasource=github-releases depName=helm/helm
            ARG HELM_VER='1.0.1'
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "helm/helm",
                        currentValue: "1.0.1",
                    },
                ],
            });
        });

        it("ARG HELM_VER double quote", () => {
            const content = `
            # renovate: datasource=github-releases depName=helm/helm
            ARG HELM_VER="v3.15.4"
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "helm/helm",
                        currentValue: "v3.15.4",
                    },
                ],
            });
        });

        it("ENV HELM_VER double quote", () => {
            const content = `
            # renovate: datasource=github-releases depName=helm/helm
            ENV HELM_VER="v1.2.3.4"
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "helm/helm",
                        currentValue: "v1.2.3.4",
                    },
                ],
            });
        });

        it("HELM_VER double quote", () => {
            const content = `
            # renovate: datasource=github-releases depName=helm/helm
            HELM_VER="v1.2.3-foo"
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "helm/helm",
                        currentValue: "v1.2.3-foo",
                    },
                ],
            });
        });

        it("HELMVER double quote", () => {
            const content = `
            # renovate: datasource=github-releases depName=helm/helm
            HELMVER="1.2.3"
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "helm/helm",
                        currentValue: "1.2.3",
                    },
                ],
            });
        });

        it("version:", () => {
            const content = `
            url: https://github.com/jqlang/jq/releases/download/{VERSION}/jq-linux64
            # renovate: datasource=github-releases depName=jqlang/jq
            version: jq-1.7.1
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "jqlang/jq",
                        currentValue: "jq-1.7.1",
                    },
                ],
            });
        });

        it("kubernetesVersion:", () => {
            const content = `
            # renovate: datasource=github-releases depName=kubernetes/kubernetes
            kubernetesVersion: v1.31.0
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "kubernetes/kubernetes",
                        currentValue: "v1.31.0",
                    },
                ],
            });
        });

        it("spaces before", () => {
            const content = `
            locals {
              foo = "bar"
              # renovate: datasource=github-releases depName=containerd/containerd
              containerd_ver = "v1.7.21"
            }
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "containerd/containerd",
                        currentValue: "v1.7.21",
                    },
                ],
            });
        });

        it("versioning", () => {
            const content = `
            # renovate: datasource=repology depName=alpine_3_20/git versioning=loose
            ARG GIT_VER=2.45.2-r0
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "alpine_3_20/git",
                        currentValue: "2.45.2-r0",
                    },
                ],
            });
        });

        it("tag", () => {
            const content = `
            image:
              registry: ghcr.io
              repository: yellowhat/registry/pgvecto-rs
              # renovate: datasource=docker depName=ghcr.io/yellowhat/registry/pgvecto-rs
              tag: 16.4.0-debian-12-r19
            `;
            const res = extractPackageFile(content, packageFile, config);
            expect(res).toMatchSnapshot({
                deps: [
                    {
                        depName: "ghcr.io/yellowhat/registry/pgvecto-rs",
                        currentValue: "16.4.0-debian-12-r19",
                    },
                ],
            });
        });
    });
});
