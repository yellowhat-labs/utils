## [2.4.10](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.9...v2.4.10) (2025-03-02)


### Bug Fixes

* **deps:** update quay.io/podman/stable docker tag to v5.4.0 ([788798a](https://gitlab.com/yellowhat-labs/bootstrap/commit/788798a3fe4b3e64f0ff6b250dccb59ff692d4b4))


### Chore

* **deps:** lock file maintenance ([ce3a054](https://gitlab.com/yellowhat-labs/bootstrap/commit/ce3a054bf1c9193aa3d88974cf2c959a2c323ebe))
* **deps:** lock file maintenance ([584c711](https://gitlab.com/yellowhat-labs/bootstrap/commit/584c711ecb8c8b32d0b73914ffa566df7299f281))
* **deps:** lock file maintenance ([9a20bfc](https://gitlab.com/yellowhat-labs/bootstrap/commit/9a20bfcce5232e48ab835243d65b67b164dd52c0))
* **deps:** lock file maintenance ([b29e209](https://gitlab.com/yellowhat-labs/bootstrap/commit/b29e209683eb6a202d7ffdeae989652de9069ec8))
* **deps:** lock file maintenance ([ced5f72](https://gitlab.com/yellowhat-labs/bootstrap/commit/ced5f72cbc5e6f052edc1963b61a17d8ea11e2ef))
* **deps:** lock file maintenance ([c9e20b8](https://gitlab.com/yellowhat-labs/bootstrap/commit/c9e20b8ff8ea70e38f03d9424b9f93fee88f027f))
* **deps:** lock file maintenance ([71396c3](https://gitlab.com/yellowhat-labs/bootstrap/commit/71396c3c9124360cb3746a367529695e3880335e))
* **deps:** lock file maintenance ([82ad336](https://gitlab.com/yellowhat-labs/bootstrap/commit/82ad33642047ff4a6f97e907b76f0ca27804c060))
* **deps:** lock file maintenance ([9662a1d](https://gitlab.com/yellowhat-labs/bootstrap/commit/9662a1ddc34f51fab076e3e04a717f4ebe150115))
* **deps:** lock file maintenance ([682b149](https://gitlab.com/yellowhat-labs/bootstrap/commit/682b149eb8215f36e657f542c97f5aea83791bc3))
* **deps:** lock file maintenance ([a3b0ce2](https://gitlab.com/yellowhat-labs/bootstrap/commit/a3b0ce2ee0e3de57f48a885e2a885843b0f9dd24))
* **deps:** lock file maintenance ([701dfb6](https://gitlab.com/yellowhat-labs/bootstrap/commit/701dfb6c3bc79a8d0c29d265bd11570e518e8ae1))
* **deps:** lock file maintenance ([f3c4cd4](https://gitlab.com/yellowhat-labs/bootstrap/commit/f3c4cd4a3da2d6b0fde1615bcf690e2090f297cc))
* **deps:** lock file maintenance ([574bb87](https://gitlab.com/yellowhat-labs/bootstrap/commit/574bb87d64a68de40e9cbb7ba5901cc3d5c0e688))
* **deps:** update all non-major dependencies ([c6033fe](https://gitlab.com/yellowhat-labs/bootstrap/commit/c6033fe7e542b8083b72343aaa251597528f2750))
* **deps:** update all non-major dependencies ([2f3789f](https://gitlab.com/yellowhat-labs/bootstrap/commit/2f3789f2ef6b2287629184150944804f78905365))
* **deps:** update all non-major dependencies ([c0a3372](https://gitlab.com/yellowhat-labs/bootstrap/commit/c0a33722e602820a70bc9871b24f49da4613e0bb))
* **deps:** update all non-major dependencies ([01c5bcc](https://gitlab.com/yellowhat-labs/bootstrap/commit/01c5bccc108fffdb27eac0fd78b5b57c51eee4d3))
* **deps:** update all non-major dependencies ([60d647b](https://gitlab.com/yellowhat-labs/bootstrap/commit/60d647b5177b87d09697bd5d849ff781ba8c98e8))
* **deps:** update all non-major dependencies ([5e27a11](https://gitlab.com/yellowhat-labs/bootstrap/commit/5e27a11324facc0b0c818352d6205f6f6b1945a4))
* **deps:** update all non-major dependencies ([0d2e7f0](https://gitlab.com/yellowhat-labs/bootstrap/commit/0d2e7f025b73d4d616c817690f51bf3a0af47d48))
* **deps:** update all non-major dependencies ([b13ecae](https://gitlab.com/yellowhat-labs/bootstrap/commit/b13ecaea9372a2ac796437d224ed744cb6684711))
* **deps:** update all non-major dependencies ([936d4a8](https://gitlab.com/yellowhat-labs/bootstrap/commit/936d4a88e2734208e833facc361685e44b6966d3))
* **deps:** update all non-major dependencies ([007cd78](https://gitlab.com/yellowhat-labs/bootstrap/commit/007cd7819312ec58f7e66d8b4957a71037295122))
* **deps:** update docker.io/pulumi/pulumi-python docker tag to v3.152.0 ([bdc202e](https://gitlab.com/yellowhat-labs/bootstrap/commit/bdc202e10ffb6415e28a91111da53b37d7a00f51))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.173.1 ([eec9068](https://gitlab.com/yellowhat-labs/bootstrap/commit/eec9068c63e4b7a9fdfd13e5514c830c492e3494))

## [2.4.9](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.8...v2.4.9) (2025-02-16)


### Chore

* **deps:** lock file maintenance ([37ebb4e](https://gitlab.com/yellowhat-labs/bootstrap/commit/37ebb4e28f69772b6b19ea865f9d0508b42a0b45))
* **deps:** lock file maintenance ([7115a0a](https://gitlab.com/yellowhat-labs/bootstrap/commit/7115a0a58fdbfc2ab3a425ed98f050fdf8034f7f))
* **deps:** lock file maintenance ([373124c](https://gitlab.com/yellowhat-labs/bootstrap/commit/373124c90ee51f498a2b488ccf32077f60d9efc0))
* **deps:** lock file maintenance ([07e76eb](https://gitlab.com/yellowhat-labs/bootstrap/commit/07e76ebadde0765c10436efeacafa975aa0e66ae))
* **deps:** update all non-major dependencies ([3a05bb7](https://gitlab.com/yellowhat-labs/bootstrap/commit/3a05bb79aebfb11d52e631be0a4b18a372c168ef))
* **deps:** update all non-major dependencies ([81dddd0](https://gitlab.com/yellowhat-labs/bootstrap/commit/81dddd0e5f0ccd1aeb624acf51a13b0b2d4558df))
* **deps:** update all non-major dependencies ([96832de](https://gitlab.com/yellowhat-labs/bootstrap/commit/96832de25c781cf08fed0666de60623d0903e893))
* **deps:** update all non-major dependencies to v39.171.1 ([202d248](https://gitlab.com/yellowhat-labs/bootstrap/commit/202d2485adb5e0c6e2c1c19499006eb655d898bb))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.167.0 ([51131c8](https://gitlab.com/yellowhat-labs/bootstrap/commit/51131c854074684248e825e0aeb00bbbf7ff891f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.167.1 ([117bd9f](https://gitlab.com/yellowhat-labs/bootstrap/commit/117bd9f454612ee9e1b69a2037c7c65a17bfbf9f))
* **deps:** update node.js to 9bef0ef ([15e9625](https://gitlab.com/yellowhat-labs/bootstrap/commit/15e96252470c91482f18fe8d8c79bfc97807cd25))


### Bug Fixes

* **deps:** update dependency semantic-release to v24.2.3 ([536f8b9](https://gitlab.com/yellowhat-labs/bootstrap/commit/536f8b972694a204931fc5969ea9499de5d7d678))

## [2.4.8](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.7...v2.4.8) (2025-02-12)


### Chore

* **deps:** lock file maintenance ([b3fb415](https://gitlab.com/yellowhat-labs/bootstrap/commit/b3fb4159317e4f642725aaa28ab73f5c5c0ddac1))
* **deps:** lock file maintenance ([bffe73f](https://gitlab.com/yellowhat-labs/bootstrap/commit/bffe73f0add6b99deb64b81472262646386bd50a))
* **deps:** lock file maintenance ([263f3ec](https://gitlab.com/yellowhat-labs/bootstrap/commit/263f3ec0e31bca86c5b991dd20f143a0a108b857))
* **deps:** lock file maintenance ([32aa6ad](https://gitlab.com/yellowhat-labs/bootstrap/commit/32aa6ad4d839c396e8620bcdc8571b94ba29b564))
* **deps:** lock file maintenance ([1967862](https://gitlab.com/yellowhat-labs/bootstrap/commit/1967862d37f543ba03a21f7e2712527869f49786))
* **deps:** update all non-major dependencies ([5d44f2b](https://gitlab.com/yellowhat-labs/bootstrap/commit/5d44f2b9f266b06e1e3ec2190f3640af6ea2adfe))
* **deps:** update all non-major dependencies ([d4f7073](https://gitlab.com/yellowhat-labs/bootstrap/commit/d4f7073b6e838e70458ae6b0584bea4879bb8cd5))
* **deps:** update dependency alpine_3_21/openssl to v3.3.2-r6 ([2858c48](https://gitlab.com/yellowhat-labs/bootstrap/commit/2858c486eebd13a7b302039c92d95e87c38e79ac))


### Bug Fixes

* **nixfmt:** select only *.nix, show diff in job ([32fdbc0](https://gitlab.com/yellowhat-labs/bootstrap/commit/32fdbc04c61819920201401224691606a911265c))


### Continuous Integration

* **renovate:** sync ([f5345aa](https://gitlab.com/yellowhat-labs/bootstrap/commit/f5345aab327b51cf84c118123eb8e85266fc84d0))

## [2.4.7](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.6...v2.4.7) (2025-02-09)


### Chore

* **deps:** lock file maintenance ([8df919e](https://gitlab.com/yellowhat-labs/bootstrap/commit/8df919eeb5da42a742795fb070f4ee9a71266254))
* **deps:** lock file maintenance ([b166fba](https://gitlab.com/yellowhat-labs/bootstrap/commit/b166fba94785892cd4e9b32d04eaaa0d0ff7b57c))
* **deps:** lock file maintenance ([a4f7f60](https://gitlab.com/yellowhat-labs/bootstrap/commit/a4f7f602809dd28850a77b5bac9081c9d9d0f9af))
* **deps:** lock file maintenance ([3621754](https://gitlab.com/yellowhat-labs/bootstrap/commit/3621754686309499dcc88aea36c4cffde39ef38a))
* **deps:** lock file maintenance ([a58d2e5](https://gitlab.com/yellowhat-labs/bootstrap/commit/a58d2e5e1f7078c17987def4849dbdcb50c60c8b))
* **deps:** lock file maintenance ([48f68ea](https://gitlab.com/yellowhat-labs/bootstrap/commit/48f68eabe0bc1fb46d895ff3ef19c639e1360cb6))
* **deps:** lock file maintenance ([48c8d86](https://gitlab.com/yellowhat-labs/bootstrap/commit/48c8d86708edf9b4baff7bcf62942c9cb9ebfd34))
* **deps:** update all non-major dependencies ([e8ff49f](https://gitlab.com/yellowhat-labs/bootstrap/commit/e8ff49f1ca22c019e87527ec028bfaae85cbf333))
* **deps:** update all non-major dependencies ([ee43a1e](https://gitlab.com/yellowhat-labs/bootstrap/commit/ee43a1eeb26cb51f2a210cd28ec557a683da6b64))
* **deps:** update all non-major dependencies ([84b8144](https://gitlab.com/yellowhat-labs/bootstrap/commit/84b8144cbe6b89c07b557d43f4d0067578ac0f21))
* **deps:** update all non-major dependencies ([fd2fd6a](https://gitlab.com/yellowhat-labs/bootstrap/commit/fd2fd6a7a9a28318e4babfeebc60e3b298d342f3))
* **deps:** update dependency @semantic-release/exec to v7 ([e9dbd12](https://gitlab.com/yellowhat-labs/bootstrap/commit/e9dbd1275b5d6339a7ef22aa14b5ebce7e08dfa0))
* **deps:** update docker.io/nixos/nix docker tag to v2.26.0 ([7e4d881](https://gitlab.com/yellowhat-labs/bootstrap/commit/7e4d8817beafdfe0533e7abfdcc5523109d63d7c))
* **deps:** update docker.io/pyfound/black docker tag to v25 ([abd4ddc](https://gitlab.com/yellowhat-labs/bootstrap/commit/abd4ddc8fc801523c91bf0c44294dd802f17a0d3))


### Bug Fixes

* **deps:** update dependency semantic-release to v24.2.2 ([070dc95](https://gitlab.com/yellowhat-labs/bootstrap/commit/070dc9597c4a866f4c29ac3de99b41e5e8d97c69))

## [2.4.6](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.5...v2.4.6) (2025-01-19)


### Bug Fixes

* **deps:** lock file maintenance ([f52dd6b](https://gitlab.com/yellowhat-labs/bootstrap/commit/f52dd6bc9a683c0ce335b2ca40c5de09a4d79a35))


### Chore

* **deps:** lock file maintenance ([bae0139](https://gitlab.com/yellowhat-labs/bootstrap/commit/bae0139e60e56204bd3409195c1aa232ace87b06))
* **deps:** lock file maintenance ([0fab4eb](https://gitlab.com/yellowhat-labs/bootstrap/commit/0fab4ebbe32f32502769ee957812b8532ef6aac7))
* **deps:** lock file maintenance ([c47a41a](https://gitlab.com/yellowhat-labs/bootstrap/commit/c47a41a9b2c5837a5975ee2d26c22c879fdf1520))
* **deps:** lock file maintenance ([e38f8fb](https://gitlab.com/yellowhat-labs/bootstrap/commit/e38f8fbcc841d7286857f413336d42c0f2b49d37))
* **deps:** lock file maintenance ([7e5ff2b](https://gitlab.com/yellowhat-labs/bootstrap/commit/7e5ff2b710dca7bc9604f8a1c8a9439b62d88e20))
* **deps:** update all non-major dependencies ([bc17360](https://gitlab.com/yellowhat-labs/bootstrap/commit/bc17360105973bcfc8dbf158a7e22ef4f76ff36b))
* **deps:** update all non-major dependencies ([c253d97](https://gitlab.com/yellowhat-labs/bootstrap/commit/c253d976293eae79020126f5a4d3d80a137c2226))
* **deps:** update all non-major dependencies ([6a8c165](https://gitlab.com/yellowhat-labs/bootstrap/commit/6a8c165f38127bc083c28aff5ac450527dbd815e))
* **deps:** update all non-major dependencies to v39.117.2 ([aa72d27](https://gitlab.com/yellowhat-labs/bootstrap/commit/aa72d272c6db0e95ff83acefea6426fbb2cef863))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.107.2 ([d4c8df7](https://gitlab.com/yellowhat-labs/bootstrap/commit/d4c8df7e639f1ad34880b60530eb336c12ec82a1))
* **deps:** update node.js to d77c645 ([91b9948](https://gitlab.com/yellowhat-labs/bootstrap/commit/91b994860a4985ee20300ba6b7f2ad9719819b51))


### infra

* stop tracking copr repo as archived ([955d1f0](https://gitlab.com/yellowhat-labs/bootstrap/commit/955d1f09fed8e5365112c0968b93e15b0cb0451f))

## [2.4.5](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.4...v2.4.5) (2025-01-14)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.2.4 ([d483da8](https://gitlab.com/yellowhat-labs/bootstrap/commit/d483da8e83bc3733a4d4da59ec261a14aeab0a0a))


### Chore

* **deps:** lock file maintenance ([8e7f66b](https://gitlab.com/yellowhat-labs/bootstrap/commit/8e7f66b1f13e8c5233c263c719bfacfe0496afae))
* **deps:** lock file maintenance ([16d219c](https://gitlab.com/yellowhat-labs/bootstrap/commit/16d219cb65e2ae8358788a80d5eb34c8778589bb))
* **deps:** update all non-major dependencies ([7ff286e](https://gitlab.com/yellowhat-labs/bootstrap/commit/7ff286e3fdc86c4dfce41b97320f8da023bde3dc))
* **deps:** update dependency bandit to v1.8.2 ([1bb3805](https://gitlab.com/yellowhat-labs/bootstrap/commit/1bb3805799fe38a6028d9bf80ca5db687e56524e))
* **deps:** update node.js to 4f7fb7f ([38a45ee](https://gitlab.com/yellowhat-labs/bootstrap/commit/38a45eebf6353584f2d07d0170aa43b37f305058))

## [2.4.4](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.3...v2.4.4) (2025-01-12)


### Bug Fixes

* **nix-diff:** remove git diff as useless ([ea892a1](https://gitlab.com/yellowhat-labs/bootstrap/commit/ea892a18eee668bc76f766031362b0f4d403ae61))


### Chore

* **deps:** lock file maintenance ([aa7ff8a](https://gitlab.com/yellowhat-labs/bootstrap/commit/aa7ff8af75d2cf6d50a9c23404b0eb62c6d25399))
* **deps:** lock file maintenance ([bb05fdc](https://gitlab.com/yellowhat-labs/bootstrap/commit/bb05fdcac74b655d5e9c3cca9b5eac7e45673d6e))
* **deps:** lock file maintenance ([04a678b](https://gitlab.com/yellowhat-labs/bootstrap/commit/04a678ba5ebdc0acee70a41b97c98cd467368ec0))
* **deps:** lock file maintenance ([788b5dd](https://gitlab.com/yellowhat-labs/bootstrap/commit/788b5dd753dd6beb728324c09c1c17209854feb4))
* **deps:** lock file maintenance ([6cb9a6a](https://gitlab.com/yellowhat-labs/bootstrap/commit/6cb9a6a1963119f54927f32b3cebfa9416b89211))
* **deps:** update all non-major dependencies ([b21528b](https://gitlab.com/yellowhat-labs/bootstrap/commit/b21528bfaac0f27d8ce8ffd06693e153fb9bdb9f))
* **deps:** update all non-major dependencies ([0febc75](https://gitlab.com/yellowhat-labs/bootstrap/commit/0febc756dcbc92cf3172f6289d114bd06ccdc218))
* **deps:** update all non-major dependencies ([f77325f](https://gitlab.com/yellowhat-labs/bootstrap/commit/f77325f490f433a28f52fdc07176d548cafbc762))
* **deps:** update all non-major dependencies ([a9ca6fa](https://gitlab.com/yellowhat-labs/bootstrap/commit/a9ca6fa9d5cfeac7c4589d32c47f72effa3ed1e6))
* **deps:** update all non-major dependencies ([980f3df](https://gitlab.com/yellowhat-labs/bootstrap/commit/980f3dfb9860f8c2faef6240f4091fcfed64390d))
* **deps:** update all non-major dependencies ([690d09d](https://gitlab.com/yellowhat-labs/bootstrap/commit/690d09d7d7a3a65f8602be6bd47037122e0e7c6c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.97.0 ([f7c2f7c](https://gitlab.com/yellowhat-labs/bootstrap/commit/f7c2f7c019c78a7a29e24f74fe4b102382d5111f))


### Continuous Integration

* **renovate:** sync ([90ca4cb](https://gitlab.com/yellowhat-labs/bootstrap/commit/90ca4cb6908ab036768dabd9b3d17986cd456b03))

## [2.4.3](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.2...v2.4.3) (2025-01-08)


### Chore

* **deps:** lock file maintenance ([e8540f0](https://gitlab.com/yellowhat-labs/bootstrap/commit/e8540f0d0e3f98a7a55c8dd7f2a958517159a7b2))
* **deps:** update all non-major dependencies ([a15b183](https://gitlab.com/yellowhat-labs/bootstrap/commit/a15b183b5de6e643a09406ba84fd754ba7844b90))


### Bug Fixes

* **ci:** move nix-diff to comment.yaml ([5a5f518](https://gitlab.com/yellowhat-labs/bootstrap/commit/5a5f518e78942b24cb4163f5b7e752f51c125471))

## [2.4.2](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.1...v2.4.2) (2025-01-08)


### Bug Fixes

* **ci:** run renovate-parsed also on main ([4d2a2de](https://gitlab.com/yellowhat-labs/bootstrap/commit/4d2a2dea4ab8ee21e513892bdca5cd5fed14381e))

## [2.4.1](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.4.0...v2.4.1) (2025-01-08)


### Bug Fixes

* **ci|release:** use release stage instead of .post as main pipeline might be empty ([d711ad8](https://gitlab.com/yellowhat-labs/bootstrap/commit/d711ad8e5053e4e6b98a6808fd311ba738673fff))


### Chore

* **deps:** lock file maintenance ([5734517](https://gitlab.com/yellowhat-labs/bootstrap/commit/5734517dfe77c0a852738eae5206a3940cbe79b3))
* **deps:** lock file maintenance ([3302ba1](https://gitlab.com/yellowhat-labs/bootstrap/commit/3302ba17c45521064d181091cfb835cd0110b4c8))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.95.0 ([e57b581](https://gitlab.com/yellowhat-labs/bootstrap/commit/e57b58129cf18dcb2157d77b600757529ccbc0f3))

# [2.4.0](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.3.0...v2.4.0) (2025-01-08)


* fix(ci); nix-diff:comment deps on nix-diff ([80e62d0](https://gitlab.com/yellowhat-labs/bootstrap/commit/80e62d0140e9b7bb50a575441014c9c16748ab18))


### Features

* **ci:** include nix-diff jobs ([2d23be1](https://gitlab.com/yellowhat-labs/bootstrap/commit/2d23be13a585e5a1155900d23e71a759c5c5edbc))

# [2.3.0](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.2.2...v2.3.0) (2025-01-08)


### Chore

* **deps:** lock file maintenance ([1ed8528](https://gitlab.com/yellowhat-labs/bootstrap/commit/1ed852816bc3b93556e8450d1f9c5a1e1e7f7f01))
* **deps:** lock file maintenance ([cbe77ea](https://gitlab.com/yellowhat-labs/bootstrap/commit/cbe77ea6ddb1e6f2551c2a76990e8eae79ac75b0))
* **deps:** lock file maintenance ([5a86a75](https://gitlab.com/yellowhat-labs/bootstrap/commit/5a86a75b0f901297d7da984fc1c6f5f5ec415941))
* **deps:** lock file maintenance ([3018674](https://gitlab.com/yellowhat-labs/bootstrap/commit/3018674c8b936673fa417c21ee70ad7a33a48c2e))
* **deps:** lock file maintenance ([82ec499](https://gitlab.com/yellowhat-labs/bootstrap/commit/82ec499f21d9acae9e9c0d87363be1c74cdd5875))
* **deps:** lock file maintenance ([322b87e](https://gitlab.com/yellowhat-labs/bootstrap/commit/322b87e40a76fe73e80ee3d8f547ca27bcd7d3fb))
* **deps:** update all non-major dependencies ([fff19c6](https://gitlab.com/yellowhat-labs/bootstrap/commit/fff19c682ed539ed9ac1671dfd1e84930508c9d0))
* **deps:** update all non-major dependencies ([dacc609](https://gitlab.com/yellowhat-labs/bootstrap/commit/dacc6096ef304932250127d33ad9d013286f5989))
* **deps:** update all non-major dependencies ([c0b0139](https://gitlab.com/yellowhat-labs/bootstrap/commit/c0b0139b28e26e3723ca196eae0eb0a2c75c07f2))
* **deps:** update all non-major dependencies ([31c25f0](https://gitlab.com/yellowhat-labs/bootstrap/commit/31c25f097880f85779dddc024c3e47ff25f82a12))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.90.5 ([a68d149](https://gitlab.com/yellowhat-labs/bootstrap/commit/a68d149fc9ef0413849acfc0c9b94525e0470c4c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.91.0 ([18fa50c](https://gitlab.com/yellowhat-labs/bootstrap/commit/18fa50c62c7eeaf88354203e024d295ec612fb31))


### Features

* **ci:** run linters only on MR not main ([7f17e1b](https://gitlab.com/yellowhat-labs/bootstrap/commit/7f17e1b8b7665f22b56d367efafe1222114e5b68))

## [2.2.2](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.2.1...v2.2.2) (2025-01-03)


### Bug Fixes

* **deps:** update semantic-release monorepo ([8d20d22](https://gitlab.com/yellowhat-labs/bootstrap/commit/8d20d2280539c93a17fef594ad14737d7f720f8c))


### Chore

* **deps:** lock file maintenance ([138b2af](https://gitlab.com/yellowhat-labs/bootstrap/commit/138b2afa15874b0550cea32293a4b7ad6007bfae))
* **deps:** lock file maintenance ([a55fcdc](https://gitlab.com/yellowhat-labs/bootstrap/commit/a55fcdc4db35853904fbdcf98a8a12224cf42e15))
* **deps:** lock file maintenance ([45ee40e](https://gitlab.com/yellowhat-labs/bootstrap/commit/45ee40e75a8001c9e87374b760f05bad4753db95))
* **deps:** lock file maintenance ([e1f5142](https://gitlab.com/yellowhat-labs/bootstrap/commit/e1f5142d617f8536377aa22858084d5fd5245c4b))
* **deps:** lock file maintenance ([795196b](https://gitlab.com/yellowhat-labs/bootstrap/commit/795196bbe5645228e429804a306c6cc7b44976b1))
* **deps:** lock file maintenance ([b245770](https://gitlab.com/yellowhat-labs/bootstrap/commit/b24577050b9346054ada90bf34e0c77a8f9bb72e))
* **deps:** lock file maintenance ([c2dbd86](https://gitlab.com/yellowhat-labs/bootstrap/commit/c2dbd86f97392d81ac4eaee8f7fd2d95b00f6751))
* **deps:** update all non-major dependencies ([fe4460a](https://gitlab.com/yellowhat-labs/bootstrap/commit/fe4460a8e7fa24b7b4935d1b8ab3f7e8bc858605))
* **deps:** update all non-major dependencies ([3a19508](https://gitlab.com/yellowhat-labs/bootstrap/commit/3a19508f5199a9885ac41e223b033ec86304cad7))
* **deps:** update all non-major dependencies ([a814cd7](https://gitlab.com/yellowhat-labs/bootstrap/commit/a814cd7185d019ceb4d0852f974f54f016d1f427))
* **deps:** update all non-major dependencies ([d8aee13](https://gitlab.com/yellowhat-labs/bootstrap/commit/d8aee1330b5d949ec416ab77256e871232be5985))
* **deps:** update all non-major dependencies ([7d79653](https://gitlab.com/yellowhat-labs/bootstrap/commit/7d79653198cd5338b7caf95def4f821f0f000d1b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.76.0 ([2074e46](https://gitlab.com/yellowhat-labs/bootstrap/commit/2074e46380d9cbacf790d77906afb1cc8052b86f))

## [2.2.1](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.2.0...v2.2.1) (2024-12-17)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v14.0.2 ([eea00ac](https://gitlab.com/yellowhat-labs/bootstrap/commit/eea00ac4584288cb808a949e0d73ecd419fe9cd1))


### Chore

* **deps:** lock file maintenance ([7afc394](https://gitlab.com/yellowhat-labs/bootstrap/commit/7afc394a3d0b46d2fd006d9f0bc8dbcf07c6631a))
* **deps:** lock file maintenance ([ca0ecd8](https://gitlab.com/yellowhat-labs/bootstrap/commit/ca0ecd8f19fb0da669a9fe2ccc53bb8fd99e080b))
* **deps:** lock file maintenance ([6923655](https://gitlab.com/yellowhat-labs/bootstrap/commit/6923655e5052140d9e76196f835d929e543fdb8c))
* **deps:** lock file maintenance ([ab0c0e4](https://gitlab.com/yellowhat-labs/bootstrap/commit/ab0c0e4a2a59457c345ff5b362cc3d94e3a9cbea))
* **deps:** lock file maintenance ([9a56e37](https://gitlab.com/yellowhat-labs/bootstrap/commit/9a56e373feda8d11321a5c19af76179a66f59956))
* **deps:** lock file maintenance ([5e3590a](https://gitlab.com/yellowhat-labs/bootstrap/commit/5e3590ab28141f8955e06653c21bbd1f0dec6b12))
* **deps:** lock file maintenance ([1c743de](https://gitlab.com/yellowhat-labs/bootstrap/commit/1c743de03694442bd02e02ff7102acfb98423afe))
* **deps:** lock file maintenance ([11945ff](https://gitlab.com/yellowhat-labs/bootstrap/commit/11945ff83b81dcd26e56e849c720a0c45589c131))
* **deps:** lock file maintenance ([911a74f](https://gitlab.com/yellowhat-labs/bootstrap/commit/911a74f3eb41cc43d6bf7dc654cccb58768c4f5e))
* **deps:** lock file maintenance ([7258c82](https://gitlab.com/yellowhat-labs/bootstrap/commit/7258c828fe375572889df938606a48cbea14b522))
* **deps:** lock file maintenance ([ce67351](https://gitlab.com/yellowhat-labs/bootstrap/commit/ce673517a8c9d91bf73ea1c9d241dbb4dc7201ce))
* **deps:** lock file maintenance ([7abc90f](https://gitlab.com/yellowhat-labs/bootstrap/commit/7abc90f10b67a754fabedf726b22b5420f02b299))
* **deps:** lock file maintenance ([26d6c46](https://gitlab.com/yellowhat-labs/bootstrap/commit/26d6c460411b37ecbae7cd928e471df664b6f2d8))
* **deps:** lock file maintenance ([7c06706](https://gitlab.com/yellowhat-labs/bootstrap/commit/7c06706e0cf926ccc26434088cec66d00a59e0b2))
* **deps:** update all non-major dependencies ([94b1b5f](https://gitlab.com/yellowhat-labs/bootstrap/commit/94b1b5f6dae303775aecc850601f715bc804e4ce))
* **deps:** update all non-major dependencies ([12499cf](https://gitlab.com/yellowhat-labs/bootstrap/commit/12499cf730acafda518fff4cb04cd5431901ff02))
* **deps:** update all non-major dependencies ([c406790](https://gitlab.com/yellowhat-labs/bootstrap/commit/c4067905c69964b5a649d8308ebfbd5c0c551c46))
* **deps:** update all non-major dependencies ([e3d9b3b](https://gitlab.com/yellowhat-labs/bootstrap/commit/e3d9b3b839d2c0b904798e9a6df891dbafe08415))
* **deps:** update all non-major dependencies ([090ef83](https://gitlab.com/yellowhat-labs/bootstrap/commit/090ef8326bc2f0f0b7f4fbb7c3cef9324da918d2))
* **deps:** update all non-major dependencies ([bf24fd4](https://gitlab.com/yellowhat-labs/bootstrap/commit/bf24fd468135bc84c38e18b518b8e63f6dc18f02))
* **deps:** update all non-major dependencies ([eb40195](https://gitlab.com/yellowhat-labs/bootstrap/commit/eb401957e5154c82edd32d24e5edcd7bd557e5a9))
* **deps:** update all non-major dependencies to v39.57.0 ([7120665](https://gitlab.com/yellowhat-labs/bootstrap/commit/71206655e40a9b8ef3acf9159e3383d6ade66d2b))
* **deps:** update docker.io/bash:5.2.37 docker digest to f406f3b ([356af01](https://gitlab.com/yellowhat-labs/bootstrap/commit/356af01543d85479953311ec2f02a84cece6bed1))
* **deps:** update docker.io/pulumi/pulumi-python docker tag to v3.143.0 ([f7e8779](https://gitlab.com/yellowhat-labs/bootstrap/commit/f7e8779956eca62d45219ee2e22d02fa16f74c00))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.57.1 ([dfa8b15](https://gitlab.com/yellowhat-labs/bootstrap/commit/dfa8b15d86a61e8d5cfab665d135065e8dca032b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.62.1 ([fe0351b](https://gitlab.com/yellowhat-labs/bootstrap/commit/fe0351b378d6f3720e530ca571bae041493abdc1))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.64.0 ([a710afb](https://gitlab.com/yellowhat-labs/bootstrap/commit/a710afb1142257bbc4cd76341a1d8437f1440f8e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.69.2 ([a3ff2d6](https://gitlab.com/yellowhat-labs/bootstrap/commit/a3ff2d6232a1166fb5ba3f3b8a4bf28b85ccfc37))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.69.3 ([53134a9](https://gitlab.com/yellowhat-labs/bootstrap/commit/53134a9aaeddad039fe76982326c59128ec56aea))
* **deps:** update node.js to 6e80991 ([100c6e4](https://gitlab.com/yellowhat-labs/bootstrap/commit/100c6e4371583690b1d4b7476db0711cf0fc4c71))


### Continuous Integration

* **renovate:** group also pinDigest and lockfile ([d0bb1ce](https://gitlab.com/yellowhat-labs/bootstrap/commit/d0bb1cee46243ffd9442682e57599790bee731a0))
* **renovate:** group reduce matchUpdateTypes ([d66821d](https://gitlab.com/yellowhat-labs/bootstrap/commit/d66821da42e14618348f3b1dca0f86e61c7b40b9))

# [2.2.0](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.1.2...v2.2.0) (2024-12-06)


### Chore

* **deps:** lock file maintenance ([ed12812](https://gitlab.com/yellowhat-labs/bootstrap/commit/ed12812364acf7cd9343c94e9cab2189ebcd7ec5))
* **deps:** lock file maintenance ([e31f9e2](https://gitlab.com/yellowhat-labs/bootstrap/commit/e31f9e250eef08944224ca156f023222292f7eaa))
* **deps:** lock file maintenance ([f43eaf1](https://gitlab.com/yellowhat-labs/bootstrap/commit/f43eaf1bbabbd5d4a8dc585027e406b6ec91d5c3))
* **deps:** pin ghcr.io/astral-sh/uv docker tag to 9412e68 ([ef49703](https://gitlab.com/yellowhat-labs/bootstrap/commit/ef49703ce8850edf03638c84442498510bd9a0d1))
* **deps:** update dependency renovate to v39.55.0 ([58805bf](https://gitlab.com/yellowhat-labs/bootstrap/commit/58805bf244730ea820afaf23234ded311c016f99))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.55.0 ([c8f04fe](https://gitlab.com/yellowhat-labs/bootstrap/commit/c8f04fe2204b44bca43790a21b16babbb71e54a0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.56.0 ([42d2220](https://gitlab.com/yellowhat-labs/bootstrap/commit/42d22203929db427dbfee22f1851acef5dfb1af4))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.56.1 ([0a5ffe2](https://gitlab.com/yellowhat-labs/bootstrap/commit/0a5ffe2bd5f5ac67dce3b3f64373adc8fd8e6964))


### Continuous Integration

* **renovate:** group all minor and patch ([b455641](https://gitlab.com/yellowhat-labs/bootstrap/commit/b4556419ec7c48194348bad9014a1f8f751ec387))
* **renovate:** group minor, path, pin and digest ([4848bd3](https://gitlab.com/yellowhat-labs/bootstrap/commit/4848bd38946ca4515f6ecdbd0d8d3d8fd81de639))
* **renovate:** pinDigests=[secure] for podman ([555c76f](https://gitlab.com/yellowhat-labs/bootstrap/commit/555c76f80e7937579746cd4122ded5a1f7ebf510))
* **renovate:** sync ([a1d9e6e](https://gitlab.com/yellowhat-labs/bootstrap/commit/a1d9e6ee8e4801630243c59084815778fe94f9f3))


### Features

* **python:** use uv pip instead of pip, much faster install ([ccf5e77](https://gitlab.com/yellowhat-labs/bootstrap/commit/ccf5e77b28dcb3f2d8a839c5642d8b4cd646cb25))

## [2.1.2](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.1.1...v2.1.2) (2024-12-06)


### Chore

* **deps:** lock file maintenance ([a35aa75](https://gitlab.com/yellowhat-labs/bootstrap/commit/a35aa754a404c446926fa6ecea2d11a22ac4c1a3))
* **deps:** lock file maintenance ([ffa8e55](https://gitlab.com/yellowhat-labs/bootstrap/commit/ffa8e5560d4acfc85f103c418dd71baf678f476b))
* **deps:** lock file maintenance ([1f897ea](https://gitlab.com/yellowhat-labs/bootstrap/commit/1f897ea1c7defbf4e81617d346179d53da8e1d86))
* **deps:** update dependency renovate to v39.50.0 ([54fc1a6](https://gitlab.com/yellowhat-labs/bootstrap/commit/54fc1a68b241d3e0de92b0df8ad30fd3319a9c6f))
* **deps:** update dependency renovate to v39.52.0 ([0ad0475](https://gitlab.com/yellowhat-labs/bootstrap/commit/0ad0475e3408b908716c99c1a3d92a5a1acbb0bb))
* **deps:** update dependency renovate to v39.54.0 ([e60f25c](https://gitlab.com/yellowhat-labs/bootstrap/commit/e60f25c0ff675124ee278b0a779f458e09a0db35))
* **deps:** update docker.io/alpine:latest docker digest to 21dc606 ([b976e20](https://gitlab.com/yellowhat-labs/bootstrap/commit/b976e2069455beeaca107f557ac096d08ceee3c2))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.99 ([690a576](https://gitlab.com/yellowhat-labs/bootstrap/commit/690a5767c34bb769c36c32d5e1fe27b0fed9a9e3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.50.0 ([c31ae3e](https://gitlab.com/yellowhat-labs/bootstrap/commit/c31ae3e4af5b8f0b435d65a6cddf63d4d294029e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.52.0 ([08735fb](https://gitlab.com/yellowhat-labs/bootstrap/commit/08735fbca98e5857c4ee5bb710c335412ad5fa1f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.54.0 ([397c034](https://gitlab.com/yellowhat-labs/bootstrap/commit/397c03408cab854884ad2b13502fb00a9cee409b))


### Bug Fixes

* **build:** do not pin digest for podman ([b413638](https://gitlab.com/yellowhat-labs/bootstrap/commit/b413638ec75dc5e23600c390115dbc874b37694d))

## [2.1.1](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.1.0...v2.1.1) (2024-12-05)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.2.3 ([7191dba](https://gitlab.com/yellowhat-labs/bootstrap/commit/7191dbab6277568d97ab7b2d19b1078f3bb0e587))


### Chore

* **deps:** lock file maintenance ([e44549d](https://gitlab.com/yellowhat-labs/bootstrap/commit/e44549deb2e6d7b41e2f59e42bd6bfaa6bbdd507))
* **deps:** update dependency determinatesystems/flake-checker to v0.2.1 ([0e7cdd0](https://gitlab.com/yellowhat-labs/bootstrap/commit/0e7cdd04acdedcadbeeed66efdee0def8d724a5c))
* **deps:** update dependency renovate to v39.49.0 ([eef6d74](https://gitlab.com/yellowhat-labs/bootstrap/commit/eef6d74563531d207809914860552f59cf054033))
* **deps:** update dependency renovate to v39.49.3 ([2dffd3c](https://gitlab.com/yellowhat-labs/bootstrap/commit/2dffd3ca722b56747664e9729087df58e4e2f22c))
* **deps:** update dependency ruff to v0.8.2 ([7b3cbfc](https://gitlab.com/yellowhat-labs/bootstrap/commit/7b3cbfcee5556f5e42b042532417743830dbf680))
* **deps:** update docker.io/python docker tag to v3.13.1 ([05e48ef](https://gitlab.com/yellowhat-labs/bootstrap/commit/05e48ef8d206b6743ecdc0e7c83d39f43f8ebed8))
* **deps:** update docker.io/python:3.13.0 docker digest to 7861d60 ([776ccab](https://gitlab.com/yellowhat-labs/bootstrap/commit/776ccab04efc7ee857f52fabd6242dc68bce41ba))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.49.0 ([b28d5eb](https://gitlab.com/yellowhat-labs/bootstrap/commit/b28d5eb1bb5262f38f4205b37073ed35d71d2d38))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.49.3 ([c722de5](https://gitlab.com/yellowhat-labs/bootstrap/commit/c722de5212861eb46dd3730ad5c556f153cbffd7))
* **deps:** update node.js to 35a5dd7 ([f656d85](https://gitlab.com/yellowhat-labs/bootstrap/commit/f656d85186df7837f04beb571471dcd9156a28eb))
* **deps:** update node.js to 96cc832 ([c9e7a34](https://gitlab.com/yellowhat-labs/bootstrap/commit/c9e7a34c55c79936b217923ff2307f11b264466b))
* **deps:** update node.js to ec878c7 ([3a89f1e](https://gitlab.com/yellowhat-labs/bootstrap/commit/3a89f1edd7d78ed5f3f4918123c0ceed3c22196a))
* **deps:** update node.js to v22.12.0 ([152f1aa](https://gitlab.com/yellowhat-labs/bootstrap/commit/152f1aae8463aafe137f2ae45a3283e071e4110e))
* **deps:** update quay.io/podman/stable:v5.3.1 docker digest to 1ed7e4b ([1b0846b](https://gitlab.com/yellowhat-labs/bootstrap/commit/1b0846bc7941be7ac3ce9d2451118fc9dbb0c673))

# [2.1.0](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.0.2...v2.1.0) (2024-12-03)


### Features

* use podman to build images ([36e7fc9](https://gitlab.com/yellowhat-labs/bootstrap/commit/36e7fc9e0a01fe38fdb397377137431498603168))


### Chore

* **deps:** lock file maintenance ([670914e](https://gitlab.com/yellowhat-labs/bootstrap/commit/670914e5d9b9abba4173417151452270ccd1ee94))
* **deps:** lock file maintenance ([dbfc973](https://gitlab.com/yellowhat-labs/bootstrap/commit/dbfc973514851a247c9430584dfd6608151bf354))
* **deps:** lock file maintenance ([06a1de7](https://gitlab.com/yellowhat-labs/bootstrap/commit/06a1de719065338168e8b21c7b44cc790b9bd470))
* **deps:** lock file maintenance ([75ee4e3](https://gitlab.com/yellowhat-labs/bootstrap/commit/75ee4e3b0d50669b40cbb67a87c63ee0bd3d0bc6))
* **deps:** lock file maintenance ([171a72c](https://gitlab.com/yellowhat-labs/bootstrap/commit/171a72c2e21c99aa07f6aac86608a524f83092d7))
* **deps:** lock file maintenance ([e05fd41](https://gitlab.com/yellowhat-labs/bootstrap/commit/e05fd4129eb8c4d0bec9eb4417227d6cba31c865))
* **deps:** lock file maintenance ([e9ce3c8](https://gitlab.com/yellowhat-labs/bootstrap/commit/e9ce3c8e84104eb8890d40df5a55ff4c57b7a8ad))
* **deps:** lock file maintenance ([9ac7d52](https://gitlab.com/yellowhat-labs/bootstrap/commit/9ac7d527035f352dd5d32b68f771ff30b00074e3))
* **deps:** lock file maintenance ([2df723c](https://gitlab.com/yellowhat-labs/bootstrap/commit/2df723cbedfe259587849df23bcda06d2043be97))
* **deps:** lock file maintenance ([ac886b3](https://gitlab.com/yellowhat-labs/bootstrap/commit/ac886b3da84ab01128cd3fb163ce072647e0dcfd))
* **deps:** lock file maintenance ([7f928d7](https://gitlab.com/yellowhat-labs/bootstrap/commit/7f928d7f7e02b68663fc8b67685a6014a93735ba))
* **deps:** lock file maintenance ([2689ecb](https://gitlab.com/yellowhat-labs/bootstrap/commit/2689ecb019f4e388405bcfab9e40cfad755ff8c7))
* **deps:** lock file maintenance ([9886851](https://gitlab.com/yellowhat-labs/bootstrap/commit/988685180ca4a5bf6d331da781897d2028041f21))
* **deps:** lock file maintenance ([a293fe0](https://gitlab.com/yellowhat-labs/bootstrap/commit/a293fe02c08359713119ac8d0cdc41e85772d742))
* **deps:** lock file maintenance ([c3fdffb](https://gitlab.com/yellowhat-labs/bootstrap/commit/c3fdffb4a549b534bb659ac7dc28b4bcb71d8562))
* **deps:** lock file maintenance ([1e116ac](https://gitlab.com/yellowhat-labs/bootstrap/commit/1e116aca454dbe3e65e92e2fd8d1252f4b76177b))
* **deps:** lock file maintenance ([f4c4c30](https://gitlab.com/yellowhat-labs/bootstrap/commit/f4c4c3053b29ecdde25f0bbb1f9697df169ae817))
* **deps:** lock file maintenance ([2d4c6fe](https://gitlab.com/yellowhat-labs/bootstrap/commit/2d4c6fecad0a790512a94d1f9217d9cbdf56423a))
* **deps:** lock file maintenance ([07ac4f4](https://gitlab.com/yellowhat-labs/bootstrap/commit/07ac4f4ba0b1afd9b6aca75c3d634e48810c5e4b))
* **deps:** lock file maintenance ([9736099](https://gitlab.com/yellowhat-labs/bootstrap/commit/9736099ba461971678bc1d1bdc15ec41c03b35a8))
* **deps:** lock file maintenance ([d0e7e22](https://gitlab.com/yellowhat-labs/bootstrap/commit/d0e7e222ff8653bb3834965a066982054618b228))
* **deps:** update dependency bandit to v1.8.0 ([986b7ce](https://gitlab.com/yellowhat-labs/bootstrap/commit/986b7ce53e7eb1a104f1b858d5fec90e4612e89d))
* **deps:** update dependency crate-ci/typos to v1.28.0 ([1bc3571](https://gitlab.com/yellowhat-labs/bootstrap/commit/1bc3571abcf3d730ba48d89f0aacdb2e374feb0f))
* **deps:** update dependency crate-ci/typos to v1.28.1 ([497baeb](https://gitlab.com/yellowhat-labs/bootstrap/commit/497baebaf43997fbe747add0fd44e986008326ec))
* **deps:** update dependency crate-ci/typos to v1.28.2 ([abb7f4c](https://gitlab.com/yellowhat-labs/bootstrap/commit/abb7f4c76fc90757da91f7339635d29e456f7ded))
* **deps:** update dependency pulumi-gitlab to v8.6.1 ([69b3d37](https://gitlab.com/yellowhat-labs/bootstrap/commit/69b3d37190b945d1d7b24731da675cd3743e5358))
* **deps:** update dependency pylint to v3.3.2 ([0edece5](https://gitlab.com/yellowhat-labs/bootstrap/commit/0edece5011d06cb899003a1c06ed739684b45b58))
* **deps:** update dependency renovate to v39.20.3 ([e382647](https://gitlab.com/yellowhat-labs/bootstrap/commit/e382647f607b44c604924b4e6a74790c1b2c1ff4))
* **deps:** update dependency renovate to v39.21.0 ([822dbc7](https://gitlab.com/yellowhat-labs/bootstrap/commit/822dbc7f527ae8e8d3cb24f20e4ac95f42078136))
* **deps:** update dependency renovate to v39.23.0 ([3f31701](https://gitlab.com/yellowhat-labs/bootstrap/commit/3f3170105a2924c4dad44b69b6e8841690f0e564))
* **deps:** update dependency renovate to v39.25.3 ([520777d](https://gitlab.com/yellowhat-labs/bootstrap/commit/520777dd53683095868d4f289effd94529b48f8b))
* **deps:** update dependency renovate to v39.26.2 ([1c81871](https://gitlab.com/yellowhat-labs/bootstrap/commit/1c8187124095c24aea586035a5789eb8202526e2))
* **deps:** update dependency renovate to v39.27.0 ([0cbf206](https://gitlab.com/yellowhat-labs/bootstrap/commit/0cbf206686e58b0f682ecff0fa181676ed1e2e68))
* **deps:** update dependency renovate to v39.30.0 ([a9de821](https://gitlab.com/yellowhat-labs/bootstrap/commit/a9de821c4d0da77b949c2e6654ac38b67950a4e9))
* **deps:** update dependency renovate to v39.31.0 ([e997f51](https://gitlab.com/yellowhat-labs/bootstrap/commit/e997f51bb9a7a69ffc0a971068b785347a88b4ba))
* **deps:** update dependency renovate to v39.31.4 ([919c4f5](https://gitlab.com/yellowhat-labs/bootstrap/commit/919c4f5cbce9b85d5eb11aad7e0ee1e89f6c40d1))
* **deps:** update dependency renovate to v39.33.0 ([25447d0](https://gitlab.com/yellowhat-labs/bootstrap/commit/25447d01e25a061e4116c5683ad1b79d4e27ef49))
* **deps:** update dependency renovate to v39.34.0 ([f4c9b12](https://gitlab.com/yellowhat-labs/bootstrap/commit/f4c9b12193638abcbbddb7f477a7f2c029fc3a8b))
* **deps:** update dependency renovate to v39.34.1 ([198f8ed](https://gitlab.com/yellowhat-labs/bootstrap/commit/198f8ed964dc6cf3e178a42c6f494e8c25cf81e3))
* **deps:** update dependency renovate to v39.41.0 ([b756114](https://gitlab.com/yellowhat-labs/bootstrap/commit/b756114cfb47e20969beb486d5100a40554262f3))
* **deps:** update dependency renovate to v39.42.2 ([427cc8f](https://gitlab.com/yellowhat-labs/bootstrap/commit/427cc8ffebe214cb5b7a9624413578a0490cdf90))
* **deps:** update dependency renovate to v39.42.4 ([218a2da](https://gitlab.com/yellowhat-labs/bootstrap/commit/218a2daf146fcbf1a8213bf45ae51ba3e8212eae))
* **deps:** update dependency renovate to v39.45.0 ([ec1fc02](https://gitlab.com/yellowhat-labs/bootstrap/commit/ec1fc023b07af5e19fdb38d1463470cb9225acdb))
* **deps:** update dependency renovate to v39.46.1 ([f671cf7](https://gitlab.com/yellowhat-labs/bootstrap/commit/f671cf78e58b75425c86c00a0674d9ce8ec61a6d))
* **deps:** update dependency renovate to v39.48.0 ([38de892](https://gitlab.com/yellowhat-labs/bootstrap/commit/38de892a864556c52272e09949c47b9c7d303ede))
* **deps:** update dependency ruff to v0.8.0 ([d102192](https://gitlab.com/yellowhat-labs/bootstrap/commit/d1021925f9abc7ef79a8a2d927562c4cac94c399))
* **deps:** update dependency ruff to v0.8.1 ([cdfb999](https://gitlab.com/yellowhat-labs/bootstrap/commit/cdfb9994e825a6ef67fcbeca3b2cc96014d18a58))
* **deps:** update dependency typescript to v5.7.2 ([13ad8eb](https://gitlab.com/yellowhat-labs/bootstrap/commit/13ad8eb431f9dff7503d4753ac0e895ab64bea78))
* **deps:** update docker.io/nixos/nix docker tag to v2.25.3 ([3a9604a](https://gitlab.com/yellowhat-labs/bootstrap/commit/3a9604ab666f2400dc174a35370795e26bf7096d))
* **deps:** update docker.io/pulumi/pulumi-python docker tag to v3.142.0 ([c85db58](https://gitlab.com/yellowhat-labs/bootstrap/commit/c85db584b42bf0b79fb521decbf446c7926ebf4b))
* **deps:** update docker.io/python:3.13.0 docker digest to 22723cf ([d3509db](https://gitlab.com/yellowhat-labs/bootstrap/commit/d3509db6dab2785c5fccd3f90736fc676dce4f58))
* **deps:** update docker.io/python:3.13.0-slim docker digest to 0de8181 ([66baf37](https://gitlab.com/yellowhat-labs/bootstrap/commit/66baf37b3b9a6415a80229ec2be0cecef1fad7c6))
* **deps:** update docker.io/python:3.13.0-slim docker digest to f2d472e ([a62cb96](https://gitlab.com/yellowhat-labs/bootstrap/commit/a62cb96913fb302be722ee0ffe44fd5c20910086))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.97 ([e07a429](https://gitlab.com/yellowhat-labs/bootstrap/commit/e07a429f051bc6d4720cab6def565511ae286024))
* **deps:** update ghcr.io/igorshubovych/markdownlint-cli docker tag to v0.43.0 ([a0f941f](https://gitlab.com/yellowhat-labs/bootstrap/commit/a0f941f7c8948d04ff544f2598cb10c4b999d94b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.20.3 ([e695903](https://gitlab.com/yellowhat-labs/bootstrap/commit/e6959039ad128b59360e90834ec1a39cb8257d49))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.21.0 ([92948c1](https://gitlab.com/yellowhat-labs/bootstrap/commit/92948c1058cb37e542ba3f9d818b8f6d054e8c48))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.23.0 ([db918ac](https://gitlab.com/yellowhat-labs/bootstrap/commit/db918acc4539a047ea7605412e9cb7753e61370f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.25.3 ([94c4a98](https://gitlab.com/yellowhat-labs/bootstrap/commit/94c4a98f4dfd0ea9c846620821cb8b6b139e36e8))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.26.1 ([2090498](https://gitlab.com/yellowhat-labs/bootstrap/commit/209049820d03f4d61755a405c656c45371548c6d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.26.2 ([0e9a2f1](https://gitlab.com/yellowhat-labs/bootstrap/commit/0e9a2f10c1d0539ddc43ca2bbb5b10c5ec359020))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.26.3 ([367e326](https://gitlab.com/yellowhat-labs/bootstrap/commit/367e326e2d672dd361bf549b02dad7886f6db5ce))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.27.0 ([0ad7121](https://gitlab.com/yellowhat-labs/bootstrap/commit/0ad71211dcd09eb0146ad33bdc626548ece30b67))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.29.0 ([8606c85](https://gitlab.com/yellowhat-labs/bootstrap/commit/8606c857d9f2af54ac0fa15bea4465665cf020e5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.30.0 ([d034887](https://gitlab.com/yellowhat-labs/bootstrap/commit/d034887d34396d425b0a1d0e6c95abffb0fb7c83))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.31.0 ([020e531](https://gitlab.com/yellowhat-labs/bootstrap/commit/020e53173997723eeb07708e09b995758c0b1b4b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.31.4 ([97d67e3](https://gitlab.com/yellowhat-labs/bootstrap/commit/97d67e3050a93d0396c9e7429118b64e02fdd8c9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.33.0 ([06d75b8](https://gitlab.com/yellowhat-labs/bootstrap/commit/06d75b83ba9d265d99019e750e4d2c0b41846229))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.34.0 ([943fb64](https://gitlab.com/yellowhat-labs/bootstrap/commit/943fb640778f08936e985c612c7e5b47fa77fee4))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.38.0 ([3457d60](https://gitlab.com/yellowhat-labs/bootstrap/commit/3457d60e9acd77e6ad394cf799fedd2cbbe1ef98))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.41.0 ([6f0ff2c](https://gitlab.com/yellowhat-labs/bootstrap/commit/6f0ff2ce9c220c606332fdedcdc395033536ebf3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.42.2 ([d79d6d6](https://gitlab.com/yellowhat-labs/bootstrap/commit/d79d6d6f702b4c20918084ef3538894754f351a9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.42.4 ([e2d0244](https://gitlab.com/yellowhat-labs/bootstrap/commit/e2d0244e09a91b0187058186d64b662e0c2109a9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.45.0 ([daff179](https://gitlab.com/yellowhat-labs/bootstrap/commit/daff179641c4f1ba896779cee66cc9950915f9b7))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.46.1 ([120f91a](https://gitlab.com/yellowhat-labs/bootstrap/commit/120f91a6722c791ad4f011e5eb975a78980b8e08))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.48.0 ([037acc6](https://gitlab.com/yellowhat-labs/bootstrap/commit/037acc61fcba8ceef48aa9938fe4f71fd240962d))
* **deps:** update node.js to cb24453 ([a347074](https://gitlab.com/yellowhat-labs/bootstrap/commit/a347074714ef4fb976fc49e1914cb5c65276ca09))
* **deps:** update node.js to fd453a2 ([91c5f7c](https://gitlab.com/yellowhat-labs/bootstrap/commit/91c5f7c0af2ab1becf63cf2a335bd29b880033cf))
* **deps:** update registry.gitlab.com/gitlab-org/cli docker tag to v1.50.0 ([c943245](https://gitlab.com/yellowhat-labs/bootstrap/commit/c943245ce12ed9ba7207a3611ccb30d756ae5e7b))
* **pulumi|Dockerfile:** small refactor ([1a26e6b](https://gitlab.com/yellowhat-labs/bootstrap/commit/1a26e6b2a62731db7938b79850f879cd5e3989b4))
* **pulumi:** add kaos ([fe71b3c](https://gitlab.com/yellowhat-labs/bootstrap/commit/fe71b3c523b0ec24cf45496997aee0484e002559))
* **pulumi:** organize start.sh ([d3204aa](https://gitlab.com/yellowhat-labs/bootstrap/commit/d3204aa44c12d1fc3dcf9c2dfa97b56b52142360))


### deps

* update dependency @semantic-release/gitlab to v13.2.2 ([d385e2a](https://gitlab.com/yellowhat-labs/bootstrap/commit/d385e2ad64fe3d13ac852cdbbfe5b7f169296389))


### Continuous Integration

* manage GitLab repositories using Pulumi ([4e30e74](https://gitlab.com/yellowhat-labs/bootstrap/commit/4e30e748fa52eb49202259403472585c26b3469f))
* preview:comment job must use rules from .infra-common ([35a64ed](https://gitlab.com/yellowhat-labs/bootstrap/commit/35a64ed78bc2abdffaac1d234c523a8c73346975))
* **pulumi|up:** only run up if there are changes ([c17afb3](https://gitlab.com/yellowhat-labs/bootstrap/commit/c17afb372a6478cdc428ccb960040d70bf36a258))
* **pulumi:** run preview only in MRs ([f0d52ac](https://gitlab.com/yellowhat-labs/bootstrap/commit/f0d52ac22be40098648299c50068c102a9a13670))
* **renovate:** add kaos repo ([5569b54](https://gitlab.com/yellowhat-labs/bootstrap/commit/5569b545f21ec1eb7ebad07d17bdb2b58209b347))
* **renovate:** fetch pulumi changelog from GitHub ([810e81a](https://gitlab.com/yellowhat-labs/bootstrap/commit/810e81a7c9dc52b49e997bb2fa53fb8f3816c527))
* **renovate:** sync ([e952f65](https://gitlab.com/yellowhat-labs/bootstrap/commit/e952f65a5e53a737d5708e008bc8be283cbcbeaa))
* **renovate:** use a common resource_group ([96331d4](https://gitlab.com/yellowhat-labs/bootstrap/commit/96331d4be35a30f71e0a9983ccb48fd89dafc8d8))

## [2.0.2](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.0.1...v2.0.2) (2024-11-18)


### Chore

* **deps:** lock file maintenance ([6189ed0](https://gitlab.com/yellowhat-labs/bootstrap/commit/6189ed0e4da43bbfbee5b67e63aec57b1f2f3d19))
* **deps:** lock file maintenance ([7d9022e](https://gitlab.com/yellowhat-labs/bootstrap/commit/7d9022ee5e1a67eb8ec188a823194c95ed13b1ef))
* **deps:** pin docker.io/bash docker tag to bac7f7a ([d9135f2](https://gitlab.com/yellowhat-labs/bootstrap/commit/d9135f2ea771118cfa6cf48199aa42437977ce5c))
* **deps:** update dependency renovate to v39.18.1 ([771102e](https://gitlab.com/yellowhat-labs/bootstrap/commit/771102ee86d31414bbd7f31cfa7ca785d934065a))
* **deps:** update dependency renovate to v39.19.1 ([83fca21](https://gitlab.com/yellowhat-labs/bootstrap/commit/83fca21d42771dfb08b0ff1d2001446c4a4ea398))
* **deps:** update docker.io/bash docker tag to v5 ([8fe4fb5](https://gitlab.com/yellowhat-labs/bootstrap/commit/8fe4fb54f178f3862f3c1182243abcf593c0c8bf))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.18.1 ([89dcc2d](https://gitlab.com/yellowhat-labs/bootstrap/commit/89dcc2d934ee2f313f142461464e2663e54226df))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.19.1 ([606b418](https://gitlab.com/yellowhat-labs/bootstrap/commit/606b418bb69f70f8c76140eb6f6f3a9f4838106e))


### Bug Fixes

* **ci|nix:** run if exists not changes ([a83727e](https://gitlab.com/yellowhat-labs/bootstrap/commit/a83727e1c2a041edf19ad09cba7caa179fe624ed))


### Continuous Integration

* **renovate:** add workstation repo ([f1d1599](https://gitlab.com/yellowhat-labs/bootstrap/commit/f1d159942f362e7148b3b2349e787c4dbbc86f40))

## [2.0.1](https://gitlab.com/yellowhat-labs/bootstrap/compare/v2.0.0...v2.0.1) (2024-11-15)


### Chore

* **deps:** lock file maintenance ([0653c90](https://gitlab.com/yellowhat-labs/bootstrap/commit/0653c903ea6877068489d8cded20eaa6ecd48564))
* **deps:** lock file maintenance ([7084155](https://gitlab.com/yellowhat-labs/bootstrap/commit/70841550626b73e276d5c11c69f7ab6bdc631cc5))
* **deps:** update dependency renovate to v39.17.0 ([8b4912b](https://gitlab.com/yellowhat-labs/bootstrap/commit/8b4912b96f66066437b7c6ae107ca5a10c542156))
* **deps:** update dependency ruff to v0.7.4 ([49229c3](https://gitlab.com/yellowhat-labs/bootstrap/commit/49229c37ec57058ca03627dd00712309bfcf01c3))
* **deps:** update docker.io/python:3.13.0 docker digest to bc78d3c ([53a8655](https://gitlab.com/yellowhat-labs/bootstrap/commit/53a8655781044767a435d08b388302b564196787))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.15.4 ([737bae9](https://gitlab.com/yellowhat-labs/bootstrap/commit/737bae9ca8836eec86f68534a70f6b29bd117c0b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.17.0 ([50d62cb](https://gitlab.com/yellowhat-labs/bootstrap/commit/50d62cbd98ec6d4125536f6e9b46616e41c9bd92))
* **deps:** update registry.gitlab.com/gitlab-org/cli docker tag to v1.49.0 ([76459f8](https://gitlab.com/yellowhat-labs/bootstrap/commit/76459f81d47008c567a098a63bf78b10c5791fc7))


### Bug Fixes

* remove pyflakes as redundant ([a7191f5](https://gitlab.com/yellowhat-labs/bootstrap/commit/a7191f5eca14cbcd94d863285ee7918fba8d771e))
* **ruff:** target py313 ([d1b384b](https://gitlab.com/yellowhat-labs/bootstrap/commit/d1b384b4f738d9e5876084c9420d4b9d8849dd14))

# [2.0.0](https://gitlab.com/yellowhat-labs/bootstrap/compare/v1.12.4...v2.0.0) (2024-11-14)


### Chore

* **deps:** lock file maintenance ([60059b6](https://gitlab.com/yellowhat-labs/bootstrap/commit/60059b6948136556473ed0f5af7cd561404b2e80))
* **deps:** lock file maintenance ([48298f1](https://gitlab.com/yellowhat-labs/bootstrap/commit/48298f1c8167a7982af76681875181cfe5d3c195))
* **deps:** lock file maintenance ([c9be79b](https://gitlab.com/yellowhat-labs/bootstrap/commit/c9be79bcf599f8c143986a34a226db25f7bff9cc))
* **deps:** lock file maintenance ([db1631f](https://gitlab.com/yellowhat-labs/bootstrap/commit/db1631fa9f318c16c32e8c8e9b501ba57ce82fe9))
* **deps:** lock file maintenance ([7e295c8](https://gitlab.com/yellowhat-labs/bootstrap/commit/7e295c8bb2864fa967d9e545ca62e0bed6bc5709))
* **deps:** lock file maintenance ([66d6ddd](https://gitlab.com/yellowhat-labs/bootstrap/commit/66d6ddda719232c2f4fdb623fe5fb7db4e9b5ab2))
* **deps:** lock file maintenance ([0357555](https://gitlab.com/yellowhat-labs/bootstrap/commit/0357555beb184b51316c7594ba5048f527882e58))
* **deps:** lock file maintenance ([ca3db65](https://gitlab.com/yellowhat-labs/bootstrap/commit/ca3db65fed1341c7a6b5f9e41ebdb4466e21df4f))
* **deps:** lock file maintenance ([2dfa091](https://gitlab.com/yellowhat-labs/bootstrap/commit/2dfa091233e0a59a01576eec24bc925b81e6d4ce))
* **deps:** lock file maintenance ([82a84e6](https://gitlab.com/yellowhat-labs/bootstrap/commit/82a84e600ade3cb6a6f09ac3587f777a7a8c496f))
* **deps:** lock file maintenance ([e435564](https://gitlab.com/yellowhat-labs/bootstrap/commit/e43556469e3cb4f58a4aab684e3b1606281434c3))
* **deps:** lock file maintenance ([d4cddcc](https://gitlab.com/yellowhat-labs/bootstrap/commit/d4cddcc215aaa5c7f114030af231a9b36321cebd))
* **deps:** lock file maintenance ([a9831a1](https://gitlab.com/yellowhat-labs/bootstrap/commit/a9831a1ae5fe562329d5685054cd944947bcf9e0))
* **deps:** lock file maintenance ([3bf6942](https://gitlab.com/yellowhat-labs/bootstrap/commit/3bf69428d8ab139d3a15ce7ed13caeb1368c629a))
* **deps:** lock file maintenance ([03d4b5b](https://gitlab.com/yellowhat-labs/bootstrap/commit/03d4b5be0175469a480c8e4cfc94cf357acadffe))
* **deps:** lock file maintenance ([839155c](https://gitlab.com/yellowhat-labs/bootstrap/commit/839155cb5fb0c6ff2864aeda662e51e5ea8ac4d4))
* **deps:** lock file maintenance ([29dccdd](https://gitlab.com/yellowhat-labs/bootstrap/commit/29dccdd625918a04a8957cb41c25a05b2c4961bd))
* **deps:** lock file maintenance ([956b863](https://gitlab.com/yellowhat-labs/bootstrap/commit/956b86324e046b1b2d883480cc169f959194242b))
* **deps:** lock file maintenance ([2b8e18e](https://gitlab.com/yellowhat-labs/bootstrap/commit/2b8e18ec8ee45c91d44593261652d781fe0a58a0))
* **deps:** lock file maintenance ([7072d38](https://gitlab.com/yellowhat-labs/bootstrap/commit/7072d388e86126e8aa7e59dc92a4c4df500829a9))
* **deps:** lock file maintenance ([8bc9ecc](https://gitlab.com/yellowhat-labs/bootstrap/commit/8bc9ecc30531b9743eebe4597992bcafcd7fcdc5))
* **deps:** lock file maintenance ([bdffb40](https://gitlab.com/yellowhat-labs/bootstrap/commit/bdffb405abd599b540e923f631c559e480453e5f))
* **deps:** update dependency crate-ci/typos to v1.27.0 ([5c7f053](https://gitlab.com/yellowhat-labs/bootstrap/commit/5c7f053b7ae6962f93d769b82ced2de1fefd32bb))
* **deps:** update dependency crate-ci/typos to v1.27.1 ([bb1919f](https://gitlab.com/yellowhat-labs/bootstrap/commit/bb1919f55d17e1b37e18d446da2022323b3e63d3))
* **deps:** update dependency crate-ci/typos to v1.27.2 ([dee5869](https://gitlab.com/yellowhat-labs/bootstrap/commit/dee5869b89f6385f01a45f0a019c382b5a6e883b))
* **deps:** update dependency crate-ci/typos to v1.27.3 ([fd246e5](https://gitlab.com/yellowhat-labs/bootstrap/commit/fd246e57f23d799ba1f2607cae69b18b4dd0e22e))
* **deps:** update dependency renovate to v38.133.4 ([378e514](https://gitlab.com/yellowhat-labs/bootstrap/commit/378e51438a75dd103319a26a505f8afb5c864263))
* **deps:** update dependency renovate to v38.139.1 ([33175a2](https://gitlab.com/yellowhat-labs/bootstrap/commit/33175a228848d0b07c2ff01b098aa8f1fbb99dd5))
* **deps:** update dependency renovate to v38.140.2 ([d52c4f9](https://gitlab.com/yellowhat-labs/bootstrap/commit/d52c4f9e18c1486cb0c01660eca6ebf2f0a88fe2))
* **deps:** update dependency renovate to v38.142.0 ([db329cb](https://gitlab.com/yellowhat-labs/bootstrap/commit/db329cb13106f80f63e260d5524f89f04b9f3a16))
* **deps:** update dependency renovate to v38.142.1 ([687d7fd](https://gitlab.com/yellowhat-labs/bootstrap/commit/687d7fdd87bff93b76a469b4f69016e7cd240a62))
* **deps:** update dependency renovate to v38.142.2 ([20118a2](https://gitlab.com/yellowhat-labs/bootstrap/commit/20118a2608d1d7cc12aa0ee994a9b23a7a115774))
* **deps:** update dependency renovate to v38.142.4 ([665bfa1](https://gitlab.com/yellowhat-labs/bootstrap/commit/665bfa1fc58ac6e3a4a7ccc9920386aad4a76467))
* **deps:** update dependency renovate to v39 ([770b383](https://gitlab.com/yellowhat-labs/bootstrap/commit/770b383bb316d66eaa1e596022e41abbd28e3ecf))
* **deps:** update dependency renovate to v39.0.2 ([0ba8bca](https://gitlab.com/yellowhat-labs/bootstrap/commit/0ba8bca764ea09399c36628ed9a0bce14d3f8c12))
* **deps:** update dependency renovate to v39.10.1 ([0e91f19](https://gitlab.com/yellowhat-labs/bootstrap/commit/0e91f1926ed394b31d3f828c345b0e3984d048c3))
* **deps:** update dependency renovate to v39.11.7 ([8d4ce7d](https://gitlab.com/yellowhat-labs/bootstrap/commit/8d4ce7d76c18f479b75eb3485ece6be89bbfa2f1))
* **deps:** update dependency renovate to v39.14.1 ([3ea2c25](https://gitlab.com/yellowhat-labs/bootstrap/commit/3ea2c253ddeba10c4a002e5b52371d2f470f16bb))
* **deps:** update dependency renovate to v39.15.2 ([8ab4859](https://gitlab.com/yellowhat-labs/bootstrap/commit/8ab485946496f063ba0a2838b46f579f530551b0))
* **deps:** update dependency renovate to v39.7.0 ([238ec4e](https://gitlab.com/yellowhat-labs/bootstrap/commit/238ec4e63a49951e9fb56daad4d7a8b10dff3adc))
* **deps:** update dependency renovate to v39.7.1 ([65c6647](https://gitlab.com/yellowhat-labs/bootstrap/commit/65c6647b6312021d9e10d7fcf7a4ed4228661a78))
* **deps:** update dependency renovate to v39.9.0 ([640c1c3](https://gitlab.com/yellowhat-labs/bootstrap/commit/640c1c386832665c23d7201b955d7c262092c28f))
* **deps:** update dependency renovate to v39.9.2 ([f901db0](https://gitlab.com/yellowhat-labs/bootstrap/commit/f901db0d208a1732133a3c5daf9bcb5ce1970391))
* **deps:** update dependency ruff to v0.7.2 ([bac75f9](https://gitlab.com/yellowhat-labs/bootstrap/commit/bac75f964740fcf9b9a7f85059a16c480bb5b184))
* **deps:** update dependency ruff to v0.7.3 ([c044058](https://gitlab.com/yellowhat-labs/bootstrap/commit/c044058eac3cdebab21ce911359674706217a464))
* **deps:** update docker.io/alpine:edge docker digest to 8431297 ([c27a695](https://gitlab.com/yellowhat-labs/bootstrap/commit/c27a69527f48f58fbd58679f00a876b191ab7f55))
* **deps:** update docker.io/alpine:latest docker digest to 1e42bbe ([1c169ff](https://gitlab.com/yellowhat-labs/bootstrap/commit/1c169ff8c73a8e280f6e809b0726c545d775a27a))
* **deps:** update docker.io/nixos/nix docker tag to v2.24.10 ([bc39cbf](https://gitlab.com/yellowhat-labs/bootstrap/commit/bc39cbf1b40f8f8030fc9f77dd9863d8f29a618a))
* **deps:** update docker.io/nixos/nix docker tag to v2.25.0 ([182317f](https://gitlab.com/yellowhat-labs/bootstrap/commit/182317f2b5ec06dcb7a1922990a7927d3e7cc106))
* **deps:** update docker.io/nixos/nix docker tag to v2.25.1 ([13b1529](https://gitlab.com/yellowhat-labs/bootstrap/commit/13b15295e8bddcae2b5855ac5eb0b6a5635c75fd))
* **deps:** update docker.io/nixos/nix docker tag to v2.25.2 ([f068f63](https://gitlab.com/yellowhat-labs/bootstrap/commit/f068f637d2a6904f0b93a59aa0dfd787271a4dc2))
* **deps:** update docker.io/python:3.13.0 docker digest to 5b5dd1a ([650b3e7](https://gitlab.com/yellowhat-labs/bootstrap/commit/650b3e7d4367e9b50b65461a814ea517a7af90e9))
* **deps:** update docker.io/python:3.13.0 docker digest to 7cbaafd ([fa5fa1a](https://gitlab.com/yellowhat-labs/bootstrap/commit/fa5fa1ae9536a6ab4ae7610befc28dba1c88e670))
* **deps:** update docker.io/python:3.13.0 docker digest to a6ed02f ([13a0493](https://gitlab.com/yellowhat-labs/bootstrap/commit/13a049354995e561a0a5e7cd8401fcf9202f4ea8))
* **deps:** update docker.io/python:3.13.0-alpine docker digest to d4daf85 ([7eba8cc](https://gitlab.com/yellowhat-labs/bootstrap/commit/7eba8ccc7b75c08a4d0251399ec0132af64804e1))
* **deps:** update docker.io/python:3.13.0-alpine docker digest to ee60f1f ([4a21972](https://gitlab.com/yellowhat-labs/bootstrap/commit/4a21972c50857b64a4177ab5a91512a987379306))
* **deps:** update docker.io/python:3.13.0-alpine docker digest to fcbcbbe ([d8732cc](https://gitlab.com/yellowhat-labs/bootstrap/commit/d8732cca7e383c49832a5d183313ffd212b3ed38))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.94 ([ba0fee6](https://gitlab.com/yellowhat-labs/bootstrap/commit/ba0fee6a6dff768a69035662f8ed329ab2c20504))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.95 ([25a9d76](https://gitlab.com/yellowhat-labs/bootstrap/commit/25a9d763be7c0a09fc9732c024b6d8f340468192))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.96 ([df25d8d](https://gitlab.com/yellowhat-labs/bootstrap/commit/df25d8d4f176efcf46b50add62f3461d4ecb0e9b))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.21.2 ([7e0bffe](https://gitlab.com/yellowhat-labs/bootstrap/commit/7e0bffe23fa5eb933ccd2ca908cfc5692f707a85))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.133.4 ([7252505](https://gitlab.com/yellowhat-labs/bootstrap/commit/7252505b6c43710cb1bfc36a9e2fc3a82d4e1ad0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.134.0 ([2db7844](https://gitlab.com/yellowhat-labs/bootstrap/commit/2db78443eb8ebad6eb8ba51877350a3d20556d54))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.135.2 ([1b9b138](https://gitlab.com/yellowhat-labs/bootstrap/commit/1b9b138b75a2cf6ea0898380f6cea500b7413aed))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.139.1 ([f1625b0](https://gitlab.com/yellowhat-labs/bootstrap/commit/f1625b09a0d60b22c2b70c0867210a1bb212e284))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.140.2 ([a7ad104](https://gitlab.com/yellowhat-labs/bootstrap/commit/a7ad104c69ed967288c01037f31fac384360635e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.142.0 ([086d15d](https://gitlab.com/yellowhat-labs/bootstrap/commit/086d15d3a62d2abea3136d89d42b261715fcc330))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.142.1 ([e1e33e4](https://gitlab.com/yellowhat-labs/bootstrap/commit/e1e33e40ea6341ae9e0be1a1ba0888a61e95ee55))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.142.2 ([a1d32ec](https://gitlab.com/yellowhat-labs/bootstrap/commit/a1d32ec75df2212e90fddc647cfbe2bb7248cf28))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.142.4 ([0048d55](https://gitlab.com/yellowhat-labs/bootstrap/commit/0048d55ff7c4635e3c3f7c8760efc73c8484a834))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.142.5 ([cfba6b6](https://gitlab.com/yellowhat-labs/bootstrap/commit/cfba6b6c59ce837cbf4d6bbed30a9cc445188626))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39 ([67aef32](https://gitlab.com/yellowhat-labs/bootstrap/commit/67aef32714110da901e14544c91749a432f09264))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.0.2 ([0a309e5](https://gitlab.com/yellowhat-labs/bootstrap/commit/0a309e57a57a435aabfab91e153c49640a44b12a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.0.3 ([eae0146](https://gitlab.com/yellowhat-labs/bootstrap/commit/eae01460e36b359e61be26be4f0e5a6c4e20c918))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.10.1 ([1590543](https://gitlab.com/yellowhat-labs/bootstrap/commit/15905438a1cc1312c56a422b6f5ce068ee814124))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.10.2 ([5e77ad2](https://gitlab.com/yellowhat-labs/bootstrap/commit/5e77ad257e8aae4af8784d8fb58914c72841057a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.11.6 ([51797da](https://gitlab.com/yellowhat-labs/bootstrap/commit/51797daf756739f8b404e6446167282c44a3c97c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.11.7 ([1ecfc6f](https://gitlab.com/yellowhat-labs/bootstrap/commit/1ecfc6f56c3de62082e5dc8ae8765e0729cb5407))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.14.1 ([23faff0](https://gitlab.com/yellowhat-labs/bootstrap/commit/23faff0fb6ab53e786f2c5e0ec6e371617d6c90d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.15.1 ([f727307](https://gitlab.com/yellowhat-labs/bootstrap/commit/f727307eb6bc41d7edbdaf695a264aaaf04d0266))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.15.2 ([f36dc3f](https://gitlab.com/yellowhat-labs/bootstrap/commit/f36dc3f6f7fa5ee089462675763412c4004bac40))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.4.0 ([ae0670c](https://gitlab.com/yellowhat-labs/bootstrap/commit/ae0670c966e35968269b0a8e5732ec2a0cc6abb3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.7.0 ([011d3a5](https://gitlab.com/yellowhat-labs/bootstrap/commit/011d3a510d1eb0e1104d455f3882561e4754bb0f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.7.1 ([c30594e](https://gitlab.com/yellowhat-labs/bootstrap/commit/c30594eb5f3355bac1d6c333a88693d7a1d9e44a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.7.5 ([5d1fedd](https://gitlab.com/yellowhat-labs/bootstrap/commit/5d1fedd50ab9b2c111edb271166984d996b49e93))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.8.0 ([8adcea2](https://gitlab.com/yellowhat-labs/bootstrap/commit/8adcea2ab94a068e6d8376daad7483b061a0a3a7))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.9.0 ([44d94bc](https://gitlab.com/yellowhat-labs/bootstrap/commit/44d94bc9a3d48cd88bfddd13c30e048a186726ee))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v39.9.2 ([14c98ce](https://gitlab.com/yellowhat-labs/bootstrap/commit/14c98ce6d0ab6052012ec324521fef8df4afd2ca))
* **deps:** update ghcr.io/tcort/markdown-link-check docker tag to v3.13.5 ([d434710](https://gitlab.com/yellowhat-labs/bootstrap/commit/d43471072769369ffdfa548648a0e898668b63e7))
* **deps:** update ghcr.io/tcort/markdown-link-check docker tag to v3.13.6 ([bcdcba0](https://gitlab.com/yellowhat-labs/bootstrap/commit/bcdcba0fdec23d7f5d477dca1fc1df5c95642323))
* **deps:** update node.js to 5c76d05 ([39b466b](https://gitlab.com/yellowhat-labs/bootstrap/commit/39b466bc8aa4e4a1f2499f5fd7458fadac4288b7))
* **deps:** update node.js to b64ced2 ([c2dec6c](https://gitlab.com/yellowhat-labs/bootstrap/commit/c2dec6c718133d4298134c55fbb7175fdf595642))
* **deps:** update node.js to db556c2 ([f8b5602](https://gitlab.com/yellowhat-labs/bootstrap/commit/f8b5602fc8f2f119c499d64341afef4a29c6ff05))
* **deps:** update node.js to dc8ba2f ([32eae31](https://gitlab.com/yellowhat-labs/bootstrap/commit/32eae3197927625f8abd8f401e7d30bab8d9a28b))
* **deps:** update node.js to f496dba ([2a33e1e](https://gitlab.com/yellowhat-labs/bootstrap/commit/2a33e1ee58ce1a478e6b08ae1fc276956e7ac831))
* **deps:** update node.js to v22.11.0 ([d9c1b77](https://gitlab.com/yellowhat-labs/bootstrap/commit/d9c1b7720b9315c93cfbfbecbb669d96ad3e3877))
* **renovate:** simply gitlabci.fileMatch ([75a83e6](https://gitlab.com/yellowhat-labs/bootstrap/commit/75a83e64eaf9d437bcae4751e7a796a9059feae4))


### Breaking Changes

* change repo name ([0fe670e](https://gitlab.com/yellowhat-labs/bootstrap/commit/0fe670ee69e06c54b438fadb7b9971b365639f11))


### Continuous Integration

* **renovate:** fedora 40 > 41 ([ed9a1f5](https://gitlab.com/yellowhat-labs/bootstrap/commit/ed9a1f536e664a66d44cb0035f569b6b2f8d8b69))
* **renovate:** use full image as it contains go, nix, npx ([ec4284c](https://gitlab.com/yellowhat-labs/bootstrap/commit/ec4284c3e71e96ff11ad6a4d31b49ffd9b7802f2))

## [1.12.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.12.3...v1.12.4) (2024-10-28)


### Chore

* **deps:** lock file maintenance ([fd933d3](https://gitlab.com/yellowhat-labs/utils/commit/fd933d3cc8fd65640227cd7d075820d486b283a7))
* **deps:** lock file maintenance ([65f5400](https://gitlab.com/yellowhat-labs/utils/commit/65f5400ead54056eb5fe945680134b8000823230))
* **deps:** lock file maintenance ([a809630](https://gitlab.com/yellowhat-labs/utils/commit/a809630bd71f18db2008f22d39340014313a7928))
* **deps:** lock file maintenance ([8381a89](https://gitlab.com/yellowhat-labs/utils/commit/8381a89aa7b3a0ff8988407bf3fdf5a5cdea0bec))
* **deps:** lock file maintenance ([2c147ee](https://gitlab.com/yellowhat-labs/utils/commit/2c147eecd8b5feed897df5e84bff57878b18ca88))
* **deps:** lock file maintenance ([0a09964](https://gitlab.com/yellowhat-labs/utils/commit/0a09964408a5344c2c89b34d9b9f51fca6851cac))
* **deps:** update dependency @types/jest to v29.5.14 ([8521c05](https://gitlab.com/yellowhat-labs/utils/commit/8521c05883e0d2e85fa25f84ae293e315628a99a))
* **deps:** update dependency blacken-docs to v1.19.1 ([e866806](https://gitlab.com/yellowhat-labs/utils/commit/e866806b2ea82308f18017ccd3d7ec661fdd746c))
* **deps:** update dependency crate-ci/typos to v1.26.1 ([5cc85b7](https://gitlab.com/yellowhat-labs/utils/commit/5cc85b7ea78af556b473679a26cb73f6b416644a))
* **deps:** update dependency crate-ci/typos to v1.26.8 ([5ded980](https://gitlab.com/yellowhat-labs/utils/commit/5ded980eaa22d5599461665a688eaf65943a83db))
* **deps:** update dependency mypy to v1.12.1 ([e1b84d1](https://gitlab.com/yellowhat-labs/utils/commit/e1b84d142846c501ce314ef880df90efde7028fc))
* **deps:** update dependency mypy to v1.13.0 ([f546a85](https://gitlab.com/yellowhat-labs/utils/commit/f546a8587aee27337db300140483e7e9145467ba))
* **deps:** update dependency pyupgrade to v3.19.0 ([1ffceb8](https://gitlab.com/yellowhat-labs/utils/commit/1ffceb8a0536c658ca922f8b61365621419bd4b8))
* **deps:** update dependency renovate to v38.128.3 ([3f495e2](https://gitlab.com/yellowhat-labs/utils/commit/3f495e2a6c2feb0064ddb25e3f8461c47b48eb01))
* **deps:** update dependency renovate to v38.128.6 ([cabc4de](https://gitlab.com/yellowhat-labs/utils/commit/cabc4de5c8cb3d49c1a64c45f760400958e9bd26))
* **deps:** update dependency renovate to v38.129.0 ([f19ebca](https://gitlab.com/yellowhat-labs/utils/commit/f19ebcac5c67291bff6741f847b945d1aab6aa54))
* **deps:** update dependency renovate to v38.129.2 ([a38b82a](https://gitlab.com/yellowhat-labs/utils/commit/a38b82a445fd10c6cc80459823cb26d1fd0fe439))
* **deps:** update dependency renovate to v38.130.0 ([b5decd4](https://gitlab.com/yellowhat-labs/utils/commit/b5decd471a3fbedcd9179c53b11341d82a815f03))
* **deps:** update dependency renovate to v38.133.0 ([45fae12](https://gitlab.com/yellowhat-labs/utils/commit/45fae12af382caaaac7a12faadb2a4a10b9d27c1))
* **deps:** update dependency ruff to v0.7.1 ([c934c4d](https://gitlab.com/yellowhat-labs/utils/commit/c934c4d39465d6dc22d888e778c8bae1b356aa62))
* **deps:** update docker.io/mvdan/shfmt docker tag to v3.10.0 ([0af3d1b](https://gitlab.com/yellowhat-labs/utils/commit/0af3d1baa64f72423b8437db597e693507a5190e))
* **deps:** update docker.io/python:3.13.0 docker digest to a31cbb4 ([7c31c65](https://gitlab.com/yellowhat-labs/utils/commit/7c31c65987fd22a32776af14a0edfc201519d90d))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.93 ([c236ef6](https://gitlab.com/yellowhat-labs/utils/commit/c236ef67f40d8a476e805a006aa7783c7639b6c5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.128.3 ([0154f2d](https://gitlab.com/yellowhat-labs/utils/commit/0154f2df535ed1bee2b798c452015b1fe22c3759))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.128.6 ([21abae1](https://gitlab.com/yellowhat-labs/utils/commit/21abae181bb38d25229105929934ffdf70b25139))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.129.0 ([0e09f98](https://gitlab.com/yellowhat-labs/utils/commit/0e09f98915a50c9066f2fbe46103d0f236498e88))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.129.2 ([7c262e6](https://gitlab.com/yellowhat-labs/utils/commit/7c262e697101676258f130ea870d519455454361))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.130.0 ([b0b8b75](https://gitlab.com/yellowhat-labs/utils/commit/b0b8b758383700d5d1032264da1319301575a9cd))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.130.2 ([81430e8](https://gitlab.com/yellowhat-labs/utils/commit/81430e873f714f1470f140a282074da03faa769a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.130.4 ([7506bb7](https://gitlab.com/yellowhat-labs/utils/commit/7506bb7053b096c949f88bf1f81752ad99934a29))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.133.0 ([89feb15](https://gitlab.com/yellowhat-labs/utils/commit/89feb153cc3766d5e11965389f22a2624d0f4a23))
* **deps:** update node.js to da53547 ([741790e](https://gitlab.com/yellowhat-labs/utils/commit/741790eb7fcd89d80761657b654236c5b9a89546))
* **deps:** update node.js to ea54557 ([6a295cd](https://gitlab.com/yellowhat-labs/utils/commit/6a295cda4eb1e6b7695a68b3a8288837f78fe357))


### Bug Fixes

* **deps:** update dependency semantic-release to v24.2.0 ([c7355e0](https://gitlab.com/yellowhat-labs/utils/commit/c7355e071af0be0a48f38e59d8c1ec69db0e314c))


### renovate

* scrape tag ([1eebc18](https://gitlab.com/yellowhat-labs/utils/commit/1eebc182444009a8b2746f5e4c82859f7958bf6e))

## [1.12.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.12.2...v1.12.3) (2024-10-19)


### Bug Fixes

* **deps:** update dependency semantic-release to v24.1.3 ([c505f8b](https://gitlab.com/yellowhat-labs/utils/commit/c505f8bd806e752cf28dfcb6c30dbc4e005a6329))


### Chore

* **deps:** lock file maintenance ([dcb360f](https://gitlab.com/yellowhat-labs/utils/commit/dcb360f739afa2d7a532a188904cf56e5de7a08c))
* **deps:** lock file maintenance ([3f93066](https://gitlab.com/yellowhat-labs/utils/commit/3f93066553f360d07c8ee83c889bcad554ef0543))
* **deps:** lock file maintenance ([c856d2c](https://gitlab.com/yellowhat-labs/utils/commit/c856d2c3f0069f5fe934fc078b0ca00e8622441b))
* **deps:** lock file maintenance ([91945dd](https://gitlab.com/yellowhat-labs/utils/commit/91945dd8b22655f773bb90cd5bf2c249aac70ece))
* **deps:** update dependency @biomejs/biome to v1.9.4 ([6d57f94](https://gitlab.com/yellowhat-labs/utils/commit/6d57f94c38935fb888ab5e6a3c96f79e79cde464))
* **deps:** update dependency renovate to v38.124.1 ([782e773](https://gitlab.com/yellowhat-labs/utils/commit/782e773ff81fcaf63c94dd4d21671640cd1458ae))
* **deps:** update dependency renovate to v38.124.3 ([ec7ea39](https://gitlab.com/yellowhat-labs/utils/commit/ec7ea391cf8e120b18a97805c2b2d30c9305f270))
* **deps:** update dependency renovate to v38.127.0 ([82c3f5d](https://gitlab.com/yellowhat-labs/utils/commit/82c3f5d5e8d058fe8fa38a49074ff3ae2d2d742d))
* **deps:** update dependency renovate to v38.128.1 ([c2d2382](https://gitlab.com/yellowhat-labs/utils/commit/c2d238271932021f0abc74162b7408cb88c9fa53))
* **deps:** update dependency ruff to v0.7.0 ([3ec98d4](https://gitlab.com/yellowhat-labs/utils/commit/3ec98d4ceb2717e7aa00bd429320de3b0b8f381e))
* **deps:** update docker.io/python:3.13.0 docker digest to 0a30160 ([a055ea9](https://gitlab.com/yellowhat-labs/utils/commit/a055ea98ad81c315d31d2b00fefaa387bfcdc03e))
* **deps:** update docker.io/python:3.13.0 docker digest to 3dd4610 ([7488b0e](https://gitlab.com/yellowhat-labs/utils/commit/7488b0e2b28930e68ef5d71e6e76838b57eb2b70))
* **deps:** update docker.io/python:3.13.0 docker digest to c590a36 ([3eb9b29](https://gitlab.com/yellowhat-labs/utils/commit/3eb9b294b4359a189f096a70f9e225897583fb88))
* **deps:** update docker.io/python:3.13.0-alpine docker digest to c38ead8 ([08ae41b](https://gitlab.com/yellowhat-labs/utils/commit/08ae41b8c3eee7c50261437fec9b86d67985e175))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.92 ([eec3d17](https://gitlab.com/yellowhat-labs/utils/commit/eec3d172877be8143fcdd1081e317a7bbb2ce4a3))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.21.1 ([edb7641](https://gitlab.com/yellowhat-labs/utils/commit/edb7641e0430a53018d3faf5798f4b023f7addce))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.124.1 ([7e5a370](https://gitlab.com/yellowhat-labs/utils/commit/7e5a370ec99c594fe67d37e91abe4c4608b348d5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.124.3 ([14ba05e](https://gitlab.com/yellowhat-labs/utils/commit/14ba05ee09e77b04c04ca77a5630ac30745a5c84))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.127.0 ([dc4dc4d](https://gitlab.com/yellowhat-labs/utils/commit/dc4dc4dd2628434c301cfb131f5d0ed1e5b067f8))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.128.1 ([2130001](https://gitlab.com/yellowhat-labs/utils/commit/2130001ece8fb9b6aa1562f7d3dad466573e15d5))
* **deps:** update node.js to 545b097 ([bd248eb](https://gitlab.com/yellowhat-labs/utils/commit/bd248eba4352e4a29fdc510bf2c3e61dd70cc1fb))
* **deps:** update node.js to f2f00f9 ([c625c20](https://gitlab.com/yellowhat-labs/utils/commit/c625c2016b919b4aefec4e69c92289a5612dcf68))
* **deps:** update node.js to f8ee7f5 ([c620e35](https://gitlab.com/yellowhat-labs/utils/commit/c620e356541c6606a895f0cdbf1b96819bb4625e))
* **deps:** update node.js to fc95a04 ([48c9f7f](https://gitlab.com/yellowhat-labs/utils/commit/48c9f7f110b938f2d3ccd11947ee7a8bd400efbf))
* **deps:** update node.js to v22.10.0 ([b0f6f13](https://gitlab.com/yellowhat-labs/utils/commit/b0f6f138cd62490dee1083ecab57cd5965c7af26))
* **deps:** update registry.gitlab.com/gitlab-org/cli docker tag to v1.48.0 ([0ff2294](https://gitlab.com/yellowhat-labs/utils/commit/0ff229436802e98f437c1a1219ca7e56344079cb))


### Continuous Integration

* **renovate:** sync ([7558e12](https://gitlab.com/yellowhat-labs/utils/commit/7558e1252eaefcaff13289ca1e935c487c1e8ba6))

## [1.12.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.12.1...v1.12.2) (2024-10-15)


### Bug Fixes

* **nix:** add flake-checker ([b5c35b3](https://gitlab.com/yellowhat-labs/utils/commit/b5c35b3f524be6389c9f27455549a690300e9362))


### Chore

* **deps:** lock file maintenance ([68f91e3](https://gitlab.com/yellowhat-labs/utils/commit/68f91e356872b9804fc0e3cd0fb31e14bab1c7eb))
* **deps:** lock file maintenance ([20722dc](https://gitlab.com/yellowhat-labs/utils/commit/20722dc7062d1abbdef41a4dcedfc8d239831764))
* **deps:** lock file maintenance ([7395aca](https://gitlab.com/yellowhat-labs/utils/commit/7395aca1aae1746cdef70f7cb72cc386fc575d09))
* **deps:** update dependency mypy to v1.12.0 ([5b11bd7](https://gitlab.com/yellowhat-labs/utils/commit/5b11bd78c79e6c68c9414ce1befad92dbdff626e))
* **deps:** update dependency pyupgrade to v3.18.0 ([07f5d2f](https://gitlab.com/yellowhat-labs/utils/commit/07f5d2f785154fcb4281ed51100ea35824a7d330))
* **deps:** update dependency renovate to v38.116.0 ([507390d](https://gitlab.com/yellowhat-labs/utils/commit/507390d93e09f4dc0e5edbf6cd8da755e03a2312))
* **deps:** update dependency renovate to v38.117.1 ([76237c5](https://gitlab.com/yellowhat-labs/utils/commit/76237c52b67d421ee0cb32eecaf3200a9e83c158))
* **deps:** update dependency renovate to v38.123.0 ([5b67ba8](https://gitlab.com/yellowhat-labs/utils/commit/5b67ba8a4d3d742a5dfea55f9243f087fcb88297))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.91 ([25e2821](https://gitlab.com/yellowhat-labs/utils/commit/25e28212a593b253594fce2a6245742e3946edd4))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.21.0 ([3c81351](https://gitlab.com/yellowhat-labs/utils/commit/3c8135182f54be03c220c4ab2125e4a9a21bcb0c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.116.0 ([f2a095c](https://gitlab.com/yellowhat-labs/utils/commit/f2a095c3320ef13a0c0fb3fe0437d28aedad9948))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.117.1 ([e8dcddd](https://gitlab.com/yellowhat-labs/utils/commit/e8dcddd7ec523664f276bf9f93a81acdb7ce5d4b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.123.0 ([cdd616a](https://gitlab.com/yellowhat-labs/utils/commit/cdd616a8f33665bdfcee0e9affb9642266a4709c))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v9 ([e8ac91d](https://gitlab.com/yellowhat-labs/utils/commit/e8ac91dfdad3fcdd31d1d2a74b10e180e50744c3))

## [1.12.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.12.0...v1.12.1) (2024-10-10)


### Chore

* **deps:** lock file maintenance ([80e7ff8](https://gitlab.com/yellowhat-labs/utils/commit/80e7ff8025988d4ff764d77d60ec0fcb3455d965))
* **deps:** lock file maintenance ([3fc7e2d](https://gitlab.com/yellowhat-labs/utils/commit/3fc7e2d638d7a516ef00753bd80c3f19df72b016))
* **deps:** lock file maintenance ([cb9f3be](https://gitlab.com/yellowhat-labs/utils/commit/cb9f3be8bec2f732896fc07ea80bd5c7071e7650))
* **deps:** lock file maintenance ([dffc2fa](https://gitlab.com/yellowhat-labs/utils/commit/dffc2fa046320bef39b3f3a6e9f699e0c53fa130))
* **deps:** lock file maintenance ([7d78d30](https://gitlab.com/yellowhat-labs/utils/commit/7d78d30721eade616073e005f1251d733bb64fd4))
* **deps:** update dependency @biomejs/biome to v1.9.3 ([a63aa0d](https://gitlab.com/yellowhat-labs/utils/commit/a63aa0d0b744b8237fc9fd23c20753045e16a254))
* **deps:** update dependency blacken-docs to v1.19.0 ([b6f6470](https://gitlab.com/yellowhat-labs/utils/commit/b6f6470012084a82f1ce25c28018a71c0178341c))
* **deps:** update dependency crate-ci/typos to v1.26.0 ([cad9383](https://gitlab.com/yellowhat-labs/utils/commit/cad938333846b2c1a60840260207a3167d8bb11f))
* **deps:** update dependency renovate to v38.106.3 ([783dd2a](https://gitlab.com/yellowhat-labs/utils/commit/783dd2aa62f5e2baabf27c53135f84b010fb7afe))
* **deps:** update dependency renovate to v38.110.2 ([15eafad](https://gitlab.com/yellowhat-labs/utils/commit/15eafad509170442056c0427a7c73eb183091d97))
* **deps:** update dependency renovate to v38.114.0 ([69eb816](https://gitlab.com/yellowhat-labs/utils/commit/69eb816b7bcd7cdccc01e04c1a879d1d615fe088))
* **deps:** update dependency renovate to v38.115.1 ([c935fbc](https://gitlab.com/yellowhat-labs/utils/commit/c935fbc2556025f11745564cc5529b74447ca38d))
* **deps:** update dependency ruff to v0.6.9 ([1c2f64d](https://gitlab.com/yellowhat-labs/utils/commit/1c2f64de18c77bf8950c8d4aef72897f308a940b))
* **deps:** update dependency typescript to v5.6.3 ([97e38e0](https://gitlab.com/yellowhat-labs/utils/commit/97e38e0ff38972561746dc090a6ae1289d4c33ee))
* **deps:** update dependency vulture to v2.13 ([5cddc8f](https://gitlab.com/yellowhat-labs/utils/commit/5cddc8f3946f82ee87a09e76aa3cfeeb1f05ef27))
* **deps:** update docker.io/pyfound/black docker tag to v24.10.0 ([0822493](https://gitlab.com/yellowhat-labs/utils/commit/0822493085cc1f81193d18859b506bc8dec884e4))
* **deps:** update docker.io/python docker tag to v3.12.7 ([fb71d58](https://gitlab.com/yellowhat-labs/utils/commit/fb71d58387382f816e875fae239647be44f26640))
* **deps:** update docker.io/python docker tag to v3.13.0 ([d09e8e0](https://gitlab.com/yellowhat-labs/utils/commit/d09e8e0e6658c850ded7403f898671321fec5722))
* **deps:** update docker.io/python:3.13.0 docker digest to 45803c3 ([4258571](https://gitlab.com/yellowhat-labs/utils/commit/42585713dbb7e232827737815993020b444cfb81))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.20.1 ([4b26b49](https://gitlab.com/yellowhat-labs/utils/commit/4b26b4929d36a20e46e80da13eeb4ad7b9a25c09))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.106.3 ([fb66f1a](https://gitlab.com/yellowhat-labs/utils/commit/fb66f1afe87d9daec6e2f33e988bd241df082bd6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.110.2 ([7ce39bf](https://gitlab.com/yellowhat-labs/utils/commit/7ce39bf68c8a32a09aa0d0a0609a2f9840fee401))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.111.0 ([7d94f1e](https://gitlab.com/yellowhat-labs/utils/commit/7d94f1ea8a7bc2ed34abaa531a5c7b7de213cde2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.114.0 ([b9536e9](https://gitlab.com/yellowhat-labs/utils/commit/b9536e9d0fd2f4ea2ac7589da386415929868b1b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.115.1 ([ac05cb3](https://gitlab.com/yellowhat-labs/utils/commit/ac05cb35f5f11204628a49a159b3fe5434118198))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v8.4.0 ([eb6956d](https://gitlab.com/yellowhat-labs/utils/commit/eb6956d52f6676388f92d6e4cfde6dad3d27ec49))
* **deps:** update registry.gitlab.com/gitlab-org/cli docker tag to v1.47.0 ([d7d7f02](https://gitlab.com/yellowhat-labs/utils/commit/d7d7f02f3d4fd99672941439686e888c28b03d19))


### Bug Fixes

* **semgrep:** use `scan` as `ci` does not run in MR ([c137d39](https://gitlab.com/yellowhat-labs/utils/commit/c137d39044fff0340e0746c72ee081f4f964f6c1))


### Continuous Integration

* **renovate:** sync ([4e7bc66](https://gitlab.com/yellowhat-labs/utils/commit/4e7bc660080749466da79eaf59f4e0291b23bcb3))

# [1.12.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.11.5...v1.12.0) (2024-10-01)


### Chore

* **deps:** lock file maintenance ([6144b34](https://gitlab.com/yellowhat-labs/utils/commit/6144b340e48f7f08c835e81f60d1000b4b36dd90))
* **deps:** lock file maintenance ([5089289](https://gitlab.com/yellowhat-labs/utils/commit/5089289a0b950a7d3b821a0b088d828d0b6845d0))
* **deps:** update dependency crate-ci/typos to v1.25.0 ([ba2cf91](https://gitlab.com/yellowhat-labs/utils/commit/ba2cf910923c473f3ec95debe681276e197afc62))
* **deps:** update dependency renovate to v38.101.1 ([6514e34](https://gitlab.com/yellowhat-labs/utils/commit/6514e34937920a3004b4451b53d8f51b5a91d0f6))
* **deps:** update dependency renovate to v38.106.0 ([c59527c](https://gitlab.com/yellowhat-labs/utils/commit/c59527c07d3cf22b29c8386a77b33d060dfc3dc7))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.101.1 ([3c3656b](https://gitlab.com/yellowhat-labs/utils/commit/3c3656bd03d605b1535bf65bd224bbe8adf181e5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.106.0 ([dfb54be](https://gitlab.com/yellowhat-labs/utils/commit/dfb54be70746e22cb6d66a3998087b75db6403be))
* README.md ([a60b738](https://gitlab.com/yellowhat-labs/utils/commit/a60b7382578d1e60744c5712d6ed27925d084196))
* update docs ([27d4907](https://gitlab.com/yellowhat-labs/utils/commit/27d4907a13f38374966473d83e6b95169c218036))


### Bug Fixes

* **semgrep:** exclude-rule ([f7cba48](https://gitlab.com/yellowhat-labs/utils/commit/f7cba48a1eef5110c07f080ff2523b71a3548a5c))
* typo ([f99fd7e](https://gitlab.com/yellowhat-labs/utils/commit/f99fd7e6ba7d5810c7ff3e1783e121ca34315bbc))


### Features

* run renovate-parsed from container image ([6b362a9](https://gitlab.com/yellowhat-labs/utils/commit/6b362a9852bf2fb73d94c1cd03b5aaa1671042d7))

## [1.11.5](https://gitlab.com/yellowhat-labs/utils/compare/v1.11.4...v1.11.5) (2024-09-28)


### Bug Fixes

* **deps:** update dependency semantic-release to v24.1.2 ([33f65c1](https://gitlab.com/yellowhat-labs/utils/commit/33f65c19aa3effe6ac36fe5fabc9ea1a0001d449))


### Chore

* **deps:** lock file maintenance ([0923d34](https://gitlab.com/yellowhat-labs/utils/commit/0923d347e0fae9a643e19c22c4f9bc0f52bf5eed))
* **deps:** lock file maintenance ([ff0a070](https://gitlab.com/yellowhat-labs/utils/commit/ff0a0705a2d585b4f705978221ffb2adf51adc71))
* **deps:** lock file maintenance ([b69c027](https://gitlab.com/yellowhat-labs/utils/commit/b69c027358c04897492c31608e6689f84c550449))
* **deps:** lock file maintenance ([a9c6ce8](https://gitlab.com/yellowhat-labs/utils/commit/a9c6ce8e68d0402d0f3d7b63955ccb2309938928))
* **deps:** lock file maintenance ([47b6d43](https://gitlab.com/yellowhat-labs/utils/commit/47b6d4321b57f10e2631d88bc1a5f7d155344ee0))
* **deps:** lock file maintenance ([0c26580](https://gitlab.com/yellowhat-labs/utils/commit/0c26580472b9af026edf5e09c7cc7e7b995a9709))
* **deps:** lock file maintenance ([e15946f](https://gitlab.com/yellowhat-labs/utils/commit/e15946f9effcef6d3ecc0c1248f101e8eace0469))
* **deps:** lock file maintenance ([bdab19e](https://gitlab.com/yellowhat-labs/utils/commit/bdab19e02769129ade8ff64105d5483db3992d19))
* **deps:** lock file maintenance ([d949df7](https://gitlab.com/yellowhat-labs/utils/commit/d949df7899eb4cbbd04420252aee8991d3460ea5))
* **deps:** lock file maintenance ([a7b9f09](https://gitlab.com/yellowhat-labs/utils/commit/a7b9f09fb66564a35a62c9a71d1ab66721a9235a))
* **deps:** lock file maintenance ([4422f58](https://gitlab.com/yellowhat-labs/utils/commit/4422f5885f14e34fe9afb9a91398fb746c6c5821))
* **deps:** lock file maintenance ([3102925](https://gitlab.com/yellowhat-labs/utils/commit/3102925683707171f2b290e2e4a08940715cd772))
* **deps:** lock file maintenance ([26711ff](https://gitlab.com/yellowhat-labs/utils/commit/26711ff7c478713d324008bc91bd8a17bb4381ac))
* **deps:** lock file maintenance ([89fae4e](https://gitlab.com/yellowhat-labs/utils/commit/89fae4e59a35e9c03fcbec528327285b8a6d8ff6))
* **deps:** update dependency @biomejs/biome to v1.9.1 ([df85807](https://gitlab.com/yellowhat-labs/utils/commit/df8580762b33daece310b1aac33439585bceaad8))
* **deps:** update dependency @biomejs/biome to v1.9.2 ([af031cf](https://gitlab.com/yellowhat-labs/utils/commit/af031cf2130b1be861e8e163540359d875c3f9aa))
* **deps:** update dependency bandit to v1.7.10 ([a6fac9e](https://gitlab.com/yellowhat-labs/utils/commit/a6fac9e611096a6900a0e844242f26a9fd56fcce))
* **deps:** update dependency crate-ci/typos to v1.24.6 ([0871db2](https://gitlab.com/yellowhat-labs/utils/commit/0871db2439b66bf9a86fbabf98a8f1df0557e80e))
* **deps:** update dependency pylint to v3.3.0 ([dc5f5dc](https://gitlab.com/yellowhat-labs/utils/commit/dc5f5dc4babf3de942c0ac0bebffdb0a8ec45de7))
* **deps:** update dependency pylint to v3.3.1 ([96b6f97](https://gitlab.com/yellowhat-labs/utils/commit/96b6f978d9d56f2a5f6bcb6d301fdb8f9395c645))
* **deps:** update dependency renovate to v38.101.0 ([2c947c4](https://gitlab.com/yellowhat-labs/utils/commit/2c947c4bb0280539caccdb38f32a38ec880838ae))
* **deps:** update dependency renovate to v38.84.1 ([1e66ade](https://gitlab.com/yellowhat-labs/utils/commit/1e66ade83d718122971af48bbf2f5e871fba4bb9))
* **deps:** update dependency renovate to v38.87.1 ([504fc25](https://gitlab.com/yellowhat-labs/utils/commit/504fc2516fa3576ba210fbef4e7797d5d1b693aa))
* **deps:** update dependency renovate to v38.89.1 ([a6ddd9d](https://gitlab.com/yellowhat-labs/utils/commit/a6ddd9d76bf4f72308ea4985da52b4264a5ad0d2))
* **deps:** update dependency renovate to v38.91.0 ([e731861](https://gitlab.com/yellowhat-labs/utils/commit/e7318614f78c83a2fdff44ddeb263b2d7993562c))
* **deps:** update dependency renovate to v38.92.0 ([5e6feb4](https://gitlab.com/yellowhat-labs/utils/commit/5e6feb40e335e420a0034bd5d96827818bf4ebd3))
* **deps:** update dependency renovate to v38.94.3 ([2266d9a](https://gitlab.com/yellowhat-labs/utils/commit/2266d9a313a92dccb9e510f95f96103deb8bf48e))
* **deps:** update dependency renovate to v38.95.3 ([f8397b8](https://gitlab.com/yellowhat-labs/utils/commit/f8397b8af32a752f1af08cfcf5f401095eb9b4cd))
* **deps:** update dependency renovate to v38.97.1 ([49ac009](https://gitlab.com/yellowhat-labs/utils/commit/49ac009731c46e93ce6c80be2458cd4f2e222d20))
* **deps:** update dependency ruff to v0.6.6 ([470cf0d](https://gitlab.com/yellowhat-labs/utils/commit/470cf0daf26db06943bf9525117e4ff36cb9e814))
* **deps:** update dependency ruff to v0.6.7 ([84b9feb](https://gitlab.com/yellowhat-labs/utils/commit/84b9feb0d4264c63ced2d92bdbc6931e1683e417))
* **deps:** update dependency ruff to v0.6.8 ([5a0acfb](https://gitlab.com/yellowhat-labs/utils/commit/5a0acfb987878ff862d219b79c33bef3f68adf5e))
* **deps:** update dependency vulture to v2.12 ([c8df67f](https://gitlab.com/yellowhat-labs/utils/commit/c8df67ff4293bec13ab5565e4ce1fbebbf86f33a))
* **deps:** update docker.io/nixos/nix docker tag to v2.24.7 ([317188e](https://gitlab.com/yellowhat-labs/utils/commit/317188e84a4d6fc2a871183d1796d07ca16dd625))
* **deps:** update docker.io/nixos/nix docker tag to v2.24.8 ([e352679](https://gitlab.com/yellowhat-labs/utils/commit/e352679489fd084d7cb9f6b476751e37ec30c347))
* **deps:** update docker.io/nixos/nix docker tag to v2.24.9 ([969b586](https://gitlab.com/yellowhat-labs/utils/commit/969b58626afa73ec4c4e2fd1dd62596f9cfdc013))
* **deps:** update docker.io/python:3.12.6 docker digest to 14f0736 ([6b5c990](https://gitlab.com/yellowhat-labs/utils/commit/6b5c99062daaf93996217644a305dde31548f132))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.88 ([a40f106](https://gitlab.com/yellowhat-labs/utils/commit/a40f1063afaa359d20bd093970aa526ac99dfab6))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.89 ([579fe69](https://gitlab.com/yellowhat-labs/utils/commit/579fe695027032273499e424f402485cc52cd4e0))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.90 ([2e4f915](https://gitlab.com/yellowhat-labs/utils/commit/2e4f9150c0d87faa9718db9cc8ad8afa2608d019))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.19.2 ([1c9ec20](https://gitlab.com/yellowhat-labs/utils/commit/1c9ec20d46d283091f1da214866c091cf7b6cb4b))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.19.3 ([c9cf76e](https://gitlab.com/yellowhat-labs/utils/commit/c9cf76e73c5f9e9ab6831ebbbe0414ca7e666e6f))
* **deps:** update ghcr.io/igorshubovych/markdownlint-cli docker tag to v0.42.0 ([6f0bdce](https://gitlab.com/yellowhat-labs/utils/commit/6f0bdceabb97aaa58115e05b20028e10197e7141))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.101.0 ([79be681](https://gitlab.com/yellowhat-labs/utils/commit/79be681793305e38d10e8adf64ee2eee3355136a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.84.1 ([35d029d](https://gitlab.com/yellowhat-labs/utils/commit/35d029de55b80507cf21a3aa26ef62e2c0b7e05f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.87.1 ([b924660](https://gitlab.com/yellowhat-labs/utils/commit/b924660406755bc40d6c948bfa8cfcc8ee1dfe94))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.89.1 ([9d69707](https://gitlab.com/yellowhat-labs/utils/commit/9d69707607fa2e5b25d01a02d8b263157d6c7838))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.91.0 ([715ae35](https://gitlab.com/yellowhat-labs/utils/commit/715ae358e622a689279e26cc3ab1507618c1641d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.92.0 ([aba49be](https://gitlab.com/yellowhat-labs/utils/commit/aba49bef8bbbcff72238e2a4ffca9303245ce0a5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.93.1 ([b1e63fe](https://gitlab.com/yellowhat-labs/utils/commit/b1e63fe4db86f61c37e2f3a55ec66faa76873d1e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.93.3 ([32133f9](https://gitlab.com/yellowhat-labs/utils/commit/32133f90dc16dfff9ce1013db77118d05dd147b1))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.94.3 ([5ae1b61](https://gitlab.com/yellowhat-labs/utils/commit/5ae1b61aebd17c4c84ba2f442ebb5b33403d7792))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.95.3 ([75c17a9](https://gitlab.com/yellowhat-labs/utils/commit/75c17a93261ecfbebfef5357a0faf078ae62eab4))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.96.0 ([9412b6a](https://gitlab.com/yellowhat-labs/utils/commit/9412b6a2e516fc561313c27a50b106562aeaab9a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.97.1 ([528ed52](https://gitlab.com/yellowhat-labs/utils/commit/528ed52781722c848bdc0f716e9b4f10dba59fd0))
* **deps:** update node.js to 69e667a ([fe3d1cd](https://gitlab.com/yellowhat-labs/utils/commit/fe3d1cd29d4eefb0919f0b758fca81ee148cdd70))
* **deps:** update node.js to c9bb434 ([d6a7ade](https://gitlab.com/yellowhat-labs/utils/commit/d6a7ade12b2c7ed2463329292b750763067a8560))
* **deps:** update node.js to cbe2d5f ([2962a75](https://gitlab.com/yellowhat-labs/utils/commit/2962a75f443d6d86d00a6a8c85078c55852d8285))
* **deps:** update node.js to v22.9.0 ([bb66f42](https://gitlab.com/yellowhat-labs/utils/commit/bb66f42c90a77a6eac9b0a81717ee983dbed5101))

## [1.11.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.11.3...v1.11.4) (2024-09-15)


### Bug Fixes

* flake-check is missing flakes ([57e0adb](https://gitlab.com/yellowhat-labs/utils/commit/57e0adb08aac9d6c4ea6854816976d2e210dab09))


### Chore

* **deps:** lock file maintenance ([72d173e](https://gitlab.com/yellowhat-labs/utils/commit/72d173ec3e3691e812737b523477bc7707a965dc))
* **deps:** lock file maintenance ([dd5b2fe](https://gitlab.com/yellowhat-labs/utils/commit/dd5b2feac6e07ce9549ce237830a6a0fea2a9b89))
* **deps:** lock file maintenance ([5e270b9](https://gitlab.com/yellowhat-labs/utils/commit/5e270b9126d6ec2aac657113c2b85eddb51e4b96))
* **deps:** update dependency @biomejs/biome to v1.9.0 ([1774a08](https://gitlab.com/yellowhat-labs/utils/commit/1774a08e95e529481cadf09956c79a3d7cd52af0))
* **deps:** update dependency @types/jest to v29.5.13 ([6695e29](https://gitlab.com/yellowhat-labs/utils/commit/6695e2940ea92978d2938abb8ccf774d06dc6c1a))
* **deps:** update dependency renovate to v38.77.0 ([2fba627](https://gitlab.com/yellowhat-labs/utils/commit/2fba627e25b76f8790db57a343c37a7b77a4473a))
* **deps:** update dependency renovate to v38.77.3 ([d3a72cb](https://gitlab.com/yellowhat-labs/utils/commit/d3a72cb307ba99eec1ad386c821adc4024bb6ef1))
* **deps:** update dependency renovate to v38.80.0 ([6bd7ea9](https://gitlab.com/yellowhat-labs/utils/commit/6bd7ea9a23be886031c3f014dc8c7e91a9a665bb))
* **deps:** update dependency ruff to v0.6.5 ([a476270](https://gitlab.com/yellowhat-labs/utils/commit/a4762701febbbb8c9ada43f2ea072296f797b342))
* **deps:** update docker.io/python:3.12.6 docker digest to 7859853 ([5a3d5a3](https://gitlab.com/yellowhat-labs/utils/commit/5a3d5a35201a99bc38eaed9db1b808ab026fdb63))
* **deps:** update docker.io/python:3.12.6-alpine docker digest to 7130f75 ([8bb6242](https://gitlab.com/yellowhat-labs/utils/commit/8bb62423ec2b88dce28f49e4b41d177f5f55060a))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.87 ([695fd1b](https://gitlab.com/yellowhat-labs/utils/commit/695fd1b9758b7d44ba4ffc11977bc2ccc44ff7f9))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.19.1 ([210b9ee](https://gitlab.com/yellowhat-labs/utils/commit/210b9ee2586435ef2b944c210f1d10b38ec14d87))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.77.0 ([8b78a63](https://gitlab.com/yellowhat-labs/utils/commit/8b78a6392617e5adde67c93ed60f76318b07c9d3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.77.3 ([94ae3d8](https://gitlab.com/yellowhat-labs/utils/commit/94ae3d8f7d5a4a2398d8951160c80c3e128293b2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.80.0 ([93aaa71](https://gitlab.com/yellowhat-labs/utils/commit/93aaa71119391007fa6f92d82e5269284058e533))

## [1.11.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.11.2...v1.11.3) (2024-09-11)


### Chore

* **deps:** lock file maintenance ([504a358](https://gitlab.com/yellowhat-labs/utils/commit/504a358a20a1f7a58a0768bd0e3fdb145f2b6a70))
* **deps:** lock file maintenance ([8f20724](https://gitlab.com/yellowhat-labs/utils/commit/8f20724349df5a9df452d016ecbc877ed5debca1))
* **deps:** lock file maintenance ([31b0a7a](https://gitlab.com/yellowhat-labs/utils/commit/31b0a7a964deee820fee22a5287cdc45fcac7df1))
* **deps:** lock file maintenance ([897a587](https://gitlab.com/yellowhat-labs/utils/commit/897a587feac3bea0d6be4c80da28915ca53c9e0a))
* **deps:** lock file maintenance ([5b06c34](https://gitlab.com/yellowhat-labs/utils/commit/5b06c34edd9cb8de92c342f5111880e282f4286a))
* **deps:** lock file maintenance ([b8fe184](https://gitlab.com/yellowhat-labs/utils/commit/b8fe184b33ae25b49527fea2e4cac827b7ae4e1d))
* **deps:** lock file maintenance ([4cd50bf](https://gitlab.com/yellowhat-labs/utils/commit/4cd50bfd009fb3453aa2811a4802d63230b5fa8d))
* **deps:** lock file maintenance ([26b7594](https://gitlab.com/yellowhat-labs/utils/commit/26b7594afd9907d62a2d00e23dbffa95687d48d6))
* **deps:** lock file maintenance ([d257392](https://gitlab.com/yellowhat-labs/utils/commit/d25739224d2a6ca53d6689632ae7bf49556c2b25))
* **deps:** lock file maintenance ([15d41a8](https://gitlab.com/yellowhat-labs/utils/commit/15d41a825f6bc319641d16ab03d84c21a568082d))
* **deps:** lock file maintenance ([2449564](https://gitlab.com/yellowhat-labs/utils/commit/24495641c298098ff5752daee5e9f8abf7e95bcc))
* **deps:** update dependency crate-ci/typos to v1.24.4 ([7b79c0a](https://gitlab.com/yellowhat-labs/utils/commit/7b79c0afd6e7ba08733013103ffe2ad31bb4f5b7))
* **deps:** update dependency crate-ci/typos to v1.24.5 ([92e513c](https://gitlab.com/yellowhat-labs/utils/commit/92e513ceca902c60f2fd635fa2a7da7cce750485))
* **deps:** update dependency renovate to v38.64.2 ([58195ae](https://gitlab.com/yellowhat-labs/utils/commit/58195aea0efcb7fe5d8be91c99fa1f0959b15403))
* **deps:** update dependency renovate to v38.67.1 ([fbb10bc](https://gitlab.com/yellowhat-labs/utils/commit/fbb10bcb3cc7c76ed0e5a943cd0aa65bac69d3a9))
* **deps:** update dependency renovate to v38.70.0 ([30a767c](https://gitlab.com/yellowhat-labs/utils/commit/30a767c00a044428c1aebece9fbc1d89d4883c25))
* **deps:** update dependency renovate to v38.70.1 ([e1f4bc6](https://gitlab.com/yellowhat-labs/utils/commit/e1f4bc6383bf1d262810a5118d499cfedb00b310))
* **deps:** update dependency renovate to v38.70.2 ([4f0cc1d](https://gitlab.com/yellowhat-labs/utils/commit/4f0cc1ddb27557e2859c0752ca06426d0d11bfb9))
* **deps:** update dependency renovate to v38.72.0 ([6f4d3e8](https://gitlab.com/yellowhat-labs/utils/commit/6f4d3e82121cd49665939f149c61547010d4bf12))
* **deps:** update dependency renovate to v38.72.1 ([d17e80a](https://gitlab.com/yellowhat-labs/utils/commit/d17e80af9c68fe2acb0e07a9f1b00dadc156793b))
* **deps:** update dependency renovate to v38.73.5 ([f993966](https://gitlab.com/yellowhat-labs/utils/commit/f9939665714c9e3d3ffdf24448a1b612c20141fa))
* **deps:** update dependency renovate to v38.73.7 ([5035e52](https://gitlab.com/yellowhat-labs/utils/commit/5035e524a8bc9ae2d38129c27f23b0290b3e8177))
* **deps:** update dependency renovate to v38.74.1 ([daa71c0](https://gitlab.com/yellowhat-labs/utils/commit/daa71c030f8580f89cb59bd0868d54934376c1ba))
* **deps:** update dependency ruff to v0.6.4 ([faa06d0](https://gitlab.com/yellowhat-labs/utils/commit/faa06d0479a1b31e10fe3ccaf99589f3de9c96e2))
* **deps:** update dependency typescript to v5.6.2 ([5856882](https://gitlab.com/yellowhat-labs/utils/commit/5856882b966d4f2391e7762b5f25b18045de2a6d))
* **deps:** update docker.io/alpine:latest docker digest to beefdbd ([f052525](https://gitlab.com/yellowhat-labs/utils/commit/f05252597d3a6fc38fa30558d81c61325d8e8a3f))
* **deps:** update docker.io/nixos/nix docker tag to v2.24.5 ([7cde930](https://gitlab.com/yellowhat-labs/utils/commit/7cde930be7835cf04db13af44226ec4652038a8f))
* **deps:** update docker.io/nixos/nix docker tag to v2.24.6 ([ff3e3b1](https://gitlab.com/yellowhat-labs/utils/commit/ff3e3b1ab7c182804930e5a47e196f626c2c2227))
* **deps:** update docker.io/python docker tag to v3.12.6 ([9e9a5af](https://gitlab.com/yellowhat-labs/utils/commit/9e9a5aff3a11169006c0733399bf5f5bc6e3b925))
* **deps:** update docker.io/python:3.12.5 docker digest to 11aa4b6 ([19b1f8a](https://gitlab.com/yellowhat-labs/utils/commit/19b1f8aa01c863a4d7f2068ff6b467026d0a8451))
* **deps:** update docker.io/python:3.12.5 docker digest to 3c08558 ([9d5b0b1](https://gitlab.com/yellowhat-labs/utils/commit/9d5b0b1b528c6e4c1bc0ef06fcc36fe14d4c4b1d))
* **deps:** update docker.io/python:3.12.5 docker digest to c7bb441 ([4334ee5](https://gitlab.com/yellowhat-labs/utils/commit/4334ee5381f51fbc2e78593e5abace7b99de1cea))
* **deps:** update docker.io/python:3.12.5-alpine docker digest to aeff643 ([e988886](https://gitlab.com/yellowhat-labs/utils/commit/e988886447f97fb9ce7543345fc73d3af539d053))
* **deps:** update docker.io/python:3.12.5-alpine docker digest to bb5d0ac ([5a0800d](https://gitlab.com/yellowhat-labs/utils/commit/5a0800d51a940169daf85d5aa6f0a890a8063fb4))
* **deps:** update docker.io/python:3.12.6 docker digest to 73840b2 ([9daca41](https://gitlab.com/yellowhat-labs/utils/commit/9daca41f1ca1bdc5b2dd7e49bf5aada893caa28d))
* **deps:** update docker.io/returntocorp/semgrep docker tag to v1.86 ([83036cc](https://gitlab.com/yellowhat-labs/utils/commit/83036cc3a5e4b40fef6cd4af55f3bc517ceaf0b4))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.64.2 ([e56e765](https://gitlab.com/yellowhat-labs/utils/commit/e56e76538264d3d07ce550d6d60b1f1eb3e21194))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.67.1 ([5f52336](https://gitlab.com/yellowhat-labs/utils/commit/5f5233697281ce609927c7679060d4f01503ed25))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.67.5 ([6cbb804](https://gitlab.com/yellowhat-labs/utils/commit/6cbb80428bb6612b23505dcc87daa42c0767e85a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.70.0 ([3c91b76](https://gitlab.com/yellowhat-labs/utils/commit/3c91b766cd53653fa80fecf9a1cb234bb25d8eea))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.70.1 ([9c2d3c7](https://gitlab.com/yellowhat-labs/utils/commit/9c2d3c7719deacccf5fb29239dbc1fc3237a8524))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.70.2 ([a34624a](https://gitlab.com/yellowhat-labs/utils/commit/a34624a5b8d53af536af2cc727024a06c033877a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.72.0 ([1e9e8d8](https://gitlab.com/yellowhat-labs/utils/commit/1e9e8d8493bfd91391a62517ee33a5d595a1a4bd))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.72.1 ([f719133](https://gitlab.com/yellowhat-labs/utils/commit/f719133d51a7a9f0a441a94ca339dd263edaa6e9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.73.5 ([00f9ab5](https://gitlab.com/yellowhat-labs/utils/commit/00f9ab57ace3a6f2b51bbea46d37033a6f58725b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.73.7 ([2ccae70](https://gitlab.com/yellowhat-labs/utils/commit/2ccae70ae15ea8f45e658cb01159144875f60c6c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.74.1 ([abd83c3](https://gitlab.com/yellowhat-labs/utils/commit/abd83c38263bfd14dd222b324f73d68d6dd70bdf))
* **deps:** update node.js to 7cbc7e4 ([2194364](https://gitlab.com/yellowhat-labs/utils/commit/2194364779db5ce6fdf1d8f2e158f68a586cbd07))
* **deps:** update node.js to 7cbc7e4 ([2fc5f88](https://gitlab.com/yellowhat-labs/utils/commit/2fc5f8851f2bc3232169251907026bde1ce1f55f))
* **deps:** update node.js to 8ec0232 ([7874730](https://gitlab.com/yellowhat-labs/utils/commit/7874730f085ca62c89e2a180cc7ef7d5d988b0f7))
* **deps:** update node.js to bd00c03 ([478dc4c](https://gitlab.com/yellowhat-labs/utils/commit/478dc4c5621ee86aed619a0e1fcf1111130bae71))
* **deps:** update node.js to bec0ea4 ([811457a](https://gitlab.com/yellowhat-labs/utils/commit/811457a244b5903b0551269c302bfebe50ad8154))
* **deps:** update node.js to v22.8.0 ([b5ae32a](https://gitlab.com/yellowhat-labs/utils/commit/b5ae32a69e706d39d8a1f21618a4f8d6de97c024))
* **deps:** update registry.gitlab.com/gitlab-org/cli docker tag to v1.46.0 ([b45c040](https://gitlab.com/yellowhat-labs/utils/commit/b45c04090baaf366a19a205efebe8817773ab398))
* **deps:** update registry.gitlab.com/gitlab-org/cli docker tag to v1.46.1 ([19eb7aa](https://gitlab.com/yellowhat-labs/utils/commit/19eb7aa58366ee62fc85d94416befe2ba8145910))


### Bug Fixes

* **deps:** update dependency semantic-release to v24.1.1 ([fcbfa53](https://gitlab.com/yellowhat-labs/utils/commit/fcbfa531e1d6c69c0dfebd054c80d10dd7ff376c))


### Continuous Integration

* drop check-registry ([a6583cd](https://gitlab.com/yellowhat-labs/utils/commit/a6583cdbb2edb5f7bf7e6d96fe20c7bf6d4c915b))
* **renovate:** make cache work ([d1fbd50](https://gitlab.com/yellowhat-labs/utils/commit/d1fbd500f78258cd8b6ef90a716f8e926aceae57))
* **renovate:** set cache dir ([40b97b2](https://gitlab.com/yellowhat-labs/utils/commit/40b97b236d0ec578ca5e636d5d0a67243c379e7f))
* **renovate:** use cache ([050c256](https://gitlab.com/yellowhat-labs/utils/commit/050c256f6ef0ffa04e77444b351539dd12b0f35b))

## [1.11.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.11.1...v1.11.2) (2024-09-03)


### Bug Fixes

* use npx instead of npm ([9602b94](https://gitlab.com/yellowhat-labs/utils/commit/9602b94131d7a28dce02041f5bc63d05171a4b9b))


### Chore

* **deps:** lock file maintenance ([1ef5c19](https://gitlab.com/yellowhat-labs/utils/commit/1ef5c198834a27f33f2710320270f3479b9e6a30))
* **deps:** lock file maintenance ([ed15700](https://gitlab.com/yellowhat-labs/utils/commit/ed15700eabb43a23f2ec82237a8564f315ea50ec))
* **deps:** lock file maintenance ([92b4928](https://gitlab.com/yellowhat-labs/utils/commit/92b4928b92fe605281e1a92d94682b21618d9c1a))
* **deps:** lock file maintenance ([9f3edde](https://gitlab.com/yellowhat-labs/utils/commit/9f3edde1d052c6ff38fbddc6bbd023426e38211c))
* **deps:** lock file maintenance ([6f83967](https://gitlab.com/yellowhat-labs/utils/commit/6f83967d0f0eb770eebea910e8833e9841381fe1))
* **deps:** lock file maintenance ([825ba5e](https://gitlab.com/yellowhat-labs/utils/commit/825ba5e99ba5876c68f7da27d7bb59b4cab77519))
* **deps:** update dependency alpine_3_20/curl to v8.9.1-r1 ([712fcba](https://gitlab.com/yellowhat-labs/utils/commit/712fcba5216c20733ac559277271d42dfe0bf86b))
* **deps:** update dependency crate-ci/typos to v1.24.3 ([c3337ff](https://gitlab.com/yellowhat-labs/utils/commit/c3337ffa46235f1345464ac9a27d06d393cac640))
* **deps:** update dependency pylint to v3.2.7 ([9011d8c](https://gitlab.com/yellowhat-labs/utils/commit/9011d8c9b85d1ead695247f2e73fff60b755e296))
* **deps:** update dependency renovate to v38.58.0 ([32021cc](https://gitlab.com/yellowhat-labs/utils/commit/32021cc134f6afc19b28327c8d4518fa73b7ee21))
* **deps:** update dependency renovate to v38.58.2 ([1707ed3](https://gitlab.com/yellowhat-labs/utils/commit/1707ed31dec659dabe100f0767b736bb489ab893))
* **deps:** update dependency renovate to v38.59.2 ([33c6a2e](https://gitlab.com/yellowhat-labs/utils/commit/33c6a2ee489c1ff085a636c93307530adce1179b))
* **deps:** update dependency renovate to v38.64.1 ([6fb6014](https://gitlab.com/yellowhat-labs/utils/commit/6fb60147c4e6d0538aeb8368aa35563d0cb3a18b))
* **deps:** update dependency ruff to v0.6.3 ([f032e0c](https://gitlab.com/yellowhat-labs/utils/commit/f032e0cf18768df16bd571f955b8d4b5153830e3))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.23.2 ([dc7ee2a](https://gitlab.com/yellowhat-labs/utils/commit/dc7ee2aa7211af11d84ffcd3f8c2a0909384bab2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.58.0 ([9fbfd76](https://gitlab.com/yellowhat-labs/utils/commit/9fbfd76b3d469b498a02342afc011d618b7f43fe))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.58.2 ([fc23338](https://gitlab.com/yellowhat-labs/utils/commit/fc233384aa02caa71473aa7ce3e483400d299631))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.59.1 ([3498a7c](https://gitlab.com/yellowhat-labs/utils/commit/3498a7c3aeff66ac6ef7d7c4a51cca6f0fea1cad))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.59.2 ([c1cf72d](https://gitlab.com/yellowhat-labs/utils/commit/c1cf72d14314f9fc74826e9ebe6d46e4cb298d28))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.59.3 ([8f66f1b](https://gitlab.com/yellowhat-labs/utils/commit/8f66f1b2d132ef98ea966c08b492e8141b82e32b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.64.1 ([fbbd2df](https://gitlab.com/yellowhat-labs/utils/commit/fbbd2df2e7a132d15620052a6a3caf688095a12b))

## [1.11.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.11.0...v1.11.1) (2024-08-29)


### Bug Fixes

* **flake-check:** add experimental feature and accept-flake-config ([7a5aec1](https://gitlab.com/yellowhat-labs/utils/commit/7a5aec1b4232e542aa7b730588801ef9d52f57fa))


### Chore

* **deps:** lock file maintenance ([4fa5baf](https://gitlab.com/yellowhat-labs/utils/commit/4fa5bafb1acd5c68471eb22ccd5baad7ebef956b))
* **deps:** lock file maintenance ([756742b](https://gitlab.com/yellowhat-labs/utils/commit/756742bc17ed5a92310099b689edf3b38f495abf))
* **deps:** lock file maintenance ([c08dca8](https://gitlab.com/yellowhat-labs/utils/commit/c08dca8a90bd010e57ebd79c9eafd94d4b1b520f))
* **deps:** lock file maintenance ([78fe877](https://gitlab.com/yellowhat-labs/utils/commit/78fe8770458d3a1dec73c8c7a675cdbd5251ece4))
* **deps:** update dependency alpine_3_20/curl to v8.9.1-r0 ([f062f10](https://gitlab.com/yellowhat-labs/utils/commit/f062f1052d465243de2023ae25bebb2e4e605c17))
* **deps:** update dependency crate-ci/typos to v1.23.7 ([f42955d](https://gitlab.com/yellowhat-labs/utils/commit/f42955da58d81384fce490e712e0acc751e9d9ee))
* **deps:** update dependency crate-ci/typos to v1.24.1 ([a431f6b](https://gitlab.com/yellowhat-labs/utils/commit/a431f6b567099ab1121ce35d29300011df70b04f))
* **deps:** update dependency mypy to v1.11.2 ([23b7291](https://gitlab.com/yellowhat-labs/utils/commit/23b7291abb9e952c7d02fb79989c6815bb378d49))
* **deps:** update dependency renovate to v38 ([9f23f71](https://gitlab.com/yellowhat-labs/utils/commit/9f23f716f6e48e4bb0fe13f8a1f0a233d72a68ad))
* **deps:** update dependency renovate to v38.56.3 ([f6a77c7](https://gitlab.com/yellowhat-labs/utils/commit/f6a77c7e0dddd9064ecdc1115556bf05cc2c630b))
* **deps:** update dependency renovate to v38.57.0 ([0f77974](https://gitlab.com/yellowhat-labs/utils/commit/0f77974aa72c35bbaa548541d17ae09124b5a7f8))
* **deps:** update dependency ruff to v0.6.2 ([8adea25](https://gitlab.com/yellowhat-labs/utils/commit/8adea25cf4991d8ad7a97bd5326f178e58fe04bf))
* **deps:** update docker.io/nixos/nix docker tag to v2.24.4 ([1d4a4bb](https://gitlab.com/yellowhat-labs/utils/commit/1d4a4bb232c56f32a2a37a5579d68623843415ca))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.47.0 ([2e562e9](https://gitlab.com/yellowhat-labs/utils/commit/2e562e9581f9436f6e3e8dcf6ff4d67b59b19875))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.51.1 ([65123c2](https://gitlab.com/yellowhat-labs/utils/commit/65123c2b838a86ca430474f8d21ca067b0416875))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.52.0 ([2e6155d](https://gitlab.com/yellowhat-labs/utils/commit/2e6155d54adda89fae6244d918cadcc92ded6630))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.52.1 ([78fcaf7](https://gitlab.com/yellowhat-labs/utils/commit/78fcaf7db4b861ae0928c55d9ece25e844bedb53))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.52.2 ([88f6b8c](https://gitlab.com/yellowhat-labs/utils/commit/88f6b8c12aca2593c98a278b598102e7c760384b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.52.3 ([8d289f5](https://gitlab.com/yellowhat-labs/utils/commit/8d289f5ce533f7b17a71c8e2fb2042743011e8ce))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.55.2 ([a61108b](https://gitlab.com/yellowhat-labs/utils/commit/a61108b2d0cb84534372372df3e9003f5596bd55))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.55.3 ([7886993](https://gitlab.com/yellowhat-labs/utils/commit/78869934cc29c4940f3fd32f5220b11bbcc3a3f0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.56.0 ([b6f9f8f](https://gitlab.com/yellowhat-labs/utils/commit/b6f9f8f5c09562c01f8a7bc0faef48d9d5928add))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.56.1 ([7cc711e](https://gitlab.com/yellowhat-labs/utils/commit/7cc711ebdfe088c07c9afd2d30f89334c94054ce))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.56.3 ([614f8e3](https://gitlab.com/yellowhat-labs/utils/commit/614f8e333f76349e42918f13e24ed48fbf8c2882))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.57.0 ([d4e3108](https://gitlab.com/yellowhat-labs/utils/commit/d4e310802e2dc8b02f44ea69a4beee646e65fe40))
* **deps:** update node.js to v22.7.0 ([5198d7c](https://gitlab.com/yellowhat-labs/utils/commit/5198d7c9f1f6bf312f6013feb7186f2ba0dfb202))
* rename tests to let renovate scrape ([7727038](https://gitlab.com/yellowhat-labs/utils/commit/77270383c71763f068a2fba5bee0736534e6e4d2))


### Continuous Integration

* **renovate:** enable lockFileMaintenance and nix ([128676f](https://gitlab.com/yellowhat-labs/utils/commit/128676f23c5d369c7f2f606b11aa71ffed309016))
* **renovate:** fix regex for versioning ([6d31cb5](https://gitlab.com/yellowhat-labs/utils/commit/6d31cb54dff89ed32520a840e616027489ffbfdb))
* **renovate:** sync ([8fd6341](https://gitlab.com/yellowhat-labs/utils/commit/8fd634180d54ec03ebebec4c36c52ad98adf8d5a))
* **renovate:** test regex ([b3a8d96](https://gitlab.com/yellowhat-labs/utils/commit/b3a8d96c6435ca7d8d69f617a2c1da7122a58dd7))

# [1.11.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.10.8...v1.11.0) (2024-08-21)


### Chore

* **deps:** pin docker.io/alpine docker tag to 0a4eaa0 ([d3c34ac](https://gitlab.com/yellowhat-labs/utils/commit/d3c34ac9d5aee743c77a9600a95e2aa5d8643074))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.45.0 ([f77418d](https://gitlab.com/yellowhat-labs/utils/commit/f77418d72002d3ed85223dc670da7413694a1335))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.46.1 ([77a739f](https://gitlab.com/yellowhat-labs/utils/commit/77a739f8dda90ebddba38b7af9a7a763cc7bb2b1))
* **deps:** update node.js to 4162c8a ([dc4b39c](https://gitlab.com/yellowhat-labs/utils/commit/dc4b39cf6de8b3128c973acd6c8b30c4f42152bc))


### Features

* add check-registry container ([e3ca58a](https://gitlab.com/yellowhat-labs/utils/commit/e3ca58adecebc215f4de7955ed2955eb66d925d8))

## [1.10.8](https://gitlab.com/yellowhat-labs/utils/compare/v1.10.7...v1.10.8) (2024-08-20)


### Bug Fixes

* **nix:** use nix-env is faster ([78ffebd](https://gitlab.com/yellowhat-labs/utils/commit/78ffebd4f4d4179f3e1c113ad219026fc2a96fef))


### Chore

* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.44.2 ([616fef3](https://gitlab.com/yellowhat-labs/utils/commit/616fef3a9849a1da766cf5772852bab127f68c3f))

## [1.10.7](https://gitlab.com/yellowhat-labs/utils/compare/v1.10.6...v1.10.7) (2024-08-20)


### Bug Fixes

* **linter:** nixfmt > nixfmt-rfc-style ([229c65b](https://gitlab.com/yellowhat-labs/utils/commit/229c65b9602309275bb1beed0d70dabc6a4e4d43))


### Chore

* **deps:** update docker.io/nixos/nix docker tag to v2.24.3 ([a36846c](https://gitlab.com/yellowhat-labs/utils/commit/a36846cfe44ce0eabf401d1e442d3f92d07f43a6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.39.6 ([36b316a](https://gitlab.com/yellowhat-labs/utils/commit/36b316a3c45f3e43611854716f73a7f3d1db6c4e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.42.0 ([c7b597d](https://gitlab.com/yellowhat-labs/utils/commit/c7b597d027f908cb98e5f84ffc64082ab5acf9d3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.43.0 ([cf33ca3](https://gitlab.com/yellowhat-labs/utils/commit/cf33ca3ca663cfaaa717b504f7804dfba939d952))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.43.1 ([33b2990](https://gitlab.com/yellowhat-labs/utils/commit/33b2990803703e4ced269f5dcc981b41fde5ce8a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.44.0 ([3a01864](https://gitlab.com/yellowhat-labs/utils/commit/3a0186432620a0cecf262a8ab9f2bbfe40d16e7d))

## [1.10.6](https://gitlab.com/yellowhat-labs/utils/compare/v1.10.5...v1.10.6) (2024-08-18)


### Bug Fixes

* **deps:** update dependency semantic-release to v24.1.0 ([0489472](https://gitlab.com/yellowhat-labs/utils/commit/048947239a53f7c297c80bbdec6d58d0b6e8e67e))


### Chore

* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.39.1 ([34117ee](https://gitlab.com/yellowhat-labs/utils/commit/34117ee3fe1879e59447a08fad25a70a07ffc9ca))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.39.5 ([7adb0c1](https://gitlab.com/yellowhat-labs/utils/commit/7adb0c140d3f564e33a340a2cf85ab7525215db3))

## [1.10.5](https://gitlab.com/yellowhat-labs/utils/compare/v1.10.4...v1.10.5) (2024-08-17)


### Bug Fixes

* **build:** use cd instead of context ([ef5c953](https://gitlab.com/yellowhat-labs/utils/commit/ef5c9539b5cf5b640a866a70dab457b1800c92c6))

## [1.10.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.10.3...v1.10.4) (2024-08-17)


### Bug Fixes

* **semgrep:** use ci option ([7d307ce](https://gitlab.com/yellowhat-labs/utils/commit/7d307cee3ebcc010cd2cdd8c3f1aa12e171d3c49))

## [1.10.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.10.2...v1.10.3) (2024-08-17)


### Bug Fixes

* **yamllint:** ensure single document ([8beb1a1](https://gitlab.com/yellowhat-labs/utils/commit/8beb1a10d73ddff21e396253198c473cf7798e4f))


### Chore

* **deps:** update docker.io/mvdan/shfmt docker tag to v3.9.0 ([7535d5f](https://gitlab.com/yellowhat-labs/utils/commit/7535d5f98bec0d5ae73c8b0939c6334fed0dbb50))
* **deps:** update docker.io/node docker tag to v22 ([7a61982](https://gitlab.com/yellowhat-labs/utils/commit/7a619829d9a02214a394cf1f52092dc622b3fb2e))

## [1.10.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.10.1...v1.10.2) (2024-08-16)


### Bug Fixes

* **yamllint:** extend per repo config ([2e4428e](https://gitlab.com/yellowhat-labs/utils/commit/2e4428ecd8c5fb4aef2e65b35934172f93682465))


### Chore

* **deps:** update dependency ruff to v0.6.1 ([18c8f76](https://gitlab.com/yellowhat-labs/utils/commit/18c8f7683aa1051acf2198c740250761067aad63))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.37.1 ([795bb69](https://gitlab.com/yellowhat-labs/utils/commit/795bb6960e062b6c3beb5cff93f2003dc5dd076f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.38.0 ([c4945bc](https://gitlab.com/yellowhat-labs/utils/commit/c4945bc4129591651e87f3f8152efc263fca2f74))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.39.0 ([5f36e59](https://gitlab.com/yellowhat-labs/utils/commit/5f36e59284cefb74c825423055f679f25f7ffea2))


### Continuous Integration

* **renovate:** reduce complexity ([455cd06](https://gitlab.com/yellowhat-labs/utils/commit/455cd06c3123e57d0f8cb2bc7c3b35fa62422404))

## [1.10.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.10.0...v1.10.1) (2024-08-16)


### Bug Fixes

* **build.yaml:** use cwd in context ([dc91eba](https://gitlab.com/yellowhat-labs/utils/commit/dc91eba4c65ce58449c474b95ea7b5ebe9d99ef0))


### Chore

* **deps:** update dependency ruff to v0.5.7 ([5cff90d](https://gitlab.com/yellowhat-labs/utils/commit/5cff90da8f59bdf788ec0d29806116673d12718a))
* **deps:** update dependency ruff to v0.6.0 ([8291643](https://gitlab.com/yellowhat-labs/utils/commit/8291643824ed767bf9161e1a2b92e68f2b3159fa))
* **deps:** update docker.io/nixos/nix docker tag to v2.24.2 ([270c71b](https://gitlab.com/yellowhat-labs/utils/commit/270c71b5544c885760025cfe3d6f3f3473087fcc))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.21.4 ([e9b339a](https://gitlab.com/yellowhat-labs/utils/commit/e9b339aa96dda0a908f6b697836232efc18adf3e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.23.2 ([bd2e943](https://gitlab.com/yellowhat-labs/utils/commit/bd2e943cf00971337898b43c1bd8aeb4ceb7a547))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.25.0 ([b33afe6](https://gitlab.com/yellowhat-labs/utils/commit/b33afe6758e0f336c022a1ef9d4699d7250a3795))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.25.1 ([9f95336](https://gitlab.com/yellowhat-labs/utils/commit/9f953363f4a60899c3bcc058a96ae6458e2fcca0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.26.0 ([b7109c0](https://gitlab.com/yellowhat-labs/utils/commit/b7109c08d37aed72e756c7eda9ed306fbf2416b2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.27.0 ([5265db2](https://gitlab.com/yellowhat-labs/utils/commit/5265db26d15a9c48cc7dc97b6d970bea541dcfac))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.31.0 ([fca9c6f](https://gitlab.com/yellowhat-labs/utils/commit/fca9c6f8fa65dc20dbd8e16bc474e64309d03f48))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.32.1 ([f276d11](https://gitlab.com/yellowhat-labs/utils/commit/f276d11d1f87be6a67afdaf7ebbb60a56b8a6800))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.37.0 ([1225445](https://gitlab.com/yellowhat-labs/utils/commit/12254458d1509b932dafb4ee4e9af8c27315d303))
* **deps:** update python:3.12.5 docker digest to e3d5b6f ([7bf3f10](https://gitlab.com/yellowhat-labs/utils/commit/7bf3f105356929bd94654a3c97570fab003899b8))
* **deps:** update returntocorp/semgrep docker tag to v1.85 ([1b7e061](https://gitlab.com/yellowhat-labs/utils/commit/1b7e061c5321d3ae4c72d25e04f1dcc0808f5cbb))

# [1.10.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.9.1...v1.10.0) (2024-08-08)


### Features

* **release:** us .post stage ([4fe62b6](https://gitlab.com/yellowhat-labs/utils/commit/4fe62b6f8ad1e29edb1d0acd43e002e424383fb7))


### release

* fetch all branches and tags ([6ecac2a](https://gitlab.com/yellowhat-labs/utils/commit/6ecac2a6f5ff94e17c184d465d64970694c9ece3))


### Chore

* **deps:** update alpine:edge docker digest to b93f4f6 ([d507d02](https://gitlab.com/yellowhat-labs/utils/commit/d507d027de21fbee830cb629ca310cec7c99dd53))
* **deps:** update dependency @semantic-release/gitlab to v13.2.1 ([dfdb0b0](https://gitlab.com/yellowhat-labs/utils/commit/dfdb0b08efcc49d5399c7c7868f41a17ebf5f0aa))
* **deps:** update dependency crate-ci/typos to v1.23.4 ([21c67e8](https://gitlab.com/yellowhat-labs/utils/commit/21c67e85c4a172a5f49a6c0b668b5471da817cdf))
* **deps:** update dependency crate-ci/typos to v1.23.5 ([7c786d0](https://gitlab.com/yellowhat-labs/utils/commit/7c786d07ed19dd13ce0b494dbaf963bc66e5adee))
* **deps:** update dependency crate-ci/typos to v1.23.6 ([2f99395](https://gitlab.com/yellowhat-labs/utils/commit/2f993958f97b140eee86002ac32d875668d20b15))
* **deps:** update dependency flake8 to v7.1.1 ([cb72019](https://gitlab.com/yellowhat-labs/utils/commit/cb72019b2aba8020e43ae1438e4d6ac9155d6e3b))
* **deps:** update dependency mypy to v1.11.1 ([6540e5b](https://gitlab.com/yellowhat-labs/utils/commit/6540e5b83dc76476a27c4e6640731ce3346c9d44))
* **deps:** update dependency pyupgrade to v3.17.0 ([524d222](https://gitlab.com/yellowhat-labs/utils/commit/524d222a112f2b262e938bf1ba05924524a702b1))
* **deps:** update dependency ruff to v0.5.5 ([83dd5ba](https://gitlab.com/yellowhat-labs/utils/commit/83dd5ba94ce2694d811ae7e17e8d613135380f79))
* **deps:** update dependency ruff to v0.5.6 ([fa57228](https://gitlab.com/yellowhat-labs/utils/commit/fa5722819e600cbfa8a9e37e134cb0b64a7b3f99))
* **deps:** update docker.io/nixos/nix docker tag to v2.24.1 ([0213627](https://gitlab.com/yellowhat-labs/utils/commit/0213627d62f6159a787b2cc7a39dfdb26cf4186b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38 ([bdb9806](https://gitlab.com/yellowhat-labs/utils/commit/bdb98061a53accbca19af5194bb8c2ef48160ba3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.10.0 ([26c6da8](https://gitlab.com/yellowhat-labs/utils/commit/26c6da898410d054e9703c27dee380b52b86ed28))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.10.1 ([44c5687](https://gitlab.com/yellowhat-labs/utils/commit/44c5687ac8d17ad3d06997e97fb2115e7b0e8107))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.14.0 ([7918988](https://gitlab.com/yellowhat-labs/utils/commit/791898898a88ab2ec28760a08dee0ac59d723ca9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.15.0 ([81435c9](https://gitlab.com/yellowhat-labs/utils/commit/81435c917b99ece65a679fe86917c1dd6ddbc74f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.15.1 ([37adc32](https://gitlab.com/yellowhat-labs/utils/commit/37adc32b7e1dc965bc4a51bc11850617268d89bc))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.17.1 ([4fe7cab](https://gitlab.com/yellowhat-labs/utils/commit/4fe7cab4167f7dc0536f010faba9701ed4fecf5c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.18.12 ([97f0fe5](https://gitlab.com/yellowhat-labs/utils/commit/97f0fe5e0b20544648a396b03a5d23da02b642d5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.18.15 ([9506701](https://gitlab.com/yellowhat-labs/utils/commit/9506701e7df1b9a665d48ec7475b3f9300076d64))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.18.16 ([85dc53a](https://gitlab.com/yellowhat-labs/utils/commit/85dc53aeecc62808cdb6e1461ee00ad6ad12ff70))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.18.17 ([1385dea](https://gitlab.com/yellowhat-labs/utils/commit/1385dea0df2d3d1dd1c7572b742ffd6a34a660f7))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.18.6 ([cbfe5be](https://gitlab.com/yellowhat-labs/utils/commit/cbfe5be8e889b2e426695f7aaa786c59237fc4c3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.20.1 ([ecaf1e9](https://gitlab.com/yellowhat-labs/utils/commit/ecaf1e94133cf09e0bbff7bb2c2077f8c1c397bd))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.21.2 ([95dae8d](https://gitlab.com/yellowhat-labs/utils/commit/95dae8d129ae4715b0208722cf64facb0a5e1528))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.21.3 ([11e5263](https://gitlab.com/yellowhat-labs/utils/commit/11e5263e429e5f8cd45d2753c30f9887ab17c422))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.5.0 ([b499896](https://gitlab.com/yellowhat-labs/utils/commit/b499896d062fe9c71de8f977093ede3e273fcdb7))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.8.3 ([ece0d99](https://gitlab.com/yellowhat-labs/utils/commit/ece0d99a9a4228f42cf4e4b512d8d720fe4952e4))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v38.9.0 ([7db355a](https://gitlab.com/yellowhat-labs/utils/commit/7db355abd1cb34486decda4a01bbedb698021580))
* **deps:** update node:21.7.3 docker digest to 4b23206 ([e1539a2](https://gitlab.com/yellowhat-labs/utils/commit/e1539a2b995a7c03f8c9c71b255ceb5d817dc4ce))
* **deps:** update pyfound/black docker tag to v24.8.0 ([50f354c](https://gitlab.com/yellowhat-labs/utils/commit/50f354cab8f9e03259c60c60c36cb22a8be1d214))
* **deps:** update python docker tag to v3.12.5 ([7b7a218](https://gitlab.com/yellowhat-labs/utils/commit/7b7a218a92e1f947b52a89a1fd4c47459593de76))
* **deps:** update python:3.12.4 docker digest to 031148f ([c93da30](https://gitlab.com/yellowhat-labs/utils/commit/c93da30d32abcd22a2090f3837282432268ca090))
* **deps:** update python:3.12.4 docker digest to 942374a ([23fb598](https://gitlab.com/yellowhat-labs/utils/commit/23fb598dbca4ddb84641a8923328c14dc7817436))
* **deps:** update python:3.12.4 docker digest to e8be0ea ([037f0ca](https://gitlab.com/yellowhat-labs/utils/commit/037f0ca99d2b0ef09efff81f0a9c203ec259f0e8))
* **deps:** update python:3.12.4-alpine docker digest to 2abecb7 ([098c56e](https://gitlab.com/yellowhat-labs/utils/commit/098c56e3885edc5f45937fcb41dad75486127ade))
* **deps:** update python:3.12.4-alpine docker digest to 63094ab ([0eb8697](https://gitlab.com/yellowhat-labs/utils/commit/0eb86970a209fbbdf2c6b5a7b79715f83671d352))
* **deps:** update python:3.12.4-alpine docker digest to a0c22d8 ([73f428b](https://gitlab.com/yellowhat-labs/utils/commit/73f428bdd47e9dfb75383e9b41ea4ec488b74cf9))
* **deps:** update registry.gitlab.com/gitlab-org/cli docker tag to v1.45.0 ([6a5671b](https://gitlab.com/yellowhat-labs/utils/commit/6a5671bce65e36b5f07425405f377b22dced2430))
* **deps:** update returntocorp/semgrep docker tag to v1.82 ([774d63b](https://gitlab.com/yellowhat-labs/utils/commit/774d63bc94243350989db56cb57c51d1a5263653))
* **deps:** update returntocorp/semgrep docker tag to v1.83 ([9d631bd](https://gitlab.com/yellowhat-labs/utils/commit/9d631bd7420e7bfb365c93c58bd8d99733a77354))
* **deps:** update returntocorp/semgrep docker tag to v1.84 ([90045ba](https://gitlab.com/yellowhat-labs/utils/commit/90045ba823ca4cb8a9032c04aea9923d57281405))
* **deps:** update returntocorp/semgrep:1.81 docker digest to aca826e ([3b8df85](https://gitlab.com/yellowhat-labs/utils/commit/3b8df85c4010d0e25ed0253f5b230631d2c552ed))
* **deps:** update returntocorp/semgrep:1.84 docker digest to 875ea8c ([fdf4b4f](https://gitlab.com/yellowhat-labs/utils/commit/fdf4b4fe2294555ef3a882c85931350a1e3812a3))
* **README:** add comment template ([dd38fbd](https://gitlab.com/yellowhat-labs/utils/commit/dd38fbdb0cb7012d37e2fec08e8faf9c222aeffd))


### renovate

* pin fedora 40 ([59a1202](https://gitlab.com/yellowhat-labs/utils/commit/59a120240b3731d4fbc8d2f159a86d9340c75b5b))

## [1.9.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.9.0...v1.9.1) (2024-07-25)


### Bug Fixes

* **comment:** allow fail ([aff8e1c](https://gitlab.com/yellowhat-labs/utils/commit/aff8e1ce3baca2b1de711e19c3ef80a65952f1be))


### Chore

* **deps:** pin registry.gitlab.com/gitlab-org/cli docker tag to 94ee31f ([ddb0671](https://gitlab.com/yellowhat-labs/utils/commit/ddb0671da239504e34f0f061868c953d9640d900))
* **deps:** update alpine:latest docker digest to 0a4eaa0 ([2407df7](https://gitlab.com/yellowhat-labs/utils/commit/2407df75748756a20f4e001df166d388ea49d647))
* **deps:** update dependency crate-ci/typos to v1.23.3 ([a2f16d1](https://gitlab.com/yellowhat-labs/utils/commit/a2f16d12919dc3fdc6472b2b8269426105ea67fb))
* **deps:** update dependency pylint to v3.2.6 ([416fe01](https://gitlab.com/yellowhat-labs/utils/commit/416fe0157d3b250b3aaa8b7470bfeb6ae86b1dc2))
* **deps:** update dependency ruff to v0.5.4 ([10ed2ce](https://gitlab.com/yellowhat-labs/utils/commit/10ed2cee980145e71bec761b7710a7f86055374c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.437.0 ([fdd30f8](https://gitlab.com/yellowhat-labs/utils/commit/fdd30f869a91217f1c19a1ed5433514e9ed5b449))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.437.3 ([fdc46bd](https://gitlab.com/yellowhat-labs/utils/commit/fdc46bd2d85604a73a7bf783a7a75ad1b9175c1a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.438.2 ([9a8ed6b](https://gitlab.com/yellowhat-labs/utils/commit/9a8ed6be542321d8f2eded257a8a79a5fc2d8161))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.440.0 ([d755745](https://gitlab.com/yellowhat-labs/utils/commit/d7557458d515601776304eb44aff3e2b629293af))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.440.5 ([eac9bb3](https://gitlab.com/yellowhat-labs/utils/commit/eac9bb3b3374336213e74fd4ff54ff30a1159b08))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.440.7 ([03bd6ad](https://gitlab.com/yellowhat-labs/utils/commit/03bd6adf9d38efae7af6760bdb5f1237642b9189))
* **deps:** update python:3.12.4 docker digest to 389f624 ([e8f0399](https://gitlab.com/yellowhat-labs/utils/commit/e8f0399e4125c7f9e14edc6c7a7fa105668ebbe7))
* **deps:** update python:3.12.4 docker digest to 8e9e432 ([cd6f7f2](https://gitlab.com/yellowhat-labs/utils/commit/cd6f7f25b7f9ba5493f3ee1bf647e940e38de62f))
* **deps:** update python:3.12.4 docker digest to b40b4e5 ([143e807](https://gitlab.com/yellowhat-labs/utils/commit/143e807c17f944068c1058cc536845a14b98beb0))
* **deps:** update python:3.12.4-alpine docker digest to 100d96c ([31ee449](https://gitlab.com/yellowhat-labs/utils/commit/31ee449c1bfc27fc115d7aba63eba8d3af1292de))
* **deps:** update python:3.12.4-alpine docker digest to 7f15e22 ([510a9cc](https://gitlab.com/yellowhat-labs/utils/commit/510a9cc4d1790d3c7917b48c1e7175990bf0377a))
* **deps:** update python:3.12.4-alpine docker digest to d7a5a8b ([40162c2](https://gitlab.com/yellowhat-labs/utils/commit/40162c2b7f4f81896b4d003e217ac3cc961de8f5))
* **deps:** update registry.gitlab.com/gitlab-org/cli docker tag to v1.44.1 ([f1d3ca5](https://gitlab.com/yellowhat-labs/utils/commit/f1d3ca5e004772e0e3aeb4d7029e97a8d8cbbde7))
* **deps:** update returntocorp/semgrep docker tag to v1.81 ([6e6ae7a](https://gitlab.com/yellowhat-labs/utils/commit/6e6ae7a426628478423a3171445bb012b213be96))

# [1.9.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.8.4...v1.9.0) (2024-07-20)


### Features

* add template to comment MRs ([553eeef](https://gitlab.com/yellowhat-labs/utils/commit/553eeef7ffdb96652a63ce0e93948a79deb9da82))


### Chore

* **deps:** update dependency crate-ci/typos to v1.23.2 ([a34f092](https://gitlab.com/yellowhat-labs/utils/commit/a34f09224c0f64211d12e359155a4fe1e5721db1))
* **deps:** update dependency mypy to v1.11.0 ([db296ab](https://gitlab.com/yellowhat-labs/utils/commit/db296aba9fab3dbd4d0c1ee57328dafc2dd96e6a))
* **deps:** update dependency ruff to v0.5.2 ([7061de9](https://gitlab.com/yellowhat-labs/utils/commit/7061de9159f319f1113a5dec56a361f841b38a1d))
* **deps:** update dependency ruff to v0.5.3 ([cb7f6a4](https://gitlab.com/yellowhat-labs/utils/commit/cb7f6a422a90c5cd94e3691e506a8d75437d190e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.426.2 ([1a06314](https://gitlab.com/yellowhat-labs/utils/commit/1a06314ea9d388ff5920a9bedba9a445db33ffa2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.426.5 ([096a318](https://gitlab.com/yellowhat-labs/utils/commit/096a31821873d4f205388144034c4b450d834b4d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.428.2 ([d68673c](https://gitlab.com/yellowhat-labs/utils/commit/d68673c41c54d224fd9a7465d3da50ce93d05ada))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.429.0 ([68946cd](https://gitlab.com/yellowhat-labs/utils/commit/68946cdb4dde482761c06f669fe4b9ec048a5ddf))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.431.4 ([f920952](https://gitlab.com/yellowhat-labs/utils/commit/f920952b4609231c1047c7a54e2363ed2bef6c17))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.431.7 ([9c52e12](https://gitlab.com/yellowhat-labs/utils/commit/9c52e12a4ddf7bbafc002a3647e380178b452d95))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.432.0 ([b2c98f2](https://gitlab.com/yellowhat-labs/utils/commit/b2c98f2cc665a4f74f622b5e4943c073950b1d7d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.433.2 ([0394357](https://gitlab.com/yellowhat-labs/utils/commit/039435794c1a3ad452ed5c2f9aaaadbbf2f57ace))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.434.3 ([e7fff45](https://gitlab.com/yellowhat-labs/utils/commit/e7fff452c2d7a9de418f42a641940b29ae73190f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.435.1 ([742fb49](https://gitlab.com/yellowhat-labs/utils/commit/742fb492304f0d0c0a1e55f9de61ddb3d31649a7))
* **deps:** update python:3.12.4 docker digest to b6f142b ([db64e34](https://gitlab.com/yellowhat-labs/utils/commit/db64e343a8dee23a15cdd427342a16434d6e8891))
* **deps:** update python:3.12.4-alpine docker digest to 0bd77ae ([4b403d1](https://gitlab.com/yellowhat-labs/utils/commit/4b403d143ab7d325fbcf091131dffd2ecbeba34e))
* **deps:** update returntocorp/semgrep docker tag to v1.79 ([0eb3a03](https://gitlab.com/yellowhat-labs/utils/commit/0eb3a035c25abad55ce34245e9280cdaffd61c21))
* **deps:** update returntocorp/semgrep docker tag to v1.80 ([035e0b3](https://gitlab.com/yellowhat-labs/utils/commit/035e0b312c98c26706f292367589254dee6d1bee))


### renovate

* better handle of " or ' ([21d427e](https://gitlab.com/yellowhat-labs/utils/commit/21d427e858ab2f4c0f641a8658558f94be7a9f16))


### Continuous Integration

* **renovate:** make regex generic ([a47406a](https://gitlab.com/yellowhat-labs/utils/commit/a47406af8074924438adc7814f20bba8a87a4664))
* use var in compare_to ([5558599](https://gitlab.com/yellowhat-labs/utils/commit/55585994fc377df9e025299dd10cc09a0e8e7163))

## [1.8.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.8.3...v1.8.4) (2024-07-08)


### Chore

* **deps:** update dependency crate-ci/typos to v1.23.0 ([fa46036](https://gitlab.com/yellowhat-labs/utils/commit/fa4603650131701e708bc440f6f654cc12e07444))
* **deps:** update dependency crate-ci/typos to v1.23.1 ([937c80e](https://gitlab.com/yellowhat-labs/utils/commit/937c80edee1b3caf516e10a1a1a5acf7919bcb2a))
* **deps:** update dependency ruff to v0.5.1 ([da2b2b9](https://gitlab.com/yellowhat-labs/utils/commit/da2b2b973eef802c7b19fcc858c01d022010bd9e))
* **deps:** update docker.io/nixos/nix docker tag to v2.23.2 ([4a40931](https://gitlab.com/yellowhat-labs/utils/commit/4a409317c826667ab345e7e0b66669b883905a30))
* **deps:** update docker.io/nixos/nix docker tag to v2.23.3 ([6f2424f](https://gitlab.com/yellowhat-labs/utils/commit/6f2424f884602628983a8b4c6be84c3cde682b5b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.421.6 ([9c0f349](https://gitlab.com/yellowhat-labs/utils/commit/9c0f349743e29d781def62b68fbac73ec23cf591))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.421.7 ([24e285c](https://gitlab.com/yellowhat-labs/utils/commit/24e285c9cd927daca16d665c293e58a3acb76efd))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.422.2 ([b3e9876](https://gitlab.com/yellowhat-labs/utils/commit/b3e987672413a6d75421390dbc110af381f8f29f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.423.1 ([ba3d865](https://gitlab.com/yellowhat-labs/utils/commit/ba3d86539a8e68a45d4fc3bfae9f680f202f45f0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.424.1 ([c239966](https://gitlab.com/yellowhat-labs/utils/commit/c23996613dcf67f437a65c895f69d872f0196855))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.424.3 ([2a40bc6](https://gitlab.com/yellowhat-labs/utils/commit/2a40bc61a575fe95c1b8b242206d65934e860c02))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.425.0 ([a955d0c](https://gitlab.com/yellowhat-labs/utils/commit/a955d0c0c194571f57def1ad9b5e6ef7f57c01f5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.425.1 ([ffcd046](https://gitlab.com/yellowhat-labs/utils/commit/ffcd046c69c9b792d0f36baeb80c6cc5e68cee05))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.426.0 ([5120af0](https://gitlab.com/yellowhat-labs/utils/commit/5120af092ef7e809e5c98a3132228c99983baffd))
* **deps:** update python:3.12.4 docker digest to 212a741 ([7b3c1b4](https://gitlab.com/yellowhat-labs/utils/commit/7b3c1b44d9c38e37958e8362057bc14bb24e56e7))
* **deps:** update python:3.12.4 docker digest to 2eedc86 ([bd0ac77](https://gitlab.com/yellowhat-labs/utils/commit/bd0ac77f4ece60a2d03cb80611e3115e3500fa70))
* **deps:** update python:3.12.4 docker digest to d12b529 ([d68b722](https://gitlab.com/yellowhat-labs/utils/commit/d68b722da5a54ea8070fae73de40469010a2b7ea))
* **deps:** update python:3.12.4-alpine docker digest to ab8bec3 ([13eacac](https://gitlab.com/yellowhat-labs/utils/commit/13eacacfc6c72c87fbbede10c1a6000fb77a4db1))
* **deps:** update python:3.12.4-alpine docker digest to b7662fc ([bd409c1](https://gitlab.com/yellowhat-labs/utils/commit/bd409c12732c7950626eba9642ca36080e2b0e4f))


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.2.0 ([aec7289](https://gitlab.com/yellowhat-labs/utils/commit/aec7289a4cedcf271347bc57aa0c4e8a04623d78))


### renovate

* **regex:** consider case "<var>_VER = <ver>" ([5319593](https://gitlab.com/yellowhat-labs/utils/commit/5319593ccbf15f6170b0429ed96cc40b132dab1e))

## [1.8.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.8.2...v1.8.3) (2024-07-01)


### Bug Fixes

* remove unused docs ([5286dff](https://gitlab.com/yellowhat-labs/utils/commit/5286dffe3df3c0b1af0cb81bf2ba777b5a61d6ba))
* remove unused docs ([ab9c93f](https://gitlab.com/yellowhat-labs/utils/commit/ab9c93f3e4ba5e74152a4481503843cf5b5e2363))


### Chore

* **deps:** update dependency @biomejs/biome to v1.8.3 ([9a057f8](https://gitlab.com/yellowhat-labs/utils/commit/9a057f854cc06ceb1cd7d2e95612a8395769fcd8))
* **deps:** update dependency blacken-docs to v1.18.0 ([fd1633a](https://gitlab.com/yellowhat-labs/utils/commit/fd1633a713cf76f2671fa425ffbc0e45e1553876))
* **deps:** update dependency crate-ci/typos to v1.22.9 ([98d5635](https://gitlab.com/yellowhat-labs/utils/commit/98d56359ad5aa03ec28efd43c84ac4f213bea5a7))
* **deps:** update dependency mypy to v1.10.1 ([8352289](https://gitlab.com/yellowhat-labs/utils/commit/83522896211ef9ab81d7a977f40d135144563d03))
* **deps:** update dependency pylint to v3.2.4 ([e990693](https://gitlab.com/yellowhat-labs/utils/commit/e990693ba5040d9c03856554c580f106173b964d))
* **deps:** update dependency pylint to v3.2.5 ([6a14cb7](https://gitlab.com/yellowhat-labs/utils/commit/6a14cb7cd5cc0c93055b298391e96d51b52dae35))
* **deps:** update dependency ruff to v0.5.0 ([008f324](https://gitlab.com/yellowhat-labs/utils/commit/008f324fa1d8b9459c1ae0d55ca0ba79b22d446d))
* **deps:** update docker.io/nixos/nix docker tag to v2.23.1 ([ee6c75f](https://gitlab.com/yellowhat-labs/utils/commit/ee6c75fc2866ef20fc268a48db933674ac56e15b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.415.0 ([634813b](https://gitlab.com/yellowhat-labs/utils/commit/634813ba95146b4fcbb0d31dd656b81d437e347e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.417.1 ([9be1896](https://gitlab.com/yellowhat-labs/utils/commit/9be18964d3aee3e05cbdd178fdcbb31a37623242))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.420.1 ([018c79f](https://gitlab.com/yellowhat-labs/utils/commit/018c79f67de3a7f56a6f198df70a8f54ee7bd2b9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.421.0 ([d31a04f](https://gitlab.com/yellowhat-labs/utils/commit/d31a04f332088309d1d78727352700bc8310d4b5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.421.3 ([921706e](https://gitlab.com/yellowhat-labs/utils/commit/921706ea2ed9c43969d6528c0dcc417b0db67aef))
* **deps:** update python:3.12.4 docker digest to fce9bc7 ([c8f1664](https://gitlab.com/yellowhat-labs/utils/commit/c8f1664e12809a0e30804430ea32a406e81b846e))
* **deps:** update python:3.12.4-alpine docker digest to ff870bf ([6e9602b](https://gitlab.com/yellowhat-labs/utils/commit/6e9602be6743c13d5f83eb3ec0268524298ef77b))
* **deps:** update returntocorp/semgrep docker tag to v1.77 ([256faf5](https://gitlab.com/yellowhat-labs/utils/commit/256faf5a2ae007df4e1b5f39dc20e0321cd90907))
* **deps:** update returntocorp/semgrep docker tag to v1.78 ([d049120](https://gitlab.com/yellowhat-labs/utils/commit/d0491203a515778ef48c83be75f1cd4aa12e5943))

## [1.8.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.8.1...v1.8.2) (2024-06-22)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v14.0.1 ([eb85409](https://gitlab.com/yellowhat-labs/utils/commit/eb8540982cda1b7d43f8a0b9b148a028b649d3ab))


### Chore

* **deps:** update alpine:latest docker digest to b89d9c9 ([3cd20de](https://gitlab.com/yellowhat-labs/utils/commit/3cd20debb3bc2b263272fcd29fd84f59b73178ab))
* **deps:** update dependency @biomejs/biome to v1.8.2 ([9bae708](https://gitlab.com/yellowhat-labs/utils/commit/9bae708b0c08fa341f433de8c5eccb8c07557605))
* **deps:** update dependency crate-ci/typos to v1.22.8 ([a120972](https://gitlab.com/yellowhat-labs/utils/commit/a1209725a9a00a8c874e8b3f69261915cde92cf6))
* **deps:** update dependency ruff to v0.4.10 ([dcc9134](https://gitlab.com/yellowhat-labs/utils/commit/dcc9134cc7dc56b61fad3bba6a6b78cbb6a85d3a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.410.2 ([9880197](https://gitlab.com/yellowhat-labs/utils/commit/988019766a1e824689bc360b71a3a2d9a791af24))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.412.2 ([4838809](https://gitlab.com/yellowhat-labs/utils/commit/4838809ce58612d7cd1034d5ad96f08d06568b3b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.413.2 ([ce46152](https://gitlab.com/yellowhat-labs/utils/commit/ce46152a434fa7bcb6c960d02e3add0f2be6dc35))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.413.4 ([70b1596](https://gitlab.com/yellowhat-labs/utils/commit/70b15969c9fc23894777933ae5984953d163ee26))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.414.1 ([8a38196](https://gitlab.com/yellowhat-labs/utils/commit/8a381966d412c895534649197c305cf40f1ebac5))
* **deps:** update python:3.12.4-alpine docker digest to dc09596 ([09eba93](https://gitlab.com/yellowhat-labs/utils/commit/09eba93167ed90b45261fed6f61bf57632d5dd0c))
* **deps:** update returntocorp/semgrep docker tag to v1.76 ([13083d4](https://gitlab.com/yellowhat-labs/utils/commit/13083d4e43591a77f57096ff07c5261f7c1f5c2a))

## [1.8.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.8.0...v1.8.1) (2024-06-17)


### Bug Fixes

* **build:** tags ([95dd144](https://gitlab.com/yellowhat-labs/utils/commit/95dd1443e560d68b2bc44dae0ca6dbfbd2608597))


### Continuous Integration

* **renovate:** add podinfo ([b8b4351](https://gitlab.com/yellowhat-labs/utils/commit/b8b4351319ff1189bd663a2cd1377b89a47809b7))
* **renovate:** fix ([cd722f2](https://gitlab.com/yellowhat-labs/utils/commit/cd722f2ea79f38a8fd0b1575517907ad6423eb64))
* **renovate:** fix ([506d12e](https://gitlab.com/yellowhat-labs/utils/commit/506d12e74cda2808795325dae6b0594774be531c))


### Chore

* add LICENSE ([fa93afa](https://gitlab.com/yellowhat-labs/utils/commit/fa93afa91ae26a552c0005a319df483c20fa2dfc))
* **deps:** update alpine:edge docker digest to 166710d ([88f19c2](https://gitlab.com/yellowhat-labs/utils/commit/88f19c2d5ed808c0724b7b16458e9115c788a96b))
* **deps:** update dependency @biomejs/biome to v1.8.1 ([44c84e6](https://gitlab.com/yellowhat-labs/utils/commit/44c84e61f4468d486d526bfd68eaf4c0f38909cf))
* **deps:** update dependency bandit to v1.7.9 ([45a976f](https://gitlab.com/yellowhat-labs/utils/commit/45a976f1b803a2815eda5eeae006bbd24de7a3cf))
* **deps:** update dependency crate-ci/typos to v1.22.3 ([8db9f9a](https://gitlab.com/yellowhat-labs/utils/commit/8db9f9a4e1ec10d99c2834a108ad20f9e5de6522))
* **deps:** update dependency crate-ci/typos to v1.22.4 ([ada8e21](https://gitlab.com/yellowhat-labs/utils/commit/ada8e21e13d5a1b434a3ac097075a430415393f1))
* **deps:** update dependency crate-ci/typos to v1.22.7 ([b4a31e8](https://gitlab.com/yellowhat-labs/utils/commit/b4a31e85e5e32e5fefd2fd234c321e219e748b14))
* **deps:** update dependency flake8 to v7.1.0 ([7bebe7e](https://gitlab.com/yellowhat-labs/utils/commit/7bebe7e5ed004b1c989f6cbdb74a7d00e309a455))
* **deps:** update dependency markdown-table-formatter to v1.6.1 ([b4c63cc](https://gitlab.com/yellowhat-labs/utils/commit/b4c63cc3bcdb4ea143de4ca5c02b9b0a245d3eef))
* **deps:** update dependency pyupgrade to v3.16.0 ([7a570c4](https://gitlab.com/yellowhat-labs/utils/commit/7a570c42cf3c468a2ed3cdba63d6b33d60a75197))
* **deps:** update dependency ruff to v0.4.9 ([f7f6cd5](https://gitlab.com/yellowhat-labs/utils/commit/f7f6cd5856b9294c5933099844ce1fd6430424ed))
* **deps:** update docker.io/nixos/nix docker tag to v2.23.0 ([185419b](https://gitlab.com/yellowhat-labs/utils/commit/185419ba8cc08e9385200ee30d68a7b38542d31a))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.23.1 ([7411625](https://gitlab.com/yellowhat-labs/utils/commit/74116257cf5ba587bbf682bee4d11cf90dfa621f))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.18.4 ([3916fe9](https://gitlab.com/yellowhat-labs/utils/commit/3916fe983ba1a90824a2b890d3324db7687fab9c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.394.1 ([10798ef](https://gitlab.com/yellowhat-labs/utils/commit/10798ef458afcb73633162d955b52832f2676770))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.395.0 ([ba2313e](https://gitlab.com/yellowhat-labs/utils/commit/ba2313e64d4898fb67e26ac2c29176f2db5e231f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.399.10 ([93db602](https://gitlab.com/yellowhat-labs/utils/commit/93db602493795082ee21fffcd9c81c070ba1be24))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.399.8 ([c13ee3f](https://gitlab.com/yellowhat-labs/utils/commit/c13ee3fc0c7656e34defcdb544caa02802a16e85))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.401.1 ([48593af](https://gitlab.com/yellowhat-labs/utils/commit/48593af3520b5d4134ec904834bbfee96936a183))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.401.4 ([ba7da4e](https://gitlab.com/yellowhat-labs/utils/commit/ba7da4e064f1c996860ec3892c578aa719c8b1ad))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.402.1 ([996d9ba](https://gitlab.com/yellowhat-labs/utils/commit/996d9ba66312c9ffd2cb4b4c9da59da92aa303e3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.405.0 ([03e8f66](https://gitlab.com/yellowhat-labs/utils/commit/03e8f667513d37a4693711cf9044688d7b92d597))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.406.1 ([cce3fcb](https://gitlab.com/yellowhat-labs/utils/commit/cce3fcb17bff52a8a740558433f6e0dccb6b87d2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.406.2 ([397461f](https://gitlab.com/yellowhat-labs/utils/commit/397461ffa536e244b5d38166309696057a2fa211))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.407.2 ([185d628](https://gitlab.com/yellowhat-labs/utils/commit/185d628c3b7d472e96f0310bd2f49b52e20e8c31))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.407.4 ([3799924](https://gitlab.com/yellowhat-labs/utils/commit/3799924af32dd850e3043345fca46745bf7fa555))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.408.0 ([ee5c993](https://gitlab.com/yellowhat-labs/utils/commit/ee5c993d6405e4dc8c075e071de1730c9a109a4d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.409.1 ([84babe0](https://gitlab.com/yellowhat-labs/utils/commit/84babe02d35db500f67d1f68b75e6c8f2e28aec3))
* **deps:** update python docker tag to v3.12.4 ([f66362b](https://gitlab.com/yellowhat-labs/utils/commit/f66362bb21739e3b80ca5efc81b866f01bac5cba))
* **deps:** update python:3.12.4 docker digest to d3d5f8a ([3e6c2c4](https://gitlab.com/yellowhat-labs/utils/commit/3e6c2c455c6c8ca08a1c6701803fa19b5c80099e))
* **deps:** update python:3.12.4-alpine docker digest to a982997 ([b66e89b](https://gitlab.com/yellowhat-labs/utils/commit/b66e89b46c50d474968855b953a838de0bfa6c58))

# [1.8.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.7.3...v1.8.0) (2024-06-06)


### Chore

* **deps:** update dependency crate-ci/typos to v1.22.1 ([9166061](https://gitlab.com/yellowhat-labs/utils/commit/9166061d65d3fccc227be859c81ae8e9b5c6193e))
* **deps:** update dependency pylint to v3.2.3 ([7ec1902](https://gitlab.com/yellowhat-labs/utils/commit/7ec1902d53c405912f7a96c0cf41c68b95a9b986))
* **deps:** update dependency ruff to v0.4.8 ([04b8d8f](https://gitlab.com/yellowhat-labs/utils/commit/04b8d8f1fa014f9eba77e4bd6b2211032b24d9a8))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.391.0 ([14a1b42](https://gitlab.com/yellowhat-labs/utils/commit/14a1b421d3ac957a4b561e412ceaa60c8255b213))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.392.0 ([7c1b7de](https://gitlab.com/yellowhat-labs/utils/commit/7c1b7de0bcf59f20fc6e24c4206af3f6c8c0d9f5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.394.0 ([d9b7fd0](https://gitlab.com/yellowhat-labs/utils/commit/d9b7fd0fc0cc5680e3ca0d0a866672aff22a362e))
* **deps:** update python:3.12.3-alpine docker digest to 32385e6 ([d7f4feb](https://gitlab.com/yellowhat-labs/utils/commit/d7f4feb4c30dddbb293073f52f3444e01a0f1124))
* **deps:** update python:3.12.3-alpine docker digest to d696c99 ([8013653](https://gitlab.com/yellowhat-labs/utils/commit/8013653d980acbf62424bb46f2110cd135c63c2a))


### Features

* **template:** add build ([9db96a0](https://gitlab.com/yellowhat-labs/utils/commit/9db96a0ea49ed7e9b8003ba0c78ddc374b40c723))

## [1.7.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.7.2...v1.7.3) (2024-06-05)


### Bug Fixes

* **deps:** update semantic-release monorepo (major) ([66fd609](https://gitlab.com/yellowhat-labs/utils/commit/66fd609bdd3515bb10f08e7e02b06e53d435392b))
* **semantic-release:** immutable objects ([f042090](https://gitlab.com/yellowhat-labs/utils/commit/f0420901461c1454932dc8bfd0bf19794b76ed92))


### Documentation

* add semantic-release ([b32f7dc](https://gitlab.com/yellowhat-labs/utils/commit/b32f7dc3c626396b22d35bac10f057a13c789c1a))


### Chore

* **deps:** update alpine:latest docker digest to 77726ef ([7387849](https://gitlab.com/yellowhat-labs/utils/commit/7387849ae98dc513a2e05fff4492bd14446276ae))
* **deps:** update dependency @biomejs/biome to v1.8.0 ([4b75c43](https://gitlab.com/yellowhat-labs/utils/commit/4b75c439f346fb2e495ce2d81b7162743049ef98))
* **deps:** update dependency alpine_3_20/git to v2.45.2-r0 ([f6e5e34](https://gitlab.com/yellowhat-labs/utils/commit/f6e5e349f46e5a81c1ac17279946caa625a2d796))
* **deps:** update dependency codespell to v2.3.0 ([07d0de3](https://gitlab.com/yellowhat-labs/utils/commit/07d0de3dc61f684d96865af8695bbc0a62eea521))
* **deps:** update dependency crate-ci/typos to v1.22.0 ([fe18b96](https://gitlab.com/yellowhat-labs/utils/commit/fe18b96b39e88ccbf17f0785d04e0f0023a545b2))
* **deps:** update dependency ruff to v0.4.5 ([2f95e29](https://gitlab.com/yellowhat-labs/utils/commit/2f95e299ab4d614f767ac5adcfa945cb210c01bd))
* **deps:** update dependency ruff to v0.4.6 ([682214a](https://gitlab.com/yellowhat-labs/utils/commit/682214aadefa7731928a9b2e9e08ffe02c502183))
* **deps:** update dependency ruff to v0.4.7 ([bc7a2dd](https://gitlab.com/yellowhat-labs/utils/commit/bc7a2dd1410a3888faa874d6d1c32b92d4391af3))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.18.3 ([9bf4437](https://gitlab.com/yellowhat-labs/utils/commit/9bf4437ef65c3c08558b030be1d206a01d33dce6))
* **deps:** update ghcr.io/igorshubovych/markdownlint-cli docker tag to v0.41.0 ([f115b13](https://gitlab.com/yellowhat-labs/utils/commit/f115b1323c214e263546c6e2108374129262b29f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.368.10 ([475a1eb](https://gitlab.com/yellowhat-labs/utils/commit/475a1eb6a4c7a3e68b97566c32eca5aec44c9c00))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.371.1 ([1ae1cbc](https://gitlab.com/yellowhat-labs/utils/commit/1ae1cbc4af9aebaafbd22a5a9785d09267989f07))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.372.1 ([2889f94](https://gitlab.com/yellowhat-labs/utils/commit/2889f94b4f1bb0b0975ee0ee1a74bd09afa48e5d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.374.3 ([068de22](https://gitlab.com/yellowhat-labs/utils/commit/068de22394eba9abb3e774951846351fc3ef1780))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.376.0 ([fa00b7a](https://gitlab.com/yellowhat-labs/utils/commit/fa00b7ac20c0b19c7d9afb481210e0246d28dfec))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.377.0 ([5790351](https://gitlab.com/yellowhat-labs/utils/commit/57903512e459bfe14c0826d29ecd5c434bfa668a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.377.4 ([31a08e5](https://gitlab.com/yellowhat-labs/utils/commit/31a08e57ca68fc96711c0aa5a76e629ab8df42b4))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.377.5 ([5a5d1b6](https://gitlab.com/yellowhat-labs/utils/commit/5a5d1b66c2b9717d77ad8ff6e6106439d3091042))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.377.8 ([a8b4ca9](https://gitlab.com/yellowhat-labs/utils/commit/a8b4ca9583d0375eddd44d17e737e9c749ae348d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.379.1 ([8d00917](https://gitlab.com/yellowhat-labs/utils/commit/8d00917c40590a278ae793d9edc8c96384368337))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.381.6 ([cc515c5](https://gitlab.com/yellowhat-labs/utils/commit/cc515c598bd481259eb3dced95ed782ffb57f9d6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.382.0 ([ad31599](https://gitlab.com/yellowhat-labs/utils/commit/ad31599eb9a47009aa4315d9d6c846e02b716e98))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.382.4 ([e4f0155](https://gitlab.com/yellowhat-labs/utils/commit/e4f0155df78bf82501d6d3ec744e475de2be40f1))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.385.0 ([aecfcc9](https://gitlab.com/yellowhat-labs/utils/commit/aecfcc99cc90370388c0949b49969f483a2902e1))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.388.1 ([6b482eb](https://gitlab.com/yellowhat-labs/utils/commit/6b482ebc36136771b863acb5e8fdd152d6cf52f0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.390.1 ([f5c5fbf](https://gitlab.com/yellowhat-labs/utils/commit/f5c5fbf0641d2cfb18a8102f5ab4e818dfb4a992))
* **deps:** update ghcr.io/tcort/markdown-link-check docker tag to v3.12.2 ([0f1ca00](https://gitlab.com/yellowhat-labs/utils/commit/0f1ca00a3227f25ccbd81adfa63fbada841044bf))
* **deps:** update node.js to 6908bbe ([9b98b6e](https://gitlab.com/yellowhat-labs/utils/commit/9b98b6eef4c8b083ebe52befca56061434596e6e))
* **deps:** update node.js to e5ffd0f ([12e2c3c](https://gitlab.com/yellowhat-labs/utils/commit/12e2c3cc84c4cf11528488e9a1d13cee40a7ccc2))
* **deps:** update python:3.12.3-alpine docker digest to 42e1e49 ([6d9d1fc](https://gitlab.com/yellowhat-labs/utils/commit/6d9d1fc9f1daf60c403ac5c952e697960886e82d))
* **deps:** update python:3.12.3-alpine docker digest to 5365725 ([f41998a](https://gitlab.com/yellowhat-labs/utils/commit/f41998a8eab0cea6f06cdedd357fe0f87a6b55ad))
* **deps:** update returntocorp/semgrep docker tag to v1.74 ([96ff1b9](https://gitlab.com/yellowhat-labs/utils/commit/96ff1b962da1afb32edd89a219d80181182c16eb))
* **deps:** update returntocorp/semgrep docker tag to v1.75 ([8b25a3d](https://gitlab.com/yellowhat-labs/utils/commit/8b25a3d64b62e2ccbe9f54e12d4b0032817357b7))


* use alpine 3.20 ([780fed1](https://gitlab.com/yellowhat-labs/utils/commit/780fed197d5a9ff3382f06920636720281b84729))

## [1.7.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.7.1...v1.7.2) (2024-05-20)


### Continuous Integration

* **renovate:** add pulimi ([(0de21cc)](https://gitlab.com/yellowhat-labs/utils/commit/0de21cc61792dbbd7cb2424931296bfa57153dec))


### Documentation

* chore ([(03b8c68)](https://gitlab.com/yellowhat-labs/utils/commit/03b8c68ec38842d72927e5f7921eeb1fc5581931))

## [1.7.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.7.0...v1.7.1) (2024-05-20)


### Bug Fixes

* **release:** set GIT_DEPTH ([(ec373a5)](https://gitlab.com/yellowhat-labs/utils/commit/ec373a5c91f7afadf4a13ce87ce64a7bd65df0d3))


### Continuous Integration

* **renovate:** solax -> solax-exporter ([(45f2404)](https://gitlab.com/yellowhat-labs/utils/commit/45f2404186d296d077123f3332f5260f25c3f1c1))


### Chore

* **deps:** update dependency pylint to v3.2.1 ([(7479a58)](https://gitlab.com/yellowhat-labs/utils/commit/7479a5825dd18205f7f1a132f796f8aaa98651aa))
* **deps:** update dependency pylint to v3.2.2 ([(0a4f2c9)](https://gitlab.com/yellowhat-labs/utils/commit/0a4f2c9d8132068558b01c7c5f75010033752cbf))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.368.6 ([(24ce3d6)](https://gitlab.com/yellowhat-labs/utils/commit/24ce3d6d65fa9b232aade871ec16cc9df8440478))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.368.7 ([(25f18f4)](https://gitlab.com/yellowhat-labs/utils/commit/25f18f49b5268936e5411225a842aa23fac99fc6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.368.9 ([(e9437a4)](https://gitlab.com/yellowhat-labs/utils/commit/e9437a4eb834f9f1447bc63d5710f40e72d2d116))

# [1.7.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.6.4...v1.7.0) (2024-05-17)


### Features

* **templates:** use input parameters ([(bcf5168)](https://gitlab.com/yellowhat-labs/utils/commit/bcf51688a606f80e852df65e6efc3d4150c1b846))


### Chore

* **deps:** update dependency alpine_3_19/git to v2.43.4-r0 ([(5aff877)](https://gitlab.com/yellowhat-labs/utils/commit/5aff87762c2f04d7750e8c12a342cae680a67037))
* **deps:** update dependency pylint to v3.1.1 ([(403e88f)](https://gitlab.com/yellowhat-labs/utils/commit/403e88fb8a12ebb55dfe2a2625731b5bd88464f4))
* **deps:** update dependency pylint to v3.2.0 ([(767559d)](https://gitlab.com/yellowhat-labs/utils/commit/767559d3d824e7b6f0c109547974766390dd74c1))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.23.0 ([(dcfd475)](https://gitlab.com/yellowhat-labs/utils/commit/dcfd475950347679ff3d2b01faf636d96a76d9f7))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.357.0 ([(4f8df78)](https://gitlab.com/yellowhat-labs/utils/commit/4f8df784c623e5b0968cd75b2e77d940fcdcc97a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.361.0 ([(51c7af0)](https://gitlab.com/yellowhat-labs/utils/commit/51c7af0637ad4011b912f1ed2059b7eae167bde5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.362.0 ([(a2b2374)](https://gitlab.com/yellowhat-labs/utils/commit/a2b237422b9a697ec99d7f7c6cc74c9128e9a50c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.363.5 ([(d34223f)](https://gitlab.com/yellowhat-labs/utils/commit/d34223fa467aadb804eef3c74c9f19c45995748f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.363.9 ([(ad8324f)](https://gitlab.com/yellowhat-labs/utils/commit/ad8324f953a5f6654799fa4d095386f9cb5f095a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.364.0 ([(2d49692)](https://gitlab.com/yellowhat-labs/utils/commit/2d49692fba2e3027021a5c51e8f5ac427a2f78be))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.368.2 ([(23ff344)](https://gitlab.com/yellowhat-labs/utils/commit/23ff344d03f579d06d97fdd1c201e3a863cff786))
* **deps:** update node.js to 866cb83 ([(68c012d)](https://gitlab.com/yellowhat-labs/utils/commit/68c012d797f6faf5cf5cfe7e9dfce9011664d655))
* **deps:** update python:3.12.3 docker digest to 76831bb ([(4939547)](https://gitlab.com/yellowhat-labs/utils/commit/4939547d8dc4a58fceb77d691efae4380e0fbd2b))
* **deps:** update returntocorp/semgrep docker tag to v1.73 ([(a11783a)](https://gitlab.com/yellowhat-labs/utils/commit/a11783a47d2b0034164ad7c0de987f16b23d649c))

## [1.6.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.6.3...v1.6.4) (2024-05-13)


### Bug Fixes

* **deps:** update semantic-release monorepo ([(e04b770)](https://gitlab.com/yellowhat-labs/utils/commit/e04b7702d2f466ccb90d14f61c5e4d737abcaf75))


### Chore

* **deps:** update dependency @biomejs/biome to v1.7.0 ([(63e18b3)](https://gitlab.com/yellowhat-labs/utils/commit/63e18b3d4343438f90c361629ce20bdd5464467b))
* **deps:** update dependency @biomejs/biome to v1.7.1 ([(8284013)](https://gitlab.com/yellowhat-labs/utils/commit/82840135c8863a35f534bdc23c0428492fb6059a))
* **deps:** update dependency @biomejs/biome to v1.7.3 ([(f323a17)](https://gitlab.com/yellowhat-labs/utils/commit/f323a176bf7e842937a8d615a83cd6a32271982c))
* **deps:** update dependency crate-ci/typos to v1.20.10 ([(a33b994)](https://gitlab.com/yellowhat-labs/utils/commit/a33b994a418fc52ff74acc2061ecf093b9c6d070))
* **deps:** update dependency crate-ci/typos to v1.20.9 ([(a9d2f7e)](https://gitlab.com/yellowhat-labs/utils/commit/a9d2f7edb4b13d25621d346c663aed5cff88a2ce))
* **deps:** update dependency crate-ci/typos to v1.21.0 ([(bee3ab6)](https://gitlab.com/yellowhat-labs/utils/commit/bee3ab6ecbe1bebeafb62363a2883029e3f8e2e0))
* **deps:** update dependency markdown-table-formatter to v1.6.0 ([(d7e456d)](https://gitlab.com/yellowhat-labs/utils/commit/d7e456dbba78e53c53090ff1f6865b72549a54ad))
* **deps:** update dependency mypy to v1.10.0 ([(da84193)](https://gitlab.com/yellowhat-labs/utils/commit/da841936f9f4375030632659f18a8598982e9d31))
* **deps:** update dependency ruff to v0.4.1 ([(b07f86f)](https://gitlab.com/yellowhat-labs/utils/commit/b07f86fd43067a9c5247710b35787d7d7e6ba33c))
* **deps:** update dependency ruff to v0.4.2 ([(1ad8518)](https://gitlab.com/yellowhat-labs/utils/commit/1ad8518093cc372c93207e8c03c8a548066b34e4))
* **deps:** update dependency ruff to v0.4.4 ([(30ff4e7)](https://gitlab.com/yellowhat-labs/utils/commit/30ff4e78bc104b02b7574a9a1392533d9fe5babf))
* **deps:** update docker.io/nixos/nix docker tag to v2.22.0 ([(55c1c17)](https://gitlab.com/yellowhat-labs/utils/commit/55c1c17c55572864d0239e4502268dd5912ab2b5))
* **deps:** update docker.io/nixos/nix docker tag to v2.22.1 ([(32600d5)](https://gitlab.com/yellowhat-labs/utils/commit/32600d5275bde4f27b803ddb061cffedf14fbfdb))
* **deps:** update docker.io/nixos/nix:2.22.0 docker digest to 6913f4e ([(2b48e2c)](https://gitlab.com/yellowhat-labs/utils/commit/2b48e2c3bf4aa0062e2f4aa287c5acdfb4934cc6))
* **deps:** update ghcr.io/igorshubovych/markdownlint-cli docker tag to v0.40.0 ([(b0634f4)](https://gitlab.com/yellowhat-labs/utils/commit/b0634f41f4d03b5d646d30ce27fb95a8c97f1e7b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.291.1 ([(fbc670a)](https://gitlab.com/yellowhat-labs/utils/commit/fbc670aebeece20e89132d1ca971e7895af68f64))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.293.0 ([(df8d60d)](https://gitlab.com/yellowhat-labs/utils/commit/df8d60dc6547909821580d6d0f6d21ddebd5e946))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.298.0 ([(137df41)](https://gitlab.com/yellowhat-labs/utils/commit/137df414e76d412cd779f5cc83ccfcd920d7b1d4))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.315.0 ([(07d2b95)](https://gitlab.com/yellowhat-labs/utils/commit/07d2b95fb033a4134518d27bd7da6462f84769e7))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.319.1 ([(46a0834)](https://gitlab.com/yellowhat-labs/utils/commit/46a0834bde3607c613567e447eac5145094fa225))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.321.0 ([(30fa53a)](https://gitlab.com/yellowhat-labs/utils/commit/30fa53a256c01071b03bf68ff75df6e6e592627c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.321.2 ([(e55ebc1)](https://gitlab.com/yellowhat-labs/utils/commit/e55ebc1d5a988be04a7376d4d6707484bd0c2b91))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.323.1 ([(0108f9e)](https://gitlab.com/yellowhat-labs/utils/commit/0108f9e07d900d5888370a10daaa7dd9cd5a4f82))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.356.1 ([(01ff62f)](https://gitlab.com/yellowhat-labs/utils/commit/01ff62f86a8d863021a0b4ead4293f6692b44ba6))
* **deps:** update node.js to c56ef51 ([(7e8712f)](https://gitlab.com/yellowhat-labs/utils/commit/7e8712f10ca9af52a3e66fd67c20bd1061e23155))
* **deps:** update node.js to fcf2fd3 ([(e7a1111)](https://gitlab.com/yellowhat-labs/utils/commit/e7a1111302a20bcb69a265f15b632afd511eaee7))
* **deps:** update pyfound/black docker tag to v24.4.1 ([(7b9c001)](https://gitlab.com/yellowhat-labs/utils/commit/7b9c001fba4c8c3caddce4716ea281f22b749e08))
* **deps:** update pyfound/black docker tag to v24.4.2 ([(6a42236)](https://gitlab.com/yellowhat-labs/utils/commit/6a422362f74377c679593d23f6178ab4c3d6a8c3))
* **deps:** update python:3.12.3 docker digest to 4574872 ([(407c972)](https://gitlab.com/yellowhat-labs/utils/commit/407c9725835827f009907d2a9434dd61217478a2))
* **deps:** update returntocorp/semgrep docker tag to v1.69 ([(ebd577f)](https://gitlab.com/yellowhat-labs/utils/commit/ebd577f39ad16a1f9e9ed665ae5e0950de34ab8e))
* **deps:** update returntocorp/semgrep docker tag to v1.70 ([(0521080)](https://gitlab.com/yellowhat-labs/utils/commit/052108056742495128286dad05dfab1c74dde3a6))
* **deps:** update returntocorp/semgrep docker tag to v1.72 ([(1c5ab38)](https://gitlab.com/yellowhat-labs/utils/commit/1c5ab38975fad0843ca3626f15f6c7159607fd2f))

## [1.6.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.6.2...v1.6.3) (2024-04-13)


### Bug Fixes

* run *nix job only on changes ([(cda06c4)](https://gitlab.com/yellowhat-labs/utils/commit/cda06c4b4156dab7a5661e30cd1b3c4c170ffd0c))


### Chore

* **deps:** update dependency crate-ci/typos to v1.20.8 ([(eb97d42)](https://gitlab.com/yellowhat-labs/utils/commit/eb97d42a2a61c184d46da8c19bbe9d991ac74066))
* **deps:** update dependency ruff to v0.3.7 ([(dd32e6c)](https://gitlab.com/yellowhat-labs/utils/commit/dd32e6c2c603e24586d1f5c4656cce1ffee7a3cd))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.287.2 ([(d9857d6)](https://gitlab.com/yellowhat-labs/utils/commit/d9857d6c76b2bb09555adecedea3617224c84bb9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.291.0 ([(b4280a3)](https://gitlab.com/yellowhat-labs/utils/commit/b4280a3a10edd0ccb672e7869b04e93a185b0899))
* **deps:** update node.js to v21.7.3 ([(08cec22)](https://gitlab.com/yellowhat-labs/utils/commit/08cec22cfd0037625152d7fd3ec7f649c120a408))
* **deps:** update pyfound/black docker tag to v24.4.0 ([(995395d)](https://gitlab.com/yellowhat-labs/utils/commit/995395de3a63f09fcb5182fd708802d2d839e6e4))

## [1.6.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.6.1...v1.6.2) (2024-04-12)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.8 ([(72c4229)](https://gitlab.com/yellowhat-labs/utils/commit/72c422970d530c499f580ad6ea9ab662ca75035f))


### Chore

* **deps:** update dependency crate-ci/typos to v1.20.4 ([(7a9695b)](https://gitlab.com/yellowhat-labs/utils/commit/7a9695bd4cf721764f018dcf1eef3513b0b4fd61))
* **deps:** update dependency crate-ci/typos to v1.20.7 ([(0224bb3)](https://gitlab.com/yellowhat-labs/utils/commit/0224bb35f3b017e6f49ac6d8b562c48fff4d883f))
* **deps:** update docker.io/nixos/nix docker tag to v2.21.2 ([(01472cc)](https://gitlab.com/yellowhat-labs/utils/commit/01472ccea8eddd06fada598f10e1ea145fdc72a6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.279.4 ([(5fd7a61)](https://gitlab.com/yellowhat-labs/utils/commit/5fd7a611d152b53816fe9d040ffd5325e7efdb96))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.280.0 ([(bfba412)](https://gitlab.com/yellowhat-labs/utils/commit/bfba41278617569a849a02cc4f4b186ce2b0ec63))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.282.1 ([(decebf4)](https://gitlab.com/yellowhat-labs/utils/commit/decebf4ae23b7e38e8b3d46aa408ec314941e786))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v8.2.1 ([(6a54921)](https://gitlab.com/yellowhat-labs/utils/commit/6a54921f891cacf5608de37c40c12739f6680ce9))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v8.2.3 ([(bf71250)](https://gitlab.com/yellowhat-labs/utils/commit/bf71250246a8afcd83af4772dd4eb41f0a15a68f))
* **deps:** update node.js to b55b64b ([(f2a1568)](https://gitlab.com/yellowhat-labs/utils/commit/f2a15687aadd8c3b10ad69760e185fdaada894a7))
* **deps:** update node.js to v21.7.2 ([(d39ccf6)](https://gitlab.com/yellowhat-labs/utils/commit/d39ccf6683c79b81da174cd0dc8c3128a7b55629))
* **deps:** update python docker tag to v3.12.3 ([(7b35d68)](https://gitlab.com/yellowhat-labs/utils/commit/7b35d6870e1f72266aa5dec02154a55002df7a23))
* **deps:** update returntocorp/semgrep docker tag to v1.68 ([(110d881)](https://gitlab.com/yellowhat-labs/utils/commit/110d8817c4bbe8641a25eaae4cdb006ebf9a9735))

## [1.6.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.6.0...v1.6.1) (2024-04-03)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.7 ([(c92c3da)](https://gitlab.com/yellowhat-labs/utils/commit/c92c3da913dc46bede051408586e79f898625b5d))


### Chore

* **deps:** update dependency @biomejs/biome to v1.6.4 ([(6e3df9b)](https://gitlab.com/yellowhat-labs/utils/commit/6e3df9b0130762a399aa743c36bdaf678865dedd))
* **deps:** update dependency crate-ci/typos to v1.20.1 ([(c7f4795)](https://gitlab.com/yellowhat-labs/utils/commit/c7f479526c8ce3e93d736601636e128f98c1eee4))
* **deps:** update dependency crate-ci/typos to v1.20.3 ([(38e590e)](https://gitlab.com/yellowhat-labs/utils/commit/38e590ee8b94afc27cb01301e016dc3a67acab2d))
* **deps:** update dependency ruff to v0.3.5 ([(1770221)](https://gitlab.com/yellowhat-labs/utils/commit/177022115c07c58a1af3ac9a84bab3923ac7a294))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.278.1 ([(0c6cc0b)](https://gitlab.com/yellowhat-labs/utils/commit/0c6cc0b611ad14cdadb342fcda22c724f1d56802))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.279.0 ([(96a2d05)](https://gitlab.com/yellowhat-labs/utils/commit/96a2d0557321a742a3c9a65339406d81631f65bd))
* **renovate:** fix ([(efb4646)](https://gitlab.com/yellowhat-labs/utils/commit/efb46463331566b36fd20d33a1ecb01f76ceb82a))

# [1.6.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.5.3...v1.6.0) (2024-03-31)


### Features

* **detect-secret:** delete ([(109a7f2)](https://gitlab.com/yellowhat-labs/utils/commit/109a7f2d50eeab4cb69458ee8e8530f5e2fc8449))


### Chore

* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.278.0 ([(37cd266)](https://gitlab.com/yellowhat-labs/utils/commit/37cd2663021241e33f3b4a318ed568f848b414e9))

## [1.5.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.5.2...v1.5.3) (2024-03-30)


### Bug Fixes

* **deps:** update alpine:edge docker digest to e31c3b1 ([(98fee20)](https://gitlab.com/yellowhat-labs/utils/commit/98fee20b1af9794de426ee0759a2a6040c4e2113))


### Chore

* **deps:** update dependency @biomejs/biome to v1.6.3 ([(ebf27c6)](https://gitlab.com/yellowhat-labs/utils/commit/ebf27c6023c35af8f6a4999e72961cc5198f5964))
* **deps:** update dependency pyupgrade to v3.15.2 ([(2ffe87b)](https://gitlab.com/yellowhat-labs/utils/commit/2ffe87b4d2e342c2ce167ff32f454814d4d4fddb))
* **deps:** update docker.io/nixos/nix docker tag to v2.21.1 ([(3aaff42)](https://gitlab.com/yellowhat-labs/utils/commit/3aaff42354af56b5ca8d3d7a77e484de833a2780))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.22.0 ([(8f335da)](https://gitlab.com/yellowhat-labs/utils/commit/8f335dad079a38c6f9081bcdb8b2a5530ea4dc89))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.270.0 ([(14e30da)](https://gitlab.com/yellowhat-labs/utils/commit/14e30daf3512d9b6b8d95ccc9c71693e486d4483))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.271.0 ([(d2fd01b)](https://gitlab.com/yellowhat-labs/utils/commit/d2fd01ba19502e8073ea1c3f669192173b003cca))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.272.0 ([(21c1317)](https://gitlab.com/yellowhat-labs/utils/commit/21c13171c20d2be3bd12ef3d4b3c39627512c7a9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.273.0 ([(62888b1)](https://gitlab.com/yellowhat-labs/utils/commit/62888b1c51f57dbb6c0a9345eb8ec037c0b2d0ad))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.274.0 ([(729b022)](https://gitlab.com/yellowhat-labs/utils/commit/729b02254587fa4bdd26058b35171c976ece77ad))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.277.0 ([(5a50e7d)](https://gitlab.com/yellowhat-labs/utils/commit/5a50e7decb1effb28327ece4620ff169403f051c))
* **deps:** update python:3.12.2 docker digest to 3e7c0f8 ([(c9126cf)](https://gitlab.com/yellowhat-labs/utils/commit/c9126cfb25489bc6e1812c0d41a144ba6d46a48b))
* **deps:** update python:3.12.2-alpine docker digest to c7eb5c9 ([(e643860)](https://gitlab.com/yellowhat-labs/utils/commit/e643860ba5f3992df9483e16580e3b747939dad4))
* **deps:** update returntocorp/semgrep docker tag to v1.67 ([(5c630f6)](https://gitlab.com/yellowhat-labs/utils/commit/5c630f66f0ff868e2329d46b07872bb7d9e5ec35))
* **deps:** update returntocorp/semgrep:1.66 docker digest to 72bd1c9 ([(e82eae3)](https://gitlab.com/yellowhat-labs/utils/commit/e82eae39850f03148c4b8741920da1ebca2ea353))
* **deps:** update returntocorp/semgrep:1.66 docker digest to ea0c171 ([(3d51d14)](https://gitlab.com/yellowhat-labs/utils/commit/3d51d14cc19cb6c343869d1ab7ebb75e16291b16))

## [1.5.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.5.1...v1.5.2) (2024-03-24)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.6 ([(4112b10)](https://gitlab.com/yellowhat-labs/utils/commit/4112b10ecff48a659b3ceb2305a64435169b18ff))


### Chore

* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.269.3 ([(3b5bb0d)](https://gitlab.com/yellowhat-labs/utils/commit/3b5bb0dea9b7507bfc946f555d8e95b612f2cc55))
* fix renovate-test ([(1657458)](https://gitlab.com/yellowhat-labs/utils/commit/16574584853e8552587512f89d46b9aabd14b207))

## [1.5.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.5.0...v1.5.1) (2024-03-24)


### Bug Fixes

* merge linter*.yaml as glob does not work include.project ([(adde12d)](https://gitlab.com/yellowhat-labs/utils/commit/adde12d4a9077e1d2df88ceed8731005c8ceaf06))


### Chore

* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.269.0 ([(992b03e)](https://gitlab.com/yellowhat-labs/utils/commit/992b03ee52561b35634d2f5bc6df3549778e22df))


### Continuous Integration

* fix renovate ([(6366fd7)](https://gitlab.com/yellowhat-labs/utils/commit/6366fd73daa76703de42ffb9061c3ba63df9a455))

# [1.5.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.21...v1.5.0) (2024-03-24)


### Features

* split linter and add nix ([(a0e159f)](https://gitlab.com/yellowhat-labs/utils/commit/a0e159f6b1b7f3fa3cdeaa444f534a1a3e2bfdb7))


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v13 ([(024ac93)](https://gitlab.com/yellowhat-labs/utils/commit/024ac939658b0641528c5e37554c8d92446706da))


### Chore

* **deps:** update dependency @biomejs/biome to v1.6.2 ([(6c92735)](https://gitlab.com/yellowhat-labs/utils/commit/6c927357c37bce88055b2340e906a5be0ae5784b))
* **deps:** update dependency ruff to v0.3.4 ([(3cfa578)](https://gitlab.com/yellowhat-labs/utils/commit/3cfa5788c94341f640cce8a856a8a435511a39e4))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.262.2 ([(70a3f3c)](https://gitlab.com/yellowhat-labs/utils/commit/70a3f3c3b9b894bacdb2fe5081037f8204d8c1d5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.263.0 ([(a6d668f)](https://gitlab.com/yellowhat-labs/utils/commit/a6d668ffc9e09425f94ea4863a92217c9948f96e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.265.0 ([(f668661)](https://gitlab.com/yellowhat-labs/utils/commit/f668661f3a383b586de4d1b2ce38e01a50bd6c7d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.266.0 ([(870fa97)](https://gitlab.com/yellowhat-labs/utils/commit/870fa974f01b89ae310704c2ffd96083505b2854))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.268.0 ([(8dd702c)](https://gitlab.com/yellowhat-labs/utils/commit/8dd702c4afcf42ace145ba29c32e549a6bf2d08b))
* fix renovate-test ([(32b5bed)](https://gitlab.com/yellowhat-labs/utils/commit/32b5bedf4374f8e644e82ede7618107f5684991f))

## [1.4.21](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.20...v1.4.21) (2024-03-20)


### Bug Fixes

* **linter:** add typos ([(b18d77a)](https://gitlab.com/yellowhat-labs/utils/commit/b18d77ac737616b95efc7a74c008120d167f3b57))


### Chore

* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.256.1 ([(a063c98)](https://gitlab.com/yellowhat-labs/utils/commit/a063c98238dd4c82bab3b8590c0012e74dc3fe8a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.256.2 ([(cf14e38)](https://gitlab.com/yellowhat-labs/utils/commit/cf14e38aeb2cb9e6706ac9866ea0c142e87c6995))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.261.0 ([(554f608)](https://gitlab.com/yellowhat-labs/utils/commit/554f6080cb5b07111e603d59dcb666ca39305423))
* **deps:** update returntocorp/semgrep docker tag to v1.66 ([(fb1d313)](https://gitlab.com/yellowhat-labs/utils/commit/fb1d313e8acaa59a96f67a155f5a3caa992bf90c))

## [1.4.20](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.19...v1.4.20) (2024-03-19)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.5 ([(e01d947)](https://gitlab.com/yellowhat-labs/utils/commit/e01d947dbdbb09737ded0dec5c91fdfc775de5ff))


### Chore

* **deps:** update dependency @semantic-release/commit-analyzer to v12 ([(f7f76f5)](https://gitlab.com/yellowhat-labs/utils/commit/f7f76f5fad4115cdc87850f76f5b1591646ff557))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.252.0 ([(e26e263)](https://gitlab.com/yellowhat-labs/utils/commit/e26e263f09f23ac7c0826b8b75506557e5a24558))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.253.1 ([(3fe7065)](https://gitlab.com/yellowhat-labs/utils/commit/3fe70654a3dcbd8900551d4c8991f09ed8fc3102))

## [1.4.19](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.18...v1.4.19) (2024-03-17)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.4 ([(123583b)](https://gitlab.com/yellowhat-labs/utils/commit/123583b97277812221f59773a85c65acfbf8a339))


### Chore

* **deps:** update alpine:edge docker digest to 08ad3c8 ([(3988b42)](https://gitlab.com/yellowhat-labs/utils/commit/3988b429a96524ef7c0fc81fda5f5675578b3f63))
* **deps:** update alpine:edge docker digest to 67b8a3c ([(e72667d)](https://gitlab.com/yellowhat-labs/utils/commit/e72667dbfc81ec72c452cc707fa7fc04b8c99243))
* **deps:** update dependency ruff to v0.3.3 ([(ca8c5ac)](https://gitlab.com/yellowhat-labs/utils/commit/ca8c5acf58d684010b41d55fb13502c9dc5a7fb8))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.249.0 ([(d20d967)](https://gitlab.com/yellowhat-labs/utils/commit/d20d967c50079ead6c744c279aecf0c72d9358ba))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.249.3 ([(d4736fe)](https://gitlab.com/yellowhat-labs/utils/commit/d4736fe9612b3f4c5eabb7d07a5bb49a78fa06bf))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.250.0 ([(2dd98d6)](https://gitlab.com/yellowhat-labs/utils/commit/2dd98d6b8c6016298ff346455a45cf6ad233b212))
* **deps:** update node.js to 58c5c8d ([(c065740)](https://gitlab.com/yellowhat-labs/utils/commit/c065740e032e17d8389a044066e1671504cd245a))
* **deps:** update pyfound/black docker tag to v24.3.0 ([(2cc9e0e)](https://gitlab.com/yellowhat-labs/utils/commit/2cc9e0e16f7a53eecff1994f94a5d2963b0d9b81))
* **deps:** update python:3.12.2-alpine docker digest to 25a82f6 ([(adb2681)](https://gitlab.com/yellowhat-labs/utils/commit/adb268105f627c0235f2c0ace182a616185a9533))
* **deps:** update python:3.12.2-alpine docker digest to fa2fd94 ([(d47dbdb)](https://gitlab.com/yellowhat-labs/utils/commit/d47dbdb920c7b75743a239a6d0d997021ff6edb1))

## [1.4.18](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.17...v1.4.18) (2024-3-15)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.3 ([(f3807c0)](https://gitlab.com/yellowhat-labs/utils/commit/f3807c03b5042547837ff03f91e35a331f630fcc))


### Chore

* **deps:** update dependency @biomejs/biome to v1.6.0 ([(adbf996)](https://gitlab.com/yellowhat-labs/utils/commit/adbf996e7b8d39a2093b0a740af283e7cd3201a7))
* **deps:** update dependency @biomejs/biome to v1.6.1 ([(d5f10ba)](https://gitlab.com/yellowhat-labs/utils/commit/d5f10ba21c026b720598b65579136461723a5150))
* **deps:** update dependency bandit to v1.7.8 ([(5aa2093)](https://gitlab.com/yellowhat-labs/utils/commit/5aa209342c34fddc5f14b660194b4b2d76713555))
* **deps:** update dependency mypy to v1.9.0 ([(b54e4e0)](https://gitlab.com/yellowhat-labs/utils/commit/b54e4e0bd60e6a4f8c760e850e8c3d9a938fcee6))
* **deps:** update dependency pylint to v3.1.0 ([(c573c55)](https://gitlab.com/yellowhat-labs/utils/commit/c573c55b8962781930ce9b3c067b54ba9e75afbc))
* **deps:** update dependency ruff to v0.3.0 ([(5d6cfc2)](https://gitlab.com/yellowhat-labs/utils/commit/5d6cfc2978e7039855f5911bd8d558fdca92998f))
* **deps:** update dependency ruff to v0.3.1 ([(cf28b7f)](https://gitlab.com/yellowhat-labs/utils/commit/cf28b7f8770de3da4d9b5189896915458a788a26))
* **deps:** update dependency ruff to v0.3.2 ([(1f2f91f)](https://gitlab.com/yellowhat-labs/utils/commit/1f2f91f3080a94971e90fca3f7feb820aefaa3d2))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.21.0 ([(1fd487d)](https://gitlab.com/yellowhat-labs/utils/commit/1fd487d778ee854f4dce139e566c1bfb7bf5b9e0))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.21.1 ([(9ecaa49)](https://gitlab.com/yellowhat-labs/utils/commit/9ecaa492f1d852ec97416b71b3c6ac42d30835f7))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.214.0 ([(746bfe2)](https://gitlab.com/yellowhat-labs/utils/commit/746bfe2914a21e7e7bed08878e6988e180054db8))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.214.2 ([(46ec01a)](https://gitlab.com/yellowhat-labs/utils/commit/46ec01a7600a38f4d374c17f470fdf74ea5a0396))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.219.0 ([(6be9c2d)](https://gitlab.com/yellowhat-labs/utils/commit/6be9c2d0d555d2d04fd4e78d5320d64693423134))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.219.4 ([(53dacc7)](https://gitlab.com/yellowhat-labs/utils/commit/53dacc7ed3424ccd5d88ba37f8312fe60d280218))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.220.0 ([(964507d)](https://gitlab.com/yellowhat-labs/utils/commit/964507da8fe8179bdf45342c3cf4c7394f702349))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.220.2 ([(b9ffce9)](https://gitlab.com/yellowhat-labs/utils/commit/b9ffce9734b9cd975e814fb7aa361b5a4eb4fda0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.220.3 ([(61754ab)](https://gitlab.com/yellowhat-labs/utils/commit/61754ab93053f5c0c715430b77fe78b429f95a42))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.221.0 ([(1cc46b5)](https://gitlab.com/yellowhat-labs/utils/commit/1cc46b52dd95f69b49bd4336d8aa9a6687ab0fc9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.221.1 ([(b901820)](https://gitlab.com/yellowhat-labs/utils/commit/b90182046a3b3baa18244561b3890bf3883138f4))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.224.2 ([(22af799)](https://gitlab.com/yellowhat-labs/utils/commit/22af799bae29cf6235bf39d23aa7740fb2249b7a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.226.0 ([(b51d7b7)](https://gitlab.com/yellowhat-labs/utils/commit/b51d7b7ff3cf9726528f3aad2133ced830b2ac78))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.226.1 ([(b4329cc)](https://gitlab.com/yellowhat-labs/utils/commit/b4329cc4acce5843a999463b620a7d88e67f0c3a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.226.3 ([(f26af8d)](https://gitlab.com/yellowhat-labs/utils/commit/f26af8d2e5fc6e6d9c8a60769318f100322f3d8e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.227.1 ([(695f95d)](https://gitlab.com/yellowhat-labs/utils/commit/695f95d2f4f098eb0255fc476e10e11debd996cb))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.229.1 ([(8b12221)](https://gitlab.com/yellowhat-labs/utils/commit/8b12221e371e936d664baa8e61e5aff9ca295f33))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.229.3 ([(c12942b)](https://gitlab.com/yellowhat-labs/utils/commit/c12942b54c6b2633876471180f3a4d0e68b14caf))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.229.5 ([(a02cbd5)](https://gitlab.com/yellowhat-labs/utils/commit/a02cbd5d6acfb0575927a795872344569d6528d9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.230.0 ([(8bc5c08)](https://gitlab.com/yellowhat-labs/utils/commit/8bc5c082cee72f84aca939ac30cc2633f78614d5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.231.2 ([(45b0033)](https://gitlab.com/yellowhat-labs/utils/commit/45b0033dcfa1e92f484ac2ea78f53bc4d8e7578d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.232.0 ([(e6698e6)](https://gitlab.com/yellowhat-labs/utils/commit/e6698e6a7f17c15569cf18e3a3a949510d0a6713))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.233.1 ([(0efb38c)](https://gitlab.com/yellowhat-labs/utils/commit/0efb38c24c0a8793f89906aa2bf2a3eb1531b39a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.234.1 ([(3ab56ac)](https://gitlab.com/yellowhat-labs/utils/commit/3ab56acdf0f3f47a7d240fce7382ca59ec96b416))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.235.1 ([(44f6021)](https://gitlab.com/yellowhat-labs/utils/commit/44f602151381a66b50d17a018045b015326d5b07))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.235.4 ([(770b22c)](https://gitlab.com/yellowhat-labs/utils/commit/770b22cdc59a9d848bdfb8b0c1eff73ac36d8a1c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.238.0 ([(7e6ed77)](https://gitlab.com/yellowhat-labs/utils/commit/7e6ed7712d9ae2ac0489bc160e064efdfe4abc8a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.240.1 ([(57f9509)](https://gitlab.com/yellowhat-labs/utils/commit/57f95090cc02719a9fddcd7746b99cd68d3a8bcb))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.241.0 ([(5aeeb18)](https://gitlab.com/yellowhat-labs/utils/commit/5aeeb18b9a5a7588307d08eac65c39759f4e49e2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.241.1 ([(2032d10)](https://gitlab.com/yellowhat-labs/utils/commit/2032d1075059ad767f5262958059804b4eef6954))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.246.1 ([(317a5b3)](https://gitlab.com/yellowhat-labs/utils/commit/317a5b386f4641e80d55ea4a0691bc15ddbe0b59))
* **deps:** update ghcr.io/tcort/markdown-link-check docker tag to v3.12.0 ([(2bbef1a)](https://gitlab.com/yellowhat-labs/utils/commit/2bbef1a76a0994e5bda94583f4fb063702168629))
* **deps:** update ghcr.io/tcort/markdown-link-check docker tag to v3.12.1 ([(666661e)](https://gitlab.com/yellowhat-labs/utils/commit/666661e7b68915ca4b89a1395fc9fcdca54c5c7e))
* **deps:** update koalaman/shellcheck-alpine docker tag to v0.10.0 ([(ac7df55)](https://gitlab.com/yellowhat-labs/utils/commit/ac7df55b671836f4ecd4b3e0e655678814f4918a))
* **deps:** update node.js to ca75f97 ([(bb36c35)](https://gitlab.com/yellowhat-labs/utils/commit/bb36c354c353c00308ad0c8667d5167534815bc6))
* **deps:** update node.js to v21.7.0 ([(3e36939)](https://gitlab.com/yellowhat-labs/utils/commit/3e369394b2015ddd2a4898117ac0697413d0dfe9))
* **deps:** update node.js to v21.7.1 ([(e9324b1)](https://gitlab.com/yellowhat-labs/utils/commit/e9324b134e9a5ef6f79f4fb208f62931bd1ecd45))
* **deps:** update python:3.12.2 docker digest to 91b4916 ([(db16729)](https://gitlab.com/yellowhat-labs/utils/commit/db167298d925b31cb544089a3c8787f5e7d618b5))
* **deps:** update returntocorp/semgrep docker tag to v1.63 ([(3fc237d)](https://gitlab.com/yellowhat-labs/utils/commit/3fc237dd11d3ce60596e4698834a38befbfcb3e8))
* **deps:** update returntocorp/semgrep docker tag to v1.64 ([(70f25bf)](https://gitlab.com/yellowhat-labs/utils/commit/70f25bf81e035b8d4c0dc8a740dc2a738a5ed9f4))
* **deps:** update returntocorp/semgrep docker tag to v1.65 ([(4477258)](https://gitlab.com/yellowhat-labs/utils/commit/4477258bcb9e2ed20f07527231ace381463eed6f))


### renovate

* add htpc ([(7040864)](https://gitlab.com/yellowhat-labs/utils/commit/70408642a8ab76ed210d956f87779698efa37ed9))

## [1.4.17](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.16...v1.4.17) (2024-2-25)


### Bug Fixes

* **ruff:** show report in gitlab ([(39c8634)](https://gitlab.com/yellowhat-labs/utils/commit/39c8634acd986079ea7a4b764d43ceb9d5cab8e1))


### Chore

* **deps:** update dependency pylint to v3.0.4 ([(cfac9bf)](https://gitlab.com/yellowhat-labs/utils/commit/cfac9bff739dd4f04aa10f021ac00f3620cc763c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.213.0 ([(a954ec7)](https://gitlab.com/yellowhat-labs/utils/commit/a954ec70501104db21eb30c2e6a771715bbd5e43))

## [1.4.16](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.15...v1.4.16) (2024-2-23)


### Bug Fixes

* allow renovate_test to fail ([(3f99a8e)](https://gitlab.com/yellowhat-labs/utils/commit/3f99a8e877d7d2b44777bb4dba99a498e93cc609))


### Chore

* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.206.1 ([(4911b94)](https://gitlab.com/yellowhat-labs/utils/commit/4911b9490360688a971fa712364b9b6444e0bfdf))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.208.1 ([(efaf99c)](https://gitlab.com/yellowhat-labs/utils/commit/efaf99c6dd0b74f8eca924541e5ad9e9a40f3d2d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.210.0 ([(ab23743)](https://gitlab.com/yellowhat-labs/utils/commit/ab237434766af66875dfd8f518c523e8a1443ce7))
* **deps:** update returntocorp/semgrep docker tag to v1.62 ([(4c817d6)](https://gitlab.com/yellowhat-labs/utils/commit/4c817d61d8e7bda6cb2d0ba134aed53fdbea8272))

## [1.4.15](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.14...v1.4.15) (2024-2-21)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.0.3 ([(be0dbd7)](https://gitlab.com/yellowhat-labs/utils/commit/be0dbd7391a995ec26158c2c06e33904c9957eba))


### Chore

* **deps:** update dependency pyupgrade to v3.15.1 ([(1d973cf)](https://gitlab.com/yellowhat-labs/utils/commit/1d973cfc14b4b2bcd12ad3d2a4d1dd93dffa03e9))
* **deps:** update dependency ruff to v0.2.2 ([(bbff16f)](https://gitlab.com/yellowhat-labs/utils/commit/bbff16f9ffd6758e5d4ed523854d33400b6d1605))
* **deps:** update dependency yamllint to v1.35.0 ([(e5e2dcd)](https://gitlab.com/yellowhat-labs/utils/commit/e5e2dcd98a4e921b6958097b7309213d7325afec))
* **deps:** update dependency yamllint to v1.35.1 ([(02b5100)](https://gitlab.com/yellowhat-labs/utils/commit/02b5100f04c71410754adbbba34ef0718ae12987))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.20.1 ([(944ae3c)](https://gitlab.com/yellowhat-labs/utils/commit/944ae3caeb45afbd98528705494666b1a3f1368e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.175.3 ([(105afe4)](https://gitlab.com/yellowhat-labs/utils/commit/105afe4eefae02a3ac6d111824f6d7517edfa9a5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.176.0 ([(256d47d)](https://gitlab.com/yellowhat-labs/utils/commit/256d47d76763dee44b70af83aac5aa5db9d2135e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.180.0 ([(66eb592)](https://gitlab.com/yellowhat-labs/utils/commit/66eb592b352d75fdf800b32c64dcbc4e5402f6d2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.182.0 ([(265cc8e)](https://gitlab.com/yellowhat-labs/utils/commit/265cc8ec751944e6c1f85ef91cbe6149d45ddb6a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.182.3 ([(2090a5e)](https://gitlab.com/yellowhat-labs/utils/commit/2090a5e7087ff11cdb92b4644bc7a8bb966c85ab))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.186.1 ([(78fca09)](https://gitlab.com/yellowhat-labs/utils/commit/78fca097cb3f9a6110e2ee75c5d524987b5610af))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.187.2 ([(7d65455)](https://gitlab.com/yellowhat-labs/utils/commit/7d65455f6432402731d6a5d2f7cc1023adb9359c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.189.0 ([(a7693c5)](https://gitlab.com/yellowhat-labs/utils/commit/a7693c51c368e13a5b52c1896c0fc518599b3639))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.191.1 ([(d27c75c)](https://gitlab.com/yellowhat-labs/utils/commit/d27c75cc887fb3073f4b0e0bfcf2fad12e8ed9a9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.192.1 ([(48fe9f6)](https://gitlab.com/yellowhat-labs/utils/commit/48fe9f6b4564003dfdf5585e91a076e7a706bb51))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.194.0 ([(dbefe38)](https://gitlab.com/yellowhat-labs/utils/commit/dbefe382fc817c2607b6a752bafaa7967e0eea41))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.194.1 ([(454a560)](https://gitlab.com/yellowhat-labs/utils/commit/454a560299c5bb4d704ec7fa9f6e8d216eb7a87e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.194.5 ([(a1c7246)](https://gitlab.com/yellowhat-labs/utils/commit/a1c72464b7bc14c42e4a5ca8dc19e64ee2e69b39))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.197.0 ([(21c59b4)](https://gitlab.com/yellowhat-labs/utils/commit/21c59b4d45af7721e439ac9cf5b3c70135530c1c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.198.0 ([(a9f41fa)](https://gitlab.com/yellowhat-labs/utils/commit/a9f41fac275e520b22e8ba12e2a0b5a608eb1610))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.198.3 ([(3fc1634)](https://gitlab.com/yellowhat-labs/utils/commit/3fc1634a8f1505eb437b10cc0a54c189e0ae42b1))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.202.1 ([(0e13067)](https://gitlab.com/yellowhat-labs/utils/commit/0e130671e09261b9755d0ec750ba85e172720eb7))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.203.2 ([(529b04c)](https://gitlab.com/yellowhat-labs/utils/commit/529b04c7ae2a573ec112b0f8c4683a8e068f2689))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v8.1.2 ([(b784106)](https://gitlab.com/yellowhat-labs/utils/commit/b784106216cce52bcd40ef0ec95f13eb1776eb17))
* **deps:** update mvdan/shfmt docker tag to v3.8.0 ([(bc6a6f1)](https://gitlab.com/yellowhat-labs/utils/commit/bc6a6f1b1ead6a44bdb83f990236302179b9db34))
* **deps:** update node.js to 90eb022 ([(00dd7f2)](https://gitlab.com/yellowhat-labs/utils/commit/00dd7f26687c3a205785ed48f6d761fe7bd38e0e))
* **deps:** update node.js to v21.6.2 ([(8029c95)](https://gitlab.com/yellowhat-labs/utils/commit/8029c955a254790a6b3e6243eedd40c198e84d75))
* **deps:** update pyfound/black docker tag to v24.2.0 ([(9504014)](https://gitlab.com/yellowhat-labs/utils/commit/95040144ab3a0b5dd9bca1dd4b12748cbc2b1525))
* **deps:** update python docker tag to v3.12.2 ([(ca2153e)](https://gitlab.com/yellowhat-labs/utils/commit/ca2153e3d4eb300712a75d15aa7c8d48bc106527))
* **deps:** update python:3.12.2 docker digest to 35eff34 ([(716c516)](https://gitlab.com/yellowhat-labs/utils/commit/716c516e52f9ea486d33d1d100cdeb5d4575d39d))
* **deps:** update returntocorp/semgrep docker tag to v1.60 ([(61e425a)](https://gitlab.com/yellowhat-labs/utils/commit/61e425a3b7acb41b19c9b6601722fefd39cd6cb6))
* **deps:** update returntocorp/semgrep docker tag to v1.61 ([(240884b)](https://gitlab.com/yellowhat-labs/utils/commit/240884bd98fe9a5af98841501d9265dede4610dc))
* **deps:** update returntocorp/semgrep:1.61 docker digest to 83533e3 ([(2b68d88)](https://gitlab.com/yellowhat-labs/utils/commit/2b68d887ea133254d441d75b61726cc9a3a6d9cd))

## [1.4.14](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.13...v1.4.14) (2024-2-7)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.2 ([(d291315)](https://gitlab.com/yellowhat-labs/utils/commit/d291315c2d4b7f261bd4bbe7871a5ed27fd8b043))


### Chore

* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.20.0 ([(e0cbec8)](https://gitlab.com/yellowhat-labs/utils/commit/e0cbec8e1aa317882301650dcd7e368b2d2fc7a3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.175.0 ([(6d7bc81)](https://gitlab.com/yellowhat-labs/utils/commit/6d7bc8194337c080e2b006d0e5d36e9209408a0c))

## [1.4.13](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.12...v1.4.13) (2024-2-7)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.1 ([(0a7a770)](https://gitlab.com/yellowhat-labs/utils/commit/0a7a77041026b94b5c79d522f92ae4f7e727a03e))


### Chore

* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.174.7 ([(3720f8e)](https://gitlab.com/yellowhat-labs/utils/commit/3720f8e8f3a77a8db4290c8562b56dcf1828c3cb))

## [1.4.12](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.11...v1.4.12) (2024-2-6)


### Bug Fixes

* yamllint ([(9925a76)](https://gitlab.com/yellowhat-labs/utils/commit/9925a76df1aa9f296c432cadd13d579e427e4568))


### Chore

* Configure Renovate ([(cb5be41)](https://gitlab.com/yellowhat-labs/utils/commit/cb5be410526532fdc6849d6fafbe860a848be04b))
* **deps:** update dependency @biomejs/biome to v1.5.2 ([(f5cbf0f)](https://gitlab.com/yellowhat-labs/utils/commit/f5cbf0fdac1e1216c26caebe0583ae7867af21fc))
* **deps:** update dependency @biomejs/biome to v1.5.3 ([(1b44b3f)](https://gitlab.com/yellowhat-labs/utils/commit/1b44b3f7639cd0bee91872ac75ca5f728e91d0ff))
* **deps:** update dependency bandit to v1.7.7 ([(cd4265a)](https://gitlab.com/yellowhat-labs/utils/commit/cd4265ae4389a164fe5a9390e45d6c0b847d3d51))
* **deps:** update dependency ruff to v0.1.14 ([(bd187de)](https://gitlab.com/yellowhat-labs/utils/commit/bd187def6a7746e601790adfc4b1fb7885867ffa))
* **deps:** update dependency ruff to v0.1.15 ([(f0cfbd3)](https://gitlab.com/yellowhat-labs/utils/commit/f0cfbd35ae504b9360ce73ff9a1c1d9c5011b264))
* **deps:** update dependency ruff to v0.2.0 ([(1fd2562)](https://gitlab.com/yellowhat-labs/utils/commit/1fd25622b68e9033c53cf9ce3b8fe45633335b2c))
* **deps:** update dependency ruff to v0.2.1 ([(9e695d5)](https://gitlab.com/yellowhat-labs/utils/commit/9e695d5fb2a6bb0efa76c8e7518fe5b27e2a8ee4))
* **deps:** update dependency vulture to v2.11 ([(11b9a9b)](https://gitlab.com/yellowhat-labs/utils/commit/11b9a9bdc3f821610477049e515fa12b27fa206e))
* **deps:** update dependency yamllint to v1.34.0 ([(4b8f93c)](https://gitlab.com/yellowhat-labs/utils/commit/4b8f93cb28112fb34ef6f7939cf9466a63cfc459))
* **deps:** update ghcr.io/gitleaks/gitleaks docker tag to v8.18.2 ([(fbcddf3)](https://gitlab.com/yellowhat-labs/utils/commit/fbcddf373b85bd375d7d3f7696b28abc8065c4b5))
* **deps:** update ghcr.io/igorshubovych/markdownlint-cli docker tag to v0.39.0 ([(2c0ca3d)](https://gitlab.com/yellowhat-labs/utils/commit/2c0ca3d3f55c4aa8102de63d942f90b5d84c8a3d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.134.0 ([(f114619)](https://gitlab.com/yellowhat-labs/utils/commit/f114619b1d6a228f39d6d97bae113a4293bf85f6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.139.1 ([(58bcaf9)](https://gitlab.com/yellowhat-labs/utils/commit/58bcaf967350adc9a47eac3b1e02aff7d7f1b189))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.140.0 ([(b8b657d)](https://gitlab.com/yellowhat-labs/utils/commit/b8b657d79398239ac3768183f6b62a02986fc9f8))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.140.14 ([(bef3bf8)](https://gitlab.com/yellowhat-labs/utils/commit/bef3bf879863453bd36b17abd6605f2d82740fbf))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.140.6 ([(1cefd3c)](https://gitlab.com/yellowhat-labs/utils/commit/1cefd3c1100809aec51eecd2fcb75c2c8942751e))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.141.0 ([(3f7a9cb)](https://gitlab.com/yellowhat-labs/utils/commit/3f7a9cbf1f6f0da19f8428cb089f4b0d9fcfc7dd))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.146.0 ([(f39471c)](https://gitlab.com/yellowhat-labs/utils/commit/f39471cfa1399647aec82558f88ce8593fcd8af1))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.147.0 ([(1fabc6f)](https://gitlab.com/yellowhat-labs/utils/commit/1fabc6fc2079d1496169367465138e29e5bb3c39))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.152.0 ([(bcabd41)](https://gitlab.com/yellowhat-labs/utils/commit/bcabd411ae33422cef0920582d0173ce89b04cfa))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.153.1 ([(dbf8c2a)](https://gitlab.com/yellowhat-labs/utils/commit/dbf8c2a12c1e6d46bb10364c8e67bafe9d818ba1))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.153.2 ([(77a5640)](https://gitlab.com/yellowhat-labs/utils/commit/77a5640b8662604b62d412a4c2f1b95cb93b057f))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.165.0 ([(cd8b7f0)](https://gitlab.com/yellowhat-labs/utils/commit/cd8b7f0c7aad7b7937ee6277696f5e7d8f04769a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.168.1 ([(26e19bf)](https://gitlab.com/yellowhat-labs/utils/commit/26e19bf738f9a540dfc3160573f3d3a1f2ee881d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.168.3 ([(9250d8b)](https://gitlab.com/yellowhat-labs/utils/commit/9250d8b876ae98c26a968cd43df1a8bc6cc42083))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.168.4 ([(5db6755)](https://gitlab.com/yellowhat-labs/utils/commit/5db6755b686375b647f98555edfe17ca8e99fd9b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.173.0 ([(106b16c)](https://gitlab.com/yellowhat-labs/utils/commit/106b16c93f588a0c836686e468691fba98ed131d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.173.3 ([(cda35b7)](https://gitlab.com/yellowhat-labs/utils/commit/cda35b7343e7bc53b88e36dcdb32a868d2f70c1a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.174.0 ([(55f66d0)](https://gitlab.com/yellowhat-labs/utils/commit/55f66d0295fb2d1c119da36ac419a5683fde64c1))
* **deps:** update ghcr.io/renovatebot/renovate:37.139.3 docker digest to b64e4b9 ([(884f103)](https://gitlab.com/yellowhat-labs/utils/commit/884f1036e3cb1d54c9324388bfbfc5452faeb0b4))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v8.1.1 ([(57a6ce0)](https://gitlab.com/yellowhat-labs/utils/commit/57a6ce0d275c9619222a5be6e3b93d304f6afffa))
* **deps:** update node.js to a675a14 ([(7559b0a)](https://gitlab.com/yellowhat-labs/utils/commit/7559b0adf6f5dd71ed81d331ce5e5901ff4e03c4))
* **deps:** update node.js to v21.6.0 ([(43381ae)](https://gitlab.com/yellowhat-labs/utils/commit/43381ae2c55c15a8584285870254118d2a0b54f4))
* **deps:** update node.js to v21.6.1 ([(1de5428)](https://gitlab.com/yellowhat-labs/utils/commit/1de54284d49a6cc1c1a62e89489574132cec2254))
* **deps:** update pyfound/black docker tag to v24 ([(0f85879)](https://gitlab.com/yellowhat-labs/utils/commit/0f85879611864b85867c5c54e851671b5a53d00e))
* **deps:** update pyfound/black docker tag to v24.1.1 ([(26111c2)](https://gitlab.com/yellowhat-labs/utils/commit/26111c28bd86a16b26e2ab8f730706eb8f3ec25c))
* **deps:** update python:3.12.1 docker digest to 374e74d ([(a2ce718)](https://gitlab.com/yellowhat-labs/utils/commit/a2ce718748caf8d68e1a1f0eb362696090e105b6))
* **deps:** update python:3.12.1 docker digest to 7df3602 ([(cbd786d)](https://gitlab.com/yellowhat-labs/utils/commit/cbd786d55461654585413a274ed18020f356f1c8))
* **deps:** update python:3.12.1 docker digest to 97a22f3 ([(18abb39)](https://gitlab.com/yellowhat-labs/utils/commit/18abb3926481e4c5288b88fbafe1d30693c90705))
* **deps:** update python:3.12.1-alpine docker digest to 14cfc61 ([(4f44ee7)](https://gitlab.com/yellowhat-labs/utils/commit/4f44ee70c5a01573321cd1563aac834143bcc5ba))
* **deps:** update python:3.12.1-alpine docker digest to 4a156f7 ([(e4d3847)](https://gitlab.com/yellowhat-labs/utils/commit/e4d384741d0e6f7ae569f143750c20266ae91ca2))
* **deps:** update python:3.12.1-alpine docker digest to 801b54e ([(c5e485b)](https://gitlab.com/yellowhat-labs/utils/commit/c5e485be4fffeea9a4994ff906af3019d97c9763))
* **deps:** update python:3.12.1-alpine docker digest to a30c98e ([(d190d58)](https://gitlab.com/yellowhat-labs/utils/commit/d190d58a64cde3f950030b184874d2bb284e42e9))
* **deps:** update returntocorp/semgrep docker tag to v1.57 ([(f44ce25)](https://gitlab.com/yellowhat-labs/utils/commit/f44ce25d22296df4ad1d1c1770b6dd57a3241346))
* **deps:** update returntocorp/semgrep docker tag to v1.58 ([(8feffeb)](https://gitlab.com/yellowhat-labs/utils/commit/8feffeb19ca5eb0daf9cc55363bfed9375691919))
* **deps:** update returntocorp/semgrep docker tag to v1.59 ([(078cf10)](https://gitlab.com/yellowhat-labs/utils/commit/078cf10b33a69279ef3b710ea68fdf2d8a6676f1))
* **deps:** update returntocorp/semgrep:1.59 docker digest to 5d880fa ([(d717546)](https://gitlab.com/yellowhat-labs/utils/commit/d71754662e42a9c315acb5d50847784fa04adc6f))


### Continuous Integration

* biome ([(97ea036)](https://gitlab.com/yellowhat-labs/utils/commit/97ea0369296a66d0812f092dfee16ef84e5043fe))
* fix renovate ([(077dc9e)](https://gitlab.com/yellowhat-labs/utils/commit/077dc9edd194e02f4795343cd62ed755d9370396))
* fix renovate ([(47a72fd)](https://gitlab.com/yellowhat-labs/utils/commit/47a72fd8350dacab2c83778e99eb726fcdb9c25c))

## [1.4.11](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.10...v1.4.11) (2024-1-14)


### Bug Fixes

* **deps:** update dependency semantic-release to v23 ([(47212e7)](https://gitlab.com/yellowhat-labs/utils/commit/47212e7cc13e2ca843a871fbe45ff9331a6b9dbb))


### Chore

* **deps:** update dependency ruff to v0.1.12 ([(d9bd606)](https://gitlab.com/yellowhat-labs/utils/commit/d9bd606bb516d0bff937c519ed1ed576a7ab3527))
* **deps:** update dependency ruff to v0.1.13 ([(47b9c1a)](https://gitlab.com/yellowhat-labs/utils/commit/47b9c1a6abeba4c1638ad3f5c98374aa3a0691a1))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.128.2 ([(d039188)](https://gitlab.com/yellowhat-labs/utils/commit/d039188cbb4d99c7caa3f3518969f930a08faeae))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.128.4 ([(9902c25)](https://gitlab.com/yellowhat-labs/utils/commit/9902c250ab2e4b590fb1acef91b1971dc317a295))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.129.1 ([(f460171)](https://gitlab.com/yellowhat-labs/utils/commit/f4601717117e56abfc9a3b54ba71b21d1b08fce0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.130.0 ([(cc525e3)](https://gitlab.com/yellowhat-labs/utils/commit/cc525e3046607922f050acc13ce259435b8568a6))
* **deps:** update node.js to 1f164f0 ([(29d0a38)](https://gitlab.com/yellowhat-labs/utils/commit/29d0a388cb41414175ee1ddc1d9c8195bfe02b05))
* **deps:** update python:3.12.1 docker digest to 452d6ae ([(0653c83)](https://gitlab.com/yellowhat-labs/utils/commit/0653c8388a4ca003a5962e2a4736b4ef05eeb8f7))
* **deps:** update returntocorp/semgrep docker tag to v1.56 ([(d1f1c4d)](https://gitlab.com/yellowhat-labs/utils/commit/d1f1c4d3537e1fdecd6f2c78dfd35bb38986e1cd))


### Continuous Integration

* fix renovate ([(dc54a98)](https://gitlab.com/yellowhat-labs/utils/commit/dc54a98205b75ed21115184a1fba5f7c92c2f241))
* install flak8 from pip ([(2e7b23e)](https://gitlab.com/yellowhat-labs/utils/commit/2e7b23e6f70d1590aacaa4bde9c7c729cdbd8ac0))
* **renovate:** pip name can container numbers ([(8ff74e5)](https://gitlab.com/yellowhat-labs/utils/commit/8ff74e5e7974934b2089956038157f74abef8dc9))

## [1.4.10](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.9...v1.4.10) (2024-1-10)


### Bug Fixes

* **ruff:** assume python 3.12 ([(4c02262)](https://gitlab.com/yellowhat-labs/utils/commit/4c0226293eb8e5f2e70a2ef15b3815640158bab8))


### Chore

* **deps:** update dependency @biomejs/biome to v1.5.1 ([(9e62496)](https://gitlab.com/yellowhat-labs/utils/commit/9e62496b3616c53df02224e82bcd111c00c67f2c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.127.0 ([(aabfafe)](https://gitlab.com/yellowhat-labs/utils/commit/aabfafef87c77430832bf82a38f91e64336e607b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.128.0 ([(23f2d96)](https://gitlab.com/yellowhat-labs/utils/commit/23f2d968975268c1661df07f31bbd14fc215d509))

## [1.4.9](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.8...v1.4.9) (2024-1-8)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.0.2 ([(588790e)](https://gitlab.com/yellowhat-labs/utils/commit/588790e3cc6d8de5a78306dbb4a6634916515e91))


### Chore

* **deps:** update dependency @biomejs/biome to v1.5.0 ([(6307f70)](https://gitlab.com/yellowhat-labs/utils/commit/6307f7038abb2089b5f7dacf14b3f51079bc2765))
* **deps:** update dependency pyflakes to v3.2.0 ([(e8574d6)](https://gitlab.com/yellowhat-labs/utils/commit/e8574d6bf099a58ff90baffd1826c40aabfdb9b3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.125.1 ([(5f9c256)](https://gitlab.com/yellowhat-labs/utils/commit/5f9c256b97583968708ff72bbd46ef59b8e8433d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.126.1 ([(d9d2e99)](https://gitlab.com/yellowhat-labs/utils/commit/d9d2e99552ba513b96deace197bc7efe131b3ed2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.126.3 ([(b829ff7)](https://gitlab.com/yellowhat-labs/utils/commit/b829ff798fdac223af3093273c0e905ac84eefc3))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.126.4 ([(b822e62)](https://gitlab.com/yellowhat-labs/utils/commit/b822e62cd9e82e981db8265b1ebfec5e557c5643))
* **deps:** update returntocorp/semgrep:1.55 docker digest to 8333215 ([(fe7f0f7)](https://gitlab.com/yellowhat-labs/utils/commit/fe7f0f703ade5db8bccff4ef808771e84ea6c73e))
* **deps:** update returntocorp/semgrep:1.55 docker digest to 8efd177 ([(d025d92)](https://gitlab.com/yellowhat-labs/utils/commit/d025d9268f14daaae1d909bf9b054aca9bff1607))

## [1.4.8](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.7...v1.4.8) (2024-1-5)


### Bug Fixes

* **renovate:** also manage go ([(cb248a8)](https://gitlab.com/yellowhat-labs/utils/commit/cb248a8afe1b6a093b6758b8cf0fa1cacc2bf220))


### Chore

* **deps:** update dependency ruff to v0.1.10 ([(057daa2)](https://gitlab.com/yellowhat-labs/utils/commit/057daa2fc02a141b00f215875ebf5e2b6d2b54a1))
* **deps:** update dependency ruff to v0.1.11 ([(03a3cc7)](https://gitlab.com/yellowhat-labs/utils/commit/03a3cc7f467f1cdb56bf2ee4b21d8502d9c1757b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.116.0 ([(749b38c)](https://gitlab.com/yellowhat-labs/utils/commit/749b38cdf6baf5787517402ef553ef929f96406a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.118.0 ([(43062c9)](https://gitlab.com/yellowhat-labs/utils/commit/43062c9a4608e118dc356c593e1a9167b75c4b1c))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.121.0 ([(801c5d4)](https://gitlab.com/yellowhat-labs/utils/commit/801c5d4c28f0483d79b4012c6f5d6a89fde86455))
* **deps:** update ghcr.io/renovatebot/renovate:37.115.0 docker digest to 61cebfd ([(bb26c20)](https://gitlab.com/yellowhat-labs/utils/commit/bb26c205c08df9a29806e827ea526b6cd79ef9c7))
* **deps:** update returntocorp/semgrep docker tag to v1.55 ([(e732cf9)](https://gitlab.com/yellowhat-labs/utils/commit/e732cf91d2846e6a3b3b3973025fe95717f100de))

## [1.4.7](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.6...v1.4.7) (2023-12-31)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13 ([(b20cd0c)](https://gitlab.com/yellowhat-labs/utils/commit/b20cd0c0f0da59fa97d769422ab537ecfab4df2b))


### Chore

* **deps:** update alpine:edge docker digest to 9f867dc ([(a70df9f)](https://gitlab.com/yellowhat-labs/utils/commit/a70df9f139c10576795d08f9f2d0d6f75e94fe10))
* **deps:** update dependency mypy to v1.8.0 ([(df28ace)](https://gitlab.com/yellowhat-labs/utils/commit/df28ace6421a3c3ae8be56c12e2a90b298b891e0))
* **deps:** update dependency ruff to v0.1.8 ([(d6cf8a7)](https://gitlab.com/yellowhat-labs/utils/commit/d6cf8a718aa30d87b16653fd3b3f3b9839247f9b))
* **deps:** update dependency ruff to v0.1.9 ([(3645a22)](https://gitlab.com/yellowhat-labs/utils/commit/3645a220b4872a40e8993860d93b21acb46d779b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.115.0 ([(ae46228)](https://gitlab.com/yellowhat-labs/utils/commit/ae462286e4c5054c4fc76754766f4635001884b0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.92.4 ([(15d30a5)](https://gitlab.com/yellowhat-labs/utils/commit/15d30a54e0aa05fa790bf43ffe75bfdeb7484458))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.93.1 ([(f26d58a)](https://gitlab.com/yellowhat-labs/utils/commit/f26d58a72c5b88ef3b0a2a7927646217470d60ff))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v8.1.0 ([(f486935)](https://gitlab.com/yellowhat-labs/utils/commit/f486935254b1888b09acb55736c95d93e383d394))
* **deps:** update node.js to v21.5.0 ([(cb4c188)](https://gitlab.com/yellowhat-labs/utils/commit/cb4c1887a668f07aab3dc0df46472fb1df9393bf))
* **deps:** update pyfound/black docker tag to v23.12.1 ([(bea3a3e)](https://gitlab.com/yellowhat-labs/utils/commit/bea3a3ebf5b3ac29f74d28c8b9d9f5193e02bf03))
* **deps:** update python:3.12.1 docker digest to 553d2d5 ([(73749fd)](https://gitlab.com/yellowhat-labs/utils/commit/73749fdd26fc1810f5937c18808e0b584db2c0e1))
* **deps:** update returntocorp/semgrep docker tag to v1.53 ([(838bcb0)](https://gitlab.com/yellowhat-labs/utils/commit/838bcb0123b963b3aab655351fbd21b26a39d781))
* **deps:** update returntocorp/semgrep docker tag to v1.54 ([(2f4a1b9)](https://gitlab.com/yellowhat-labs/utils/commit/2f4a1b9909a26e5b09056a3981bd12b9794beb91))

## [1.4.6](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.5...v1.4.6) (2023-12-12)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.12 ([(b5f1f67)](https://gitlab.com/yellowhat-labs/utils/commit/b5f1f6703c9de28402ac856b524a8c7d43dd1d22))
* renovate ([(1c55018)](https://gitlab.com/yellowhat-labs/utils/commit/1c55018811ef2dd801f42f5b68f33d64131d4b43))
* semantic-release use alpine 3.19 ([(e882960)](https://gitlab.com/yellowhat-labs/utils/commit/e882960cedbdb8ebf171f79a734379f705353044))


### Chore

* **deps:** update dependency pylint to v3.0.3 ([(e7b62ef)](https://gitlab.com/yellowhat-labs/utils/commit/e7b62ef8f3d8ed0aa53c558c0bba912378610231))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.89.7 ([(c0a8c39)](https://gitlab.com/yellowhat-labs/utils/commit/c0a8c39ccba460de533d0bd6be5b7e0265ca0719))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.91.3 ([(09407a1)](https://gitlab.com/yellowhat-labs/utils/commit/09407a13ffa4e314e16efc8014b198bb34b30a4b))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.91.4 ([(f45afc1)](https://gitlab.com/yellowhat-labs/utils/commit/f45afc1f34acb3253167ff88b8408ba67db897ff))
* **deps:** update node.js to 1654370 ([(9deee95)](https://gitlab.com/yellowhat-labs/utils/commit/9deee958679c7331db736cba59dd35160ba21970))
* **deps:** update pyfound/black docker tag to v23.12.0 ([(7e32908)](https://gitlab.com/yellowhat-labs/utils/commit/7e3290843441d7737fbfd9b1d9aeefecbd0463b3))


* ci(blacken-docs: move from alpine to slim ([(4b6aba2)](https://gitlab.com/yellowhat-labs/utils/commit/4b6aba2a181f4c6b7167ce6697a96afb77609d23))

## [1.4.5](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.4...v1.4.5) (2023-12-10)


### Chore

* **deps:** update dependency bandit to v1.7.6 ([(84bd16f)](https://gitlab.com/yellowhat-labs/utils/commit/84bd16f6c6d2f00b65702826f8555fb52a421446))
* **deps:** update ghcr.io/igorshubovych/markdownlint-cli docker tag to v0.38.0 ([(d48771d)](https://gitlab.com/yellowhat-labs/utils/commit/d48771d0e55719eeddaf4dc7d1f5fe71fd2ca6e6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.89.2 ([(b8d43a4)](https://gitlab.com/yellowhat-labs/utils/commit/b8d43a4dd714e04be4c59ec49e1b85d31c99a227))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.89.5 ([(e1dca93)](https://gitlab.com/yellowhat-labs/utils/commit/e1dca93da546b84b78765732ef504721aaa28fe7))
* **deps:** update python docker tag to v3.12.1 ([(865f49b)](https://gitlab.com/yellowhat-labs/utils/commit/865f49b84bf81b8b8d3d3ac3de177d0100d228e8))
* **release:** 1.4.5 ([(1ea73d0)](https://gitlab.com/yellowhat-labs/utils/commit/1ea73d091300a1690193bb9a46b027e23641fa2f))


### Bug Fixes

* run only on MR and main ([(8b4aac2)](https://gitlab.com/yellowhat-labs/utils/commit/8b4aac292b385254c81ce1028464139fc2460d0a))

## [1.4.5](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.4...v1.4.5) (2023-12-10)


### Bug Fixes

* run only on MR and main ([(8b4aac2)](https://gitlab.com/yellowhat-labs/utils/commit/8b4aac292b385254c81ce1028464139fc2460d0a))


### Chore

* **deps:** update dependency bandit to v1.7.6 ([(84bd16f)](https://gitlab.com/yellowhat-labs/utils/commit/84bd16f6c6d2f00b65702826f8555fb52a421446))
* **deps:** update ghcr.io/igorshubovych/markdownlint-cli docker tag to v0.38.0 ([(d48771d)](https://gitlab.com/yellowhat-labs/utils/commit/d48771d0e55719eeddaf4dc7d1f5fe71fd2ca6e6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.89.2 ([(b8d43a4)](https://gitlab.com/yellowhat-labs/utils/commit/b8d43a4dd714e04be4c59ec49e1b85d31c99a227))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.89.5 ([(e1dca93)](https://gitlab.com/yellowhat-labs/utils/commit/e1dca93da546b84b78765732ef504721aaa28fe7))
* **deps:** update python docker tag to v3.12.1 ([(865f49b)](https://gitlab.com/yellowhat-labs/utils/commit/865f49b84bf81b8b8d3d3ac3de177d0100d228e8))

## [1.4.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.3...v1.4.4) (2023-12-6)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.10 ([(0c707a6)](https://gitlab.com/yellowhat-labs/utils/commit/0c707a6dbe61bf8eb370fd87904ba57d9d9c52a2))
* **deps:** update dependency semantic-release to v22.0.9 ([(8e698fa)](https://gitlab.com/yellowhat-labs/utils/commit/8e698fad43908de600a35acdaaa7bdd43cb3335b))
* semantic-release ([(b59b615)](https://gitlab.com/yellowhat-labs/utils/commit/b59b6150ba73e82dce7571cc5dccc8dce29641de))
* **semantic-release:** allow bring-your-own config ([(e51f600)](https://gitlab.com/yellowhat-labs/utils/commit/e51f60009274886ac69dec21d792e117f0ec4b73))
* **shellcheck:** do not rely on shfmt ([(b525490)](https://gitlab.com/yellowhat-labs/utils/commit/b525490d42fa4a67f5df34bb48172e45bafb3a73))


### Chore

* **deps:** update dependency @biomejs/biome to v1.4.0 ([(f25d861)](https://gitlab.com/yellowhat-labs/utils/commit/f25d8614d60710cdf93d8d48239c9eb949a6ae8b))
* **deps:** update dependency @biomejs/biome to v1.4.1 ([(e4fdc18)](https://gitlab.com/yellowhat-labs/utils/commit/e4fdc18733596b7ef9f6ec13307bca3b6b0ad65d))
* **deps:** update dependency markdown-table-formatter to v1.5.0 ([(fbe75b0)](https://gitlab.com/yellowhat-labs/utils/commit/fbe75b0f71ccfab6cd017f7a9ef3f2eceb305c44))
* **deps:** update dependency mypy to v1.7.1 ([(fe5c1b3)](https://gitlab.com/yellowhat-labs/utils/commit/fe5c1b392ef9c648bf802b0cc11d33bc0eae02a9))
* **deps:** update dependency ruff to v0.1.7 ([(fd09e05)](https://gitlab.com/yellowhat-labs/utils/commit/fd09e05ac2d5495cc383fcef6cdb26c33bd96cc6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.61.4 ([(66399e7)](https://gitlab.com/yellowhat-labs/utils/commit/66399e7ac7ef8d897d444f532128a637bcdd3552))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.66.0 ([(7aa77d7)](https://gitlab.com/yellowhat-labs/utils/commit/7aa77d719e168a2405bbb0c43b25926c06755dbe))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.68.3 ([(0158c8a)](https://gitlab.com/yellowhat-labs/utils/commit/0158c8ac0dc4f7a14c583f88a74f206acf9b3df6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.71.1 ([(4fc2d50)](https://gitlab.com/yellowhat-labs/utils/commit/4fc2d50903a96ba052890c22289241bbb387a48d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.74.3 ([(b58ed11)](https://gitlab.com/yellowhat-labs/utils/commit/b58ed11d20c299e104e1b954f83755fc108e7cab))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.77.1 ([(b7a4bad)](https://gitlab.com/yellowhat-labs/utils/commit/b7a4bad04663f2c0fc588aa42beffbb232c6af8a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.81.0 ([(5182232)](https://gitlab.com/yellowhat-labs/utils/commit/5182232b2bf4e52dfdd08858ce8b428bb32e6ab2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.81.4 ([(c65366f)](https://gitlab.com/yellowhat-labs/utils/commit/c65366fd59d2cc0e28a93e8e91a85e10da9b0249))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.83.0 ([(33882fe)](https://gitlab.com/yellowhat-labs/utils/commit/33882fee1a106a98cf4cfc0851a3a841e0a37ce5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.83.5 ([(e5bd780)](https://gitlab.com/yellowhat-labs/utils/commit/e5bd78036f52650caedc889cc239c69b91479eb0))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.88.1 ([(1741682)](https://gitlab.com/yellowhat-labs/utils/commit/1741682523bdcb1ae0e228f9c5d3fef5551583d7))
* **deps:** update ghcr.io/renovatebot/renovate:37.66.0 docker digest to 308440a ([(9ad1ba6)](https://gitlab.com/yellowhat-labs/utils/commit/9ad1ba67a9f6aede000ab9700162f18690f692b0))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v7.2.0 ([(027de1d)](https://gitlab.com/yellowhat-labs/utils/commit/027de1d5ea445822e122094c54b6fbe49160de85))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v8 ([(815866b)](https://gitlab.com/yellowhat-labs/utils/commit/815866bc9527d6be12a23f760c4c54f7c7db434c))
* **deps:** update node.js to 06facfb ([(aec79a0)](https://gitlab.com/yellowhat-labs/utils/commit/aec79a04725c6d2d4244f47be673a13d51ced473))
* **deps:** update node.js to c242927 ([(56eee19)](https://gitlab.com/yellowhat-labs/utils/commit/56eee19a21971888d775a4c0d0f5a9ca8c603f6a))
* **deps:** update node.js to v21.3.0 ([(3c047a7)](https://gitlab.com/yellowhat-labs/utils/commit/3c047a7d171c2aa86bf1a90d09aa30f9d0ccf372))
* **deps:** update node.js to v21.4.0 ([(5e965d4)](https://gitlab.com/yellowhat-labs/utils/commit/5e965d49e46fd5df8d3c5092e497d89b23bbb8df))
* **deps:** update python:3.12.0-alpine docker digest to 09f18c1 ([(2777b3b)](https://gitlab.com/yellowhat-labs/utils/commit/2777b3bbbbe37f512b003887553a8b57d89188c1))
* **deps:** update python:3.12.0-alpine docker digest to 1d688a3 ([(3c7575d)](https://gitlab.com/yellowhat-labs/utils/commit/3c7575dae84f7030d2227a9dd88d4e5d20edb5f6))
* **deps:** update python:3.12.0-alpine docker digest to c5bbde5 ([(6d19feb)](https://gitlab.com/yellowhat-labs/utils/commit/6d19feba4eae30fbacab1a6e4ed253fb577cbb78))
* **deps:** update python:3.12.0-slim docker digest to 19a6235 ([(5931397)](https://gitlab.com/yellowhat-labs/utils/commit/59313978ab9b45714928aa8857515939b3f5fdaa))
* **deps:** update python:3.12.0-slim docker digest to 32477c7 ([(7777841)](https://gitlab.com/yellowhat-labs/utils/commit/7777841b2cc2413e453c3fbb93e6efaef76ec02e))
* **deps:** update python:3.12.0-slim docker digest to eb6d320 ([(b7c3654)](https://gitlab.com/yellowhat-labs/utils/commit/b7c3654ab05de4b578df691806450622ddbf70ca))
* **deps:** update returntocorp/semgrep docker tag to v1.51 ([(63096f7)](https://gitlab.com/yellowhat-labs/utils/commit/63096f7c8219d5f59ae504948977e615aa54fe46))
* **deps:** update returntocorp/semgrep docker tag to v1.52 ([(d77e519)](https://gitlab.com/yellowhat-labs/utils/commit/d77e519c7b103f9e85796b19dfc3037315a3a5a0))
* **release:** 1.4.4 ([(51080b7)](https://gitlab.com/yellowhat-labs/utils/commit/51080b74ba3a3d381a3b4c75063c072db2fb1a79))
* **release:** 1.4.5 ([(a3cf68c)](https://gitlab.com/yellowhat-labs/utils/commit/a3cf68c23ccbca00757d82c360e205610bb2d9ec))
* **release:** 1.4.6 ([(19550f6)](https://gitlab.com/yellowhat-labs/utils/commit/19550f60ddd4b9952363e1de9e29bb4912a6bf97))
* **release:** 1.4.7 ([(32601f4)](https://gitlab.com/yellowhat-labs/utils/commit/32601f428d2c43dbcfe69e7f1b0807d527e26719))
* **renovate_test:** indent current json on print ([(5425507)](https://gitlab.com/yellowhat-labs/utils/commit/54255078ffc9bc022dacd06fbef742e8d728b1b8))

## [1.4.7](https://gitlab.com/yellowhat-labs/utils/compare/1.4.6...1.4.7) (2023-12-6)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.10 ([(0c707a6)](https://gitlab.com/yellowhat-labs/utils/commit/0c707a6dbe61bf8eb370fd87904ba57d9d9c52a2))


### Chore

* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.88.1 ([(1741682)](https://gitlab.com/yellowhat-labs/utils/commit/1741682523bdcb1ae0e228f9c5d3fef5551583d7))
* **deps:** update node.js to v21.4.0 ([(5e965d4)](https://gitlab.com/yellowhat-labs/utils/commit/5e965d49e46fd5df8d3c5092e497d89b23bbb8df))
* **deps:** update returntocorp/semgrep docker tag to v1.52 ([(d77e519)](https://gitlab.com/yellowhat-labs/utils/commit/d77e519c7b103f9e85796b19dfc3037315a3a5a0))

## [1.4.6](https://gitlab.com/yellowhat-labs/utils/compare/1.4.5...1.4.6) (2023-12-5)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.9 ([(8e698fa)](https://gitlab.com/yellowhat-labs/utils/commit/8e698fad43908de600a35acdaaa7bdd43cb3335b))


### Chore

* **deps:** update dependency ruff to v0.1.7 ([(fd09e05)](https://gitlab.com/yellowhat-labs/utils/commit/fd09e05ac2d5495cc383fcef6cdb26c33bd96cc6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.83.0 ([(33882fe)](https://gitlab.com/yellowhat-labs/utils/commit/33882fee1a106a98cf4cfc0851a3a841e0a37ce5))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.83.5 ([(e5bd780)](https://gitlab.com/yellowhat-labs/utils/commit/e5bd78036f52650caedc889cc239c69b91479eb0))

## [1.4.5](https://gitlab.com/yellowhat-labs/utils/compare/1.4.4...1.4.5) (2023-12-04)


### Bug Fixes

* **shellcheck:** do not rely on shfmt ([(b525490)](https://gitlab.com/yellowhat-labs/utils/commit/b525490d42fa4a67f5df34bb48172e45bafb3a73))


### Chore

* **deps:** update dependency @biomejs/biome to v1.4.1 ([(e4fdc18)](https://gitlab.com/yellowhat-labs/utils/commit/e4fdc18733596b7ef9f6ec13307bca3b6b0ad65d))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.74.3 ([(b58ed11)](https://gitlab.com/yellowhat-labs/utils/commit/b58ed11d20c299e104e1b954f83755fc108e7cab))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.77.1 ([(b7a4bad)](https://gitlab.com/yellowhat-labs/utils/commit/b7a4bad04663f2c0fc588aa42beffbb232c6af8a))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.81.0 ([(5182232)](https://gitlab.com/yellowhat-labs/utils/commit/5182232b2bf4e52dfdd08858ce8b428bb32e6ab2))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.81.4 ([(c65366f)](https://gitlab.com/yellowhat-labs/utils/commit/c65366fd59d2cc0e28a93e8e91a85e10da9b0249))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v8 ([(815866b)](https://gitlab.com/yellowhat-labs/utils/commit/815866bc9527d6be12a23f760c4c54f7c7db434c))
* **deps:** update node.js to c242927 ([(56eee19)](https://gitlab.com/yellowhat-labs/utils/commit/56eee19a21971888d775a4c0d0f5a9ca8c603f6a))
* **deps:** update node.js to v21.3.0 ([(3c047a7)](https://gitlab.com/yellowhat-labs/utils/commit/3c047a7d171c2aa86bf1a90d09aa30f9d0ccf372))
* **deps:** update python:3.12.0-alpine docker digest to 09f18c1 ([(2777b3b)](https://gitlab.com/yellowhat-labs/utils/commit/2777b3bbbbe37f512b003887553a8b57d89188c1))
* **deps:** update python:3.12.0-alpine docker digest to 1d688a3 ([(3c7575d)](https://gitlab.com/yellowhat-labs/utils/commit/3c7575dae84f7030d2227a9dd88d4e5d20edb5f6))
* **deps:** update python:3.12.0-alpine docker digest to c5bbde5 ([(6d19feb)](https://gitlab.com/yellowhat-labs/utils/commit/6d19feba4eae30fbacab1a6e4ed253fb577cbb78))
* **deps:** update python:3.12.0-slim docker digest to 19a6235 ([(5931397)](https://gitlab.com/yellowhat-labs/utils/commit/59313978ab9b45714928aa8857515939b3f5fdaa))
* **deps:** update python:3.12.0-slim docker digest to eb6d320 ([(b7c3654)](https://gitlab.com/yellowhat-labs/utils/commit/b7c3654ab05de4b578df691806450622ddbf70ca))
* **deps:** update returntocorp/semgrep docker tag to v1.51 ([(63096f7)](https://gitlab.com/yellowhat-labs/utils/commit/63096f7c8219d5f59ae504948977e615aa54fe46))

## [1.4.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.3...1.4.4) (2023-11-29)


### Bug Fixes

* **semantic-release:** allow bring-your-own config ([(e51f600)](https://gitlab.com/yellowhat-labs/utils/commit/e51f60009274886ac69dec21d792e117f0ec4b73))


### Chore

* **deps:** update dependency @biomejs/biome to v1.4.0 ([(f25d861)](https://gitlab.com/yellowhat-labs/utils/commit/f25d8614d60710cdf93d8d48239c9eb949a6ae8b))
* **deps:** update dependency markdown-table-formatter to v1.5.0 ([(fbe75b0)](https://gitlab.com/yellowhat-labs/utils/commit/fbe75b0f71ccfab6cd017f7a9ef3f2eceb305c44))
* **deps:** update dependency mypy to v1.7.1 ([(fe5c1b3)](https://gitlab.com/yellowhat-labs/utils/commit/fe5c1b392ef9c648bf802b0cc11d33bc0eae02a9))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.61.4 ([(66399e7)](https://gitlab.com/yellowhat-labs/utils/commit/66399e7ac7ef8d897d444f532128a637bcdd3552))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.66.0 ([(7aa77d7)](https://gitlab.com/yellowhat-labs/utils/commit/7aa77d719e168a2405bbb0c43b25926c06755dbe))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.68.3 ([(0158c8a)](https://gitlab.com/yellowhat-labs/utils/commit/0158c8ac0dc4f7a14c583f88a74f206acf9b3df6))
* **deps:** update ghcr.io/renovatebot/renovate docker tag to v37.71.1 ([(4fc2d50)](https://gitlab.com/yellowhat-labs/utils/commit/4fc2d50903a96ba052890c22289241bbb387a48d))
* **deps:** update ghcr.io/renovatebot/renovate:37.66.0 docker digest to 308440a ([(9ad1ba6)](https://gitlab.com/yellowhat-labs/utils/commit/9ad1ba67a9f6aede000ab9700162f18690f692b0))
* **deps:** update ghcr.io/secretlint/secretlint docker tag to v7.2.0 ([(027de1d)](https://gitlab.com/yellowhat-labs/utils/commit/027de1d5ea445822e122094c54b6fbe49160de85))
* **deps:** update node.js to 06facfb ([(aec79a0)](https://gitlab.com/yellowhat-labs/utils/commit/aec79a04725c6d2d4244f47be673a13d51ced473))
* **deps:** update python:3.12.0-slim docker digest to 32477c7 ([(7777841)](https://gitlab.com/yellowhat-labs/utils/commit/7777841b2cc2413e453c3fbb93e6efaef76ec02e))
* **renovate_test:** indent current json on print ([(5425507)](https://gitlab.com/yellowhat-labs/utils/commit/54255078ffc9bc022dacd06fbef742e8d728b1b8))

## [1.4.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.2...v1.4.3) (2023-11-19)


### Bug Fixes

* **linter:** use black defaults ([b54900b](https://gitlab.com/yellowhat-labs/utils/commit/b54900ba2970635e3edc1251a6648884a9219a6d))

## [1.4.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.1...v1.4.2) (2023-11-19)


### Bug Fixes

* **semantic-release:** remove default entrypoint and explicitly call it ([a7feeed](https://gitlab.com/yellowhat-labs/utils/commit/a7feeed94949290599f92ba74282d7769dddfff9))

## [1.4.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.4.0...v1.4.1) (2023-11-19)


### Bug Fixes

* **deps:** update semantic-release monorepo ([81e2138](https://gitlab.com/yellowhat-labs/utils/commit/81e2138212a8f86880db0bcbf48f202830e3a363))

# [1.4.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.20...v1.4.0) (2023-11-15)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.1.0 ([671599d](https://gitlab.com/yellowhat-labs/utils/commit/671599d89223ce30b2a17a206caba56f0b9b2ef2))


### Features

* **semantic-release:** ship config file in container ([c4efe1a](https://gitlab.com/yellowhat-labs/utils/commit/c4efe1a3a5e7212df99014d111d2c6831e4520c4))
* **semantic-release:** ship config file in container ([9a92a88](https://gitlab.com/yellowhat-labs/utils/commit/9a92a88db08ab096f2a63f3e6419fac3fa3b8efe))
* **semantic-release:** ship config file in container ([270954a](https://gitlab.com/yellowhat-labs/utils/commit/270954abd8dbc4e8d769e37e6ac59c5a7c4f5d0d))

## [1.3.20](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.19...v1.3.20) (2023-11-12)


### Bug Fixes

* **biome:** indent-size is deprecated ([a8bfb10](https://gitlab.com/yellowhat-labs/utils/commit/a8bfb10e6aa93d78dfbe0b994b5d3eb9e68a6e0c))

## [1.3.19](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.18...v1.3.19) (2023-11-06)


### Bug Fixes

* **deps:** update semantic-release monorepo ([ac7637e](https://gitlab.com/yellowhat-labs/utils/commit/ac7637e9dfeb5a706de02d9c0a79e295ae28f92b))

## [1.3.18](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.17...v1.3.18) (2023-11-05)


### Bug Fixes

* **deps:** update semantic-release monorepo ([6bc50b4](https://gitlab.com/yellowhat-labs/utils/commit/6bc50b48d984eb5d4169d590d76ab2ef56771cd3))

## [1.3.17](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.16...v1.3.17) (2023-09-25)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.5 ([45b48f1](https://gitlab.com/yellowhat-labs/utils/commit/45b48f1981de6a0d25f029a9d0aadb63a4c74c5b))

## [1.3.16](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.15...v1.3.16) (2023-09-23)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.4 ([a3fc0e1](https://gitlab.com/yellowhat-labs/utils/commit/a3fc0e1f7fb9aea9ae45d3920d47f65e5ab27d5c))

## [1.3.15](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.14...v1.3.15) (2023-09-22)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.1 ([ac050ce](https://gitlab.com/yellowhat-labs/utils/commit/ac050ce26ebb4c74546482bf8d22dec43d20f400))

## [1.3.14](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.13...v1.3.14) (2023-09-18)


### Bug Fixes

* **deps:** update semantic-release monorepo (major) ([4c1b102](https://gitlab.com/yellowhat-labs/utils/commit/4c1b102f483b2fc7e1ddd2954cd75b02ac1c0c8e))

## [1.3.13](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.12...v1.3.13) (2023-09-17)


### Bug Fixes

* **deps:** update dependency semantic-release to v22 ([adae1bf](https://gitlab.com/yellowhat-labs/utils/commit/adae1bf18ffbbf4d4fbf0df51604f52386bd7861))
* **deps:** update semantic-release monorepo ([126424a](https://gitlab.com/yellowhat-labs/utils/commit/126424a8d05852906c15edff318710e695cfb1da))

## [1.3.12](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.11...v1.3.12) (2023-09-07)


### Bug Fixes

* **renovate:** add missing item ([95c2c0b](https://gitlab.com/yellowhat-labs/utils/commit/95c2c0b99fe2c894dc1dc1f82d01473dbe6855ee))

## [1.3.11](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.10...v1.3.11) (2023-09-01)


### Bug Fixes

* **deps:** update semantic-release monorepo ([8601e27](https://gitlab.com/yellowhat-labs/utils/commit/8601e275d3340406d77525d94e6893e6a2b56734))

## [1.3.10](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.9...v1.3.10) (2023-08-21)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.9 ([1f768fc](https://gitlab.com/yellowhat-labs/utils/commit/1f768fc4a808658f9728e23d436ce31cb8c6a124))

## [1.3.9](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.8...v1.3.9) (2023-08-11)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.0.5 ([9bb9279](https://gitlab.com/yellowhat-labs/utils/commit/9bb927924c035795320ad2e847a5f87ef6946e51))

## [1.3.8](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.7...v1.3.8) (2023-08-05)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.0.4 ([59d8c46](https://gitlab.com/yellowhat-labs/utils/commit/59d8c46707b732c781a834e19e367bcaf6572a27))

## [1.3.7](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.6...v1.3.7) (2023-07-05)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.7 ([df76957](https://gitlab.com/yellowhat-labs/utils/commit/df7695727c9919d98169a3cb302684e2be53bc5e))

## [1.3.6](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.5...v1.3.6) (2023-07-04)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v11.0.4 ([1d40b38](https://gitlab.com/yellowhat-labs/utils/commit/1d40b381e1178b9c6f7c670a974a65ace8d52a1a))

## [1.3.5](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.4...v1.3.5) (2023-07-04)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.6 ([8b31c2d](https://gitlab.com/yellowhat-labs/utils/commit/8b31c2d50b125ba056a1ca1f2cd7bd7811e2f766))
* **deps:** update dependency semantic-release to v21.0.6 ([2e21dd4](https://gitlab.com/yellowhat-labs/utils/commit/2e21dd4448df1cd4667d96039a32c3f5beb31016))

## [1.3.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.3...v1.3.4) (2023-06-10)


### Bug Fixes

* **deps:** update semantic-release monorepo ([50b67c4](https://gitlab.com/yellowhat-labs/utils/commit/50b67c4394fa46b7f06ef211e525fb2523e653c1))

## [1.3.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.2...v1.3.3) (2023-06-07)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v11.0.3 ([5145469](https://gitlab.com/yellowhat-labs/utils/commit/514546930304ea92bbb7783cc342c842b4dd8979))

## [1.3.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.1...v1.3.2) (2023-06-07)


### Bug Fixes

* **deps:** update dependency @semantic-release/commit-analyzer to v10.0.1 ([b3e6a90](https://gitlab.com/yellowhat-labs/utils/commit/b3e6a90b7b48871231313493535f4af3e36fda38))

## [1.3.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.3.0...v1.3.1) (2023-06-03)


### Bug Fixes

* **deps:** update dependency @semantic-release/commit-analyzer to v10 ([d96d3b7](https://gitlab.com/yellowhat-labs/utils/commit/d96d3b732b5603831d6ab3519c85c65582c8206c))
* **deps:** update semantic-release monorepo ([611729d](https://gitlab.com/yellowhat-labs/utils/commit/611729dc599e09ac6ec0479a9af0712e968b2db5))

# [1.3.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.2.4...v1.3.0) (2023-05-29)


### Features

* **linter:** add rome ([e879358](https://gitlab.com/yellowhat-labs/utils/commit/e879358d41438da117b2f28e3b611cfa90dd51b8))

## [1.2.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.2.3...v1.2.4) (2023-05-29)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v11.0.2 ([5e82480](https://gitlab.com/yellowhat-labs/utils/commit/5e82480f7a2273adf53dfd3b3789492168478e74))

## [1.2.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.2.2...v1.2.3) (2023-04-30)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v11 ([6e37a27](https://gitlab.com/yellowhat-labs/utils/commit/6e37a27b0dd65c8c902bca2262c736e7f195a45b))

## [1.2.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.2.1...v1.2.2) (2023-04-30)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.2 ([de9ca1b](https://gitlab.com/yellowhat-labs/utils/commit/de9ca1b3023a5e2367c0827141f607c39f54896e))

## [1.2.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.2.0...v1.2.1) (2023-04-28)


### Bug Fixes

* remove unused variables ([ce23897](https://gitlab.com/yellowhat-labs/utils/commit/ce238974b686520fc34f83a3caa41e642d901161))

# [1.2.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.1.5...v1.2.0) (2023-04-26)


### Features

* add secretlint and markdown-lint-check ([64014da](https://gitlab.com/yellowhat-labs/utils/commit/64014da3b1e14ab4d7eff653b18b68e3efb671fe))

# [1.2.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.1.5...v1.2.0) (2023-04-26)


### Features

* add secretlint and markdown-lint-check ([64014da](https://gitlab.com/yellowhat-labs/utils/commit/64014da3b1e14ab4d7eff653b18b68e3efb671fe))

## [1.1.5](https://gitlab.com/yellowhat-labs/utils/compare/v1.1.4...v1.1.5) (2023-04-01)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.1 ([ffb8b15](https://gitlab.com/yellowhat-labs/utils/commit/ffb8b15db7f148a8b71c0719ce98f5a3e421cbd2))

## [1.1.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.1.3...v1.1.4) (2023-03-26)


### Bug Fixes

* **deps:** update dependency semantic-release to v21 ([0fb5a22](https://gitlab.com/yellowhat-labs/utils/commit/0fb5a22a19f5bdb88dc8c31174b90e177d14c105))

## [1.1.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.1.2...v1.1.3) (2023-03-24)


### Bug Fixes

* **deps:** update all dependencies ([714c5cb](https://gitlab.com/yellowhat-labs/utils/commit/714c5cb9296c9882dfbe98f2ccac6ba30a039925))

## [1.1.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.1.1...v1.1.2) (2023-03-08)


### Bug Fixes

* update deps ([7ad6663](https://gitlab.com/yellowhat-labs/utils/commit/7ad66630e81845820500977deb5f7ebfd926c873))

## [1.1.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.1.0...v1.1.1) (2023-02-17)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v11 ([cd320a1](https://gitlab.com/yellowhat-labs/utils/commit/cd320a10a3cd15ae8e9701764012cd82e56a9f9a))

# [1.1.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.10...v1.1.0) (2023-02-12)


### Features

* add renovate-test.py ([dd3e5de](https://gitlab.com/yellowhat-labs/utils/commit/dd3e5ded9336b37ffbf4883849320c6cbbb91dcf))

# [1.1.0](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.10...v1.1.0) (2023-02-12)


### Features

* add renovate-test.py ([dd3e5de](https://gitlab.com/yellowhat-labs/utils/commit/dd3e5ded9336b37ffbf4883849320c6cbbb91dcf))

## [1.0.10](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.9...v1.0.10) (2023-02-11)


### Bug Fixes

* ruff, semgrep, renovate ([62a2207](https://gitlab.com/yellowhat-labs/utils/commit/62a2207119f2f41a803ebd6a6e90722f45f76373))

## [1.0.9](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.8...v1.0.9) (2023-02-09)


### Bug Fixes

* ruff, python 3.11.2, semgrep 1.10, renovate ([4b84cc6](https://gitlab.com/yellowhat-labs/utils/commit/4b84cc6e84884225c0a8a7e6295266d21adcc66f))

## [1.0.8](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.7...v1.0.8) (2023-02-07)


### Bug Fixes

* **linter:** use slim instead of alpine for mypy ([5bb3ff2](https://gitlab.com/yellowhat-labs/utils/commit/5bb3ff2d823c9005455210d1427913d7ec5782e1))

## [1.0.7](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.6...v1.0.7) (2023-02-07)


### Bug Fixes

* add statusbar repo ([b82fc4b](https://gitlab.com/yellowhat-labs/utils/commit/b82fc4bd77b85c2afccc2fdfcd3b440046e047fe))

## [1.0.6](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.5...v1.0.6) (2023-02-06)

## [1.0.5](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.4...v1.0.5) (2023-02-05)


### Bug Fixes

* semantic-release lock file ([d7bef1b](https://gitlab.com/yellowhat-labs/utils/commit/d7bef1b3faf2ebc9eb1e5712d5aa435d987a3f08))
* **deps:** update dependency @semantic-release/gitlab to v10.1.4 ([620a345](https://gitlab.com/yellowhat-labs/utils/commit/620a345751ae361be07e0b01b8789a283b38bac5))
* **deps:** update dependency @semantic-release/gitlab to v10.1.4 ([20f273b](https://gitlab.com/yellowhat-labs/utils/commit/20f273b0b86c5d08f60a4843af1b6821eddf5398))

## [1.0.4](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.3...v1.0.4) (2023-02-02)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v10.1.3 ([f31cea0](https://gitlab.com/yellowhat-labs/utils/commit/f31cea0500ceb276d46da3644a6ee651ab026860))
* **deps:** update dependency @semantic-release/gitlab to v10.1.3 ([dfc205a](https://gitlab.com/yellowhat-labs/utils/commit/dfc205a4b6418326a96b70b2935bba39d2e72ea1))

## [1.0.3](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.2...v1.0.3) (2023-02-02)


### Bug Fixes

* **deps:** update returntocorp/semgrep docker tag to v1.9 ([faba3d5](https://gitlab.com/yellowhat-labs/utils/commit/faba3d5e8edd93b28e56e5b956ee117c6f30d8ba))
* **deps:** update returntocorp/semgrep docker tag to v1.9 ([aa3c6f8](https://gitlab.com/yellowhat-labs/utils/commit/aa3c6f8d458eaa953c273c74e84b2aec2ed7d5d2))

## [1.0.2](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.1...v1.0.2) (2023-02-01)


### Bug Fixes

* **deps:** update pyfound/black docker tag to v23 ([0bc7770](https://gitlab.com/yellowhat-labs/utils/commit/0bc777009e6a525932481fc04912463e7002df85))
* **deps:** update pyfound/black docker tag to v23 ([b7e8f9c](https://gitlab.com/yellowhat-labs/utils/commit/b7e8f9c051b18fa1f3239dd085adc1e120a2617c))

## [1.0.1](https://gitlab.com/yellowhat-labs/utils/compare/v1.0.0...v1.0.1) (2023-01-31)


### Bug Fixes

* long version for node ([654d9e4](https://gitlab.com/yellowhat-labs/utils/commit/654d9e40f0c905e93e60d4806f00ae691759913e))

# [1.0.0](https://gitlab.com/yellowhat-labs/utils/compare/...v1.0.0) (2023-01-31)


### Bug Fixes

* initial release ([3c32049](https://gitlab.com/yellowhat-labs/utils/commit/3c32049d5bae79160a74d0a17dc7d66a9244d30a))
