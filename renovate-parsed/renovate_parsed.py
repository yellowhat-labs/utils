#!/usr/bin/env python
"""Test parsed dependencies found by renovate"""

from collections import defaultdict
from json import dumps, loads
from os import environ
from re import search
from typing import Any, List, Union
from urllib.request import Request, urlopen

CI_PROJECT_ID = environ["CI_PROJECT_ID"]
TOKEN = environ["READ_API_TOKEN"]


def get_reference() -> Any:
    """Get reference"""

    with open(".gitlab/renovate_ref.json", encoding="utf8") as obj:
        return loads(obj.read())


def get_description(project_id: str, token: str) -> Any:
    """Get description"""
    url = f"https://gitlab.com/api/v4/projects/{project_id}/issues?state=opened"
    request = Request(url)
    request.add_header("PRIVATE-TOKEN", token)
    with urlopen(request) as obj:  # nosemgrep  # nosec B310
        response = loads(obj.read())

    descs = next(
        i["description"].splitlines()
        for i in response
        if i["title"] == "Dependency Dashboard"
    )
    idx = next(i for i, d in enumerate(descs) if "Detected dependencies" in d)

    return descs[idx:]


def parse(lines: list[str]) -> defaultdict[Union[str, Any], List[Union[str, Any]]]:
    """Parse"""

    results = defaultdict(list)
    for line in lines:
        if "<summary>" in line:
            key = search(r"<summary>(.*)<\/summary>", line).group(1)  # type: ignore
        elif line[:3] == " - ":
            item = search(r"`(.*)`", line).group(1)  # type: ignore
            results[key].append(item.split()[0])
    return results


def compare(
    ref: dict[str, List[str]],
    cur: defaultdict[Union[str, Any], List[Union[str, Any]]],
) -> None:
    """Compare"""
    assert (
        ref.keys() == cur.keys()
    ), f"Keys do not match:\n{ref.keys()}\n{cur.keys()}"  # nosec B101

    for key, values in ref.items():
        if key in cur and tuple(values) == tuple(cur[key]):
            continue
        raise ValueError(f"{key} values do not match:\n{values}\n{cur[key]}")


if __name__ == "__main__":
    reference = get_reference()
    current = parse(get_description(project_id=CI_PROJECT_ID, token=TOKEN))
    print("[INFO] Current:")
    print(dumps(current, indent=4))
    compare(reference, current)
