#!/usr/bin/env sh
set -eu

CONFIG_REF=/npm/release.config.js
CONFIG_CUR=/tmp/release.config.js

if [ -f release.config.js ]; then
    echo "[INFO] Found release.config.js in $PWD"
    echo "[INFO] Merging the files..."
    cp --verbose release.config.js "$CONFIG_CUR"
    cat >release.config.js <<EOF
module.exports = {
    branches: ["main"],
    tagFormat: "v\${version}",
    plugins: [
        ...require('${CONFIG_CUR}').plugins,
        ...require('${CONFIG_REF}').plugins,
    ],
};
EOF
    cat release.config.js
else
    echo "[INFO] No release.config.js found, using $CONFIG_REF"
    cp --verbose "$CONFIG_REF" .
fi
echo ""

semantic-release
