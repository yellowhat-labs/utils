// https://semantic-release.gitbook.io/semantic-release/usage/configuration
module.exports = {
    branches: ["main"],
    tagFormat: "v${version}",
    plugins: [
        [
            "@semantic-release/changelog",
            {
                changelogFile: "CHANGELOG.md",
            },
        ],
        [
            "@semantic-release/git",
            {
                message: "chore(release): ${nextRelease.version}",
                assets: ["CHANGELOG.md"],
            },
        ],
        "@semantic-release/exec",
        "@semantic-release/gitlab",
        [
            "@semantic-release/commit-analyzer",
            {
                preset: "angular",
                releaseRules: [
                    {
                        type: "breaking",
                        release: "major",
                    },
                    {
                        type: "build", // Changes that affect the build system or external dependencies
                        release: "patch",
                    },
                    {
                        type: "chore", // Other changes that don"t modify src or test files
                        release: false,
                    },
                    {
                        type: "ci", // Changes to our CI configuration files and scripts
                        release: false,
                    },
                    {
                        type: "docs", // Documentation only changes
                        release: "patch",
                    },
                    {
                        type: "feat", // A new feature
                        release: "minor",
                    },
                    {
                        type: "fix", // A bug fix
                        release: "patch",
                    },
                    {
                        type: "perf", // A code change that improves performance
                        release: "patch",
                    },
                    {
                        type: "refactor", // A code change that neither fixes a bug nor adds a feature
                        release: false,
                    },
                    {
                        type: "revert", // Reverts a previous commit
                        release: "patch",
                    },
                    {
                        type: "style", // Changes that do not affect the meaning of the code
                        release: false,
                    },
                    {
                        type: "test", // Adding missing tests or correcting existing tests
                        release: false,
                    },
                ],
            },
        ],
        [
            "@semantic-release/release-notes-generator",
            {
                linkCompare: true,
                linkReferences: true,
                writerOpts: {
                    commitGroupsSort: [
                        "breaking",
                        "feat",
                        "fix",
                        "revert",
                        "refactor",
                        "perf",
                        "docs",
                        "style",
                        "build",
                        "chore",
                        "upgrade",
                        "ci",
                    ],
                    transform: (commit, context) => {
                        // based on https://github.com/conventional-changelog/conventional-changelog/blob/master/packages/conventional-changelog-angular/src/writer.js

                        let discard = true;
                        const notes = commit.notes.map((note) => {
                            discard = false;
                            return {
                                ...note,
                                title: "BREAKING CHANGES",
                            };
                        });

                        let type = commit.type;
                        if (commit.type === "breaking") {
                            type = "Breaking Changes";
                        } else if (commit.type === "feat") {
                            type = "Features";
                        } else if (commit.type === "fix") {
                            type = "Bug Fixes";
                        } else if (commit.type === "perf") {
                            type = "Performance Improvements";
                        } else if (commit.type === "revert" || commit.revert) {
                            type = "Reverts";
                        } else if (commit.type === "docs") {
                            type = "Documentation";
                        } else if (commit.type === "style") {
                            type = "Styles";
                        } else if (commit.type === "refactor") {
                            type = "Code Refactoring";
                        } else if (commit.type === "test") {
                            type = "Tests";
                        } else if (commit.type === "build") {
                            type = "Build System";
                        } else if (commit.type === "ci") {
                            type = "Continuous Integration";
                        } else if (commit.type === "upgrade") {
                            type = "Dependency Updates";
                        } else if (commit.type === "chore") {
                            type = "Chore";
                        }

                        const scope = commit.scope === "*" ? "" : commit.scope;
                        const shortHash =
                            typeof commit.hash === "string"
                                ? commit.hash.substring(0, 7)
                                : commit.shortHash;

                        const issues = [];
                        let subject = commit.subject;

                        if (typeof subject === "string") {
                            let url = context.repository
                                ? `${context.host}/${context.owner}/${context.repository}`
                                : context.repoUrl;
                            if (url) {
                                url = `${url}/issues/`;
                                // Issue URLs.
                                subject = commit.subject.replace(
                                    /#([0-9]+)/g,
                                    (_, issue) => {
                                        issues.push(issue);
                                        return `[#${issue}](${url}${issue})`;
                                    },
                                );
                            }
                            if (context.host) {
                                // User URLs.
                                subject = commit.subject.replace(
                                    /\B@([a-z0-9](?:-?[a-z0-9/]){0,38})/g,
                                    (_, username) => {
                                        if (username.includes("/")) {
                                            return `@${username}`;
                                        }

                                        return `[@${username}](${context.host}/${username})`;
                                    },
                                );
                            }
                        }

                        // Remove references that already appear in the subject
                        const references = commit.references.filter(
                            (reference) => !issues.includes(reference.issue),
                        );

                        return {
                            notes,
                            type,
                            scope,
                            shortHash,
                            subject,
                            references,
                        };
                    },
                },
            },
        ],
    ],
};
