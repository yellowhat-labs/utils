FROM ghcr.io/astral-sh/uv:0.6.4-python3.13-alpine@sha256:f39afcc617408279ced94c8f27b9439ab2cc8a2e93b6dcc270fbad2fc8c2d620

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# Avoid buffering Python output
ENV PYTHONUNBUFFERED 1

# renovate: datasource=repology depName=alpine_3_21/bash versioning=loose
ARG BASH_VER=5.2.37-r0
# renovate: datasource=repology depName=alpine_3_21/curl versioning=loose
ARG CURL_VER=8.12.1-r0
# renovate: datasource=repology depName=alpine_3_21/openssl versioning=loose
ARG OPENSSL_VER=3.3.3-r0
RUN apk add --no-cache \
        "bash=$BASH_VER"\
        "curl=$CURL_VER" \
        "openssl=$OPENSSL_VER"

COPY --from=docker.io/pulumi/pulumi-python:3.153.1@sha256:87bdb6a25b0db47fe6680ca45f3da26193b9eff5932cef689648ac5c67b5ef10 \
       /pulumi/bin/* \
       /usr/local/bin

COPY requirements.txt /tmp/
# hadolint ignore=SC2013
RUN pulumi version \
 && uv pip install --system --no-cache-dir --requirement /tmp/requirements.txt \
 && for line in $(grep pulumi /tmp/requirements.txt); do \
        plugin=$(echo "$line" | cut -d "=" -f 1 | cut -d "-" -f 2-); \
        version=$(pip show "pulumi_${plugin}" | grep Version | awk '{print $2}'); \
        pulumi plugin install resource "$plugin" "$version"; \
    done \
 && pulumi plugin ls \
 && rm -rf /tmp/requirements.txt

CMD ["/bin/bash"]
