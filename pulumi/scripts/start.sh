#!/usr/bin/env bash
# Start the pulumi container
set -euo pipefail

IMAGE_TAG=pulumi:local

podman build --tag "$IMAGE_TAG" .

args=(
    --interactive
    --tty
    --rm
    # Required by Pulumi to access GitLab
    --env GITLAB_TOKEN="$(pass --password "GitLab Token")"
    # Required to download/upload Pulumi state from GitLab Packages
    --env CI_JOB_TOKEN="$(pass --password "GitLab Token")"
    --env TOKEN_TYPE=PRIVATE-TOKEN
    --env CI_API_V4_URL=https://gitlab.com/api/v4
    --env CI_PROJECT_ID=42991927
    --env PULUMI_STATE_DIR=/builds/yellowhat-labs/bootstrap/pulumi/state
    --env PULUMI_STACK=dev
    --volume "${PWD}:/data:z"
    --workdir /data
)
podman run "${args[@]}" "$IMAGE_TAG"
