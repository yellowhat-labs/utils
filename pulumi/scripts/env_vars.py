#!/usr/bin/env python
"""
Fetch and export environmental variables from the project.

Run using:
    eval $(python scripts/env-vars.py)
"""

from json import loads
from os import environ
from urllib.request import Request, urlopen

request = Request(
    url=f"{environ['CI_API_V4_URL']}/projects/{environ['CI_PROJECT_ID']}/variables",
    headers={"PRIVATE-TOKEN": environ["GITLAB_TOKEN"]},
)
with urlopen(request) as obj:  # nosemgrep  # nosec B310
    variables = loads(obj.read().decode("utf-8"))

for variable in variables:
    print(f"export {variable['key']}={variable['value']}")
