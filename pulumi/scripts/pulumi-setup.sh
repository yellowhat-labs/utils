#!/usr/bin/env bash
# Setup pulumi
set -euo pipefail

echo "[INFO] Pulumi - login"
mkdir -p "$PULUMI_STATE_DIR"
pulumi login "file://${PULUMI_STATE_DIR}"
echo ""

echo "[INFO] Pulumi - create $PULUMI_STACK stack"
pulumi stack select --create "$PULUMI_STACK"
echo ""

echo "[INFO] Pulumi - stacks:"
pulumi stack ls
echo ""
