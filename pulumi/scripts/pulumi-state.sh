#!/usr/bin/env bash
# Download/Upload pulumi state
set -xeuo pipefail

PACKAGE_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/pulumi-state/0.0.0/pulumi-state.tar.gz.enc"
PACKAGE_TAR="${PACKAGE_URL##*/}"
TOKEN_TYPE="${TOKEN_TYPE:-JOB-TOKEN}"

openssl_args=(
    -aes-256-cbc
    -pbkdf2
    -pass "pass:${PULUMI_STATE_ENCRYPT_PASSWORD}"
)

case "$1" in
    "download")
        echo "[INFO] Check if exist"
        curl \
            --header "${TOKEN_TYPE}: $CI_JOB_TOKEN" \
            --output /dev/null \
            --silent \
            --head \
            --fail \
            "$PACKAGE_URL" || exit 0
        echo "[INFO] Download state, decrypt and decompress"
        curl \
            --header "${TOKEN_TYPE}: $CI_JOB_TOKEN" \
            "$PACKAGE_URL" |
            openssl enc "${openssl_args[@]}" -d |
            tar xz --directory /
        ;;
    "upload")
        echo "[INFO] Remove backups, history, locks"
        rm -rf "$PULUMI_STATE_DIR"/.pulumi/{backups,history,locks}
        echo "[INFO] Compressing and encrypt state"
        tar cz --directory "$PULUMI_STATE_DIR" "$PULUMI_STATE_DIR/" |
            openssl enc "${openssl_args[@]}" -e -out "$PACKAGE_TAR"
        echo "[INFO] Upload state"
        curl \
            --upload-file "$PACKAGE_TAR" \
            --header "${TOKEN_TYPE}: $CI_JOB_TOKEN" \
            "$PACKAGE_URL"
        ;;
    *)
        echo "Invalid option: $1"
        exit 1
        ;;
esac
