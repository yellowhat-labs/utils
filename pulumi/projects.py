"""Configure GitLab projects"""

from os import environ
import pulumi_gitlab as gitlab
from pulumi import Output, ResourceOptions


def create_project(
    name: str,
    description: str,
) -> gitlab.Project:
    """Boilerplate to create a common project"""

    project = gitlab.Project(
        name,
        name=name,
        path=name,
        description=description,
        visibility_level="public",
        namespace_id=62143620,
        group_runners_enabled=True,
        merge_method="ff",
        squash_option="default_on",
        default_branch="main",
        ci_default_git_depth=1,
        ci_separated_caches=False,
        container_registry_access_level="enabled",
        container_expiration_policy=gitlab.ProjectContainerExpirationPolicyArgs(
            enabled=True,
            cadence="1d",
            keep_n=1,
            name_regex_delete=".*",
            name_regex_keep="(?:v.+|main)",
            older_than="14d",
        ),
        opts=ResourceOptions(protect=True),
    )

    gitlab.BranchProtection(
        name,
        project=project.id,
        branch="main",
        push_access_level="maintainer",
        merge_access_level="maintainer",
        allow_force_push=True,
    )

    if name != "bootstrap":
        # https://gitlab.com/-/user_settings/personal_access_tokens?name=COMMENT&scopes=api
        gitlab.ProjectVariable(
            f"{name}/comment",
            project=project.id,
            key="COMMENT_TOKEN",
            value=Output.secret(environ["COMMENT_TOKEN"]),
            description="Comment on MR",
            variable_type="env_var",
            raw=True,
            masked=True,
            protected=False,
        )

        # Should be a gitlab.ProjectAccessToken but not for free tier...
        # https://gitlab.com/-/user_settings/personal_access_tokens?name=READ_API&scopes=read_api
        gitlab.ProjectVariable(
            f"{name}/read_api",
            project=project.id,
            key="READ_API_TOKEN",
            value=Output.secret(environ["READ_API_TOKEN"]),
            description="read_api token for renovate_test",
            variable_type="env_var",
            raw=True,
            masked=True,
            protected=False,
        )

        # https://gitlab.com/-/user_settings/personal_access_tokens?name=SEMANTIC_RELEASE&scopes=api,write_repository
        # Semantic release requires GITLAB_TOKEN, do not forget to rename
        gitlab.ProjectVariable(
            f"{name}/semantic-release",
            project=project.id,
            key="SEMANTIC_RELEASE_TOKEN",
            value=Output.secret(environ["SEMANTIC_RELEASE_TOKEN"]),
            description="Access GitLab for Renovate",
            variable_type="env_var",
            raw=True,
            masked=True,
            protected=False,
        )

    return project
