"""Configure GitLab repositories"""

from os import environ
import pulumi_gitlab as gitlab
from projects import create_project
from pulumi import Output

bootstrap = create_project(
    name="bootstrap",
    description="GitLab CI templates, Renovate",
)

create_project(
    name="ebuilds",
    description="Gentoo overlay",
)

create_project(
    name="flatpak",
    description="Flatpak repository",
)

create_project(
    name="htpc",
    description="Run HTPC using NixOS",
)

create_project(
    name="kaos",
    description="Run k3s on Oracle",
)

create_project(
    name="mule",
    description="A self-hosted kubernetes cluster managed using ArgoCD",
)

create_project(
    name="podinfo",
    description="Expose pod/node information",
)

create_project(
    name="serve",
    description="Extend python's http.server to upload files",
)

create_project(
    name="sf",
    description="A simple console file manager written in python",
)

create_project(
    name="solax-exporter",
    description="Prometheus exporter for SolaX Power",
)

create_project(
    name="statusbar",
    description="A custom statusbar, written in rust, to be used with swaybar",
)

create_project(
    name="workstation",
    description="Run workstation using NixOS",
)

# https://github.com/settings/tokens/new:
# Token name: RENOVATE
# Expiration
# Select scopes: None
gitlab.ProjectVariable(
    "bootstrap/renovate-github",
    project=bootstrap.id,
    key="GITHUB_COM_TOKEN",
    value=Output.secret(environ["GITHUB_COM_TOKEN"]),
    description="Access GitHub for Renovate",
    variable_type="env_var",
    raw=True,
    masked=True,
    protected=False,
)

# https://gitlab.com/-/user_settings/personal_access_tokens?name=RENOVATE&scopes=api,write_repository,read_registry
gitlab.ProjectVariable(
    "bootstrap/renovate-gitlab",
    project=bootstrap.id,
    key="RENOVATE_TOKEN",
    value=Output.secret(environ["RENOVATE_TOKEN"]),
    description="Access GitLab for Renovate",
    variable_type="env_var",
    raw=True,
    masked=True,
    protected=False,
)
