# pulumi

Configure GitLab repositories using [Pulumi](pulumi.com)

## Notes

* Seems that it is impossible to generate Personal Access Token
via another Personal Access Token, maybe a limitation of Free-Tier?

## Requirements

1. [Create new Personal Access Token](https://gitlab.com/-/user_settings/personal_access_tokens?name=PULUMI_GITLAB_TOKEN&scopes=api)

2. From this GitLab repository > `Settings` > `CI/CD` > `Variables` > `Add variable`:
    1. GitLab token:
        * Key: `PULUMI_GITLAB_TOKEN`
        * Value: GitLab token above
        * Visibility: Masked
        * [ ] Protect variable
        * [ ] Expand variable reference
    2. Pulumi password:
        * Key: `PULUMI_CONFIG_PASSPHRASE`
        * Value: random value
        * Visibility: Masked
        * [ ] Protect variable
        * [ ] Expand variable reference
    3. Pulumi state encryption password:
        * Key: `PULUMI_STATE_ENCRYPT_PASSWORD`
        * Value: random value
        * Visibility: Masked
        * [ ] Protect variable
        * [ ] Expand variable reference

3. From this GitLab repository > `Settings` > `Packages and Registries`:
    * `Number of duplicate assets to keep`: `10`

## Rotate secret

1. Start a local environment:

    ```bash
    ./scripts/start.sh
    eval $(python scripts/env_vars.py)
    ./scripts/pulumi-state.sh download
    ./scripts/pulumi-setup.sh
    ```

2. Update passphrase:

    ```bash
    pulumi stack change-secrets-provider passphrase
    ```

3. Upload new state:

    ```bash
    ./scripts/pulumi-state.sh upload
    ```
