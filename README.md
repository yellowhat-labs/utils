# utils

## GitLab CI/CD Templates

This repository contains GitLab CI/CD Templates:

* [static code analysis](templates/linter.yaml)
* [build container](templates/build.yaml)
* [create new release](templates/release.yaml)
* [add comment to an MR](templates/comment.yaml)

## Renovate

This repository runs `renovate` against other repositories.
