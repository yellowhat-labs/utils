module.exports = {
    platform: "gitlab",
    extends: [
        "config:recommended",
        "docker:enableMajor",
        ":disableRateLimiting",
    ],
    autodiscover: false,
    repositories: [
        "yellowhat-labs/bootstrap",
        "yellowhat-labs/ebuilds",
        "yellowhat-labs/flatpak",
        "yellowhat-labs/htpc",
        "yellowhat-labs/kaos",
        "yellowhat-labs/mule",
        "yellowhat-labs/podinfo",
        "yellowhat-labs/serve",
        "yellowhat-labs/sf",
        "yellowhat-labs/solax-exporter",
        "yellowhat-labs/statusbar",
        "yellowhat-labs/workstation",
    ],
    dependencyDashboard: true,
    baseBranches: ["main"],
    labels: ["renovate"],
    separateMajorMinor: true,
    lockFileMaintenance: {
        enabled: true,
        schedule: ["before 10pm"],
    },
    nix: {
        enabled: true,
    },
    gitlabci: {
        fileMatch: ["\\.gitlab-ci\\.ya?ml$", "\\.gitlab/.*\\.ya?ml$"],
    },
    pip_requirements: {
        fileMatch: ["requirements.txt", "requirements-dev.txt"],
    },
    packageRules: [
        {
            description: "Group all minor and patch updates together",
            groupName: "all non-major dependencies",
            groupSlug: "all-minor-patch",
            matchPackageNames: ["*"],
            matchUpdateTypes: ["minor", "patch", "pin", "digest"],
        },
        {
            description: "Pin digest for container image",
            matchDatasources: ["docker"],
            pinDigests: true,
        },
        {
            description: "Podman updates same tag but removes previous digest",
            matchPackageNames: ["/^quay\\.io/podman//"],
            matchDatasources: ["docker"],
            pinDigests: false,
        },
        {
            description: "Enable indirect updates for gomod",
            matchManagers: ["gomod"],
            matchDepTypes: ["indirect"],
            enabled: true,
        },
        {
            description: "Fetch pulumi changelog from GitHub",
            matchPackageNames: ["/^docker\\.io/pulumi//"],
            matchDatasources: ["docker"],
            sourceUrl: "https://github.com/pulumi/pulumi",
        },
    ],
    postUpdateOptions: ["gomodTidy"],
    customManagers: [
        {
            description: "Look for pip install",
            customType: "regex",
            fileMatch: [".*"],
            matchStrings: [
                "(pip|pip3)\\s+install\\s+(?<depName>[_\\-a-zA-Z0-9]+)(\\[.*\\])?==(?<currentValue>[\\w.]+)",
            ],
            datasourceTemplate: "pypi",
        },
        {
            description: "Look for npm install",
            customType: "regex",
            fileMatch: [".*"],
            matchStrings: [
                "npm\\s+install\\s+(?:--global|-g)\\s+(?<depName>[@\\/_\\-a-zA-Z]+)@(?<currentValue>[\\w.]+)",
            ],
            datasourceTemplate: "npm",
        },
        {
            description: "Look for npx",
            customType: "regex",
            fileMatch: [".*"],
            matchStrings: [
                "npx\\s+(?:--yes|-y)\\s+(?<depName>[@\\/_\\-a-zA-Z]+)@(?<currentValue>[\\w.]+)",
            ],
            datasourceTemplate: "npm",
        },
        {
            description: "Comment",
            customType: "regex",
            fileMatch: [".*"],
            matchStrings: [
                "#\\s*renovate:\\s*datasource=(?<datasource>\\S+)\\s+depName=(?<depName>\\S+)\\s*(versioning=(?<versioning>\\S*))?\\s+.*(?i)(tag|ver|version)\\s*(=|:)\\s*(['\"]?)(?<currentValue>[^\"'\\s]*)(['\"]?)",
            ],
            versioningTemplate:
                "{{#if versioning}}{{{versioning}}}{{else}}ver{{/if}}",
        },
    ],
    allowCommandTemplating: true,
    allowedCommands: [".*"],
};
